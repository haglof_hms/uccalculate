/* 2007-04-13 Per-�ke Danielsson */
#if !defined(AFX_HEIGHTH25_H)
#define AFX_HEIGHTH25_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

////////////////////////////////////////////////////////////////////////////////////////
// CHeightH25

class CHeightH25 : public CCalculationBaseClass
{
	//private
protected:
	BOOL setHeightForTrees(double H25_entered);	
	BOOL setHeightForDGVTree(void);

	double askUserForH25Value(LPCTSTR spc_name);

public:
	CHeightH25(void);
	CHeightH25(int spc_index,CTransaction_trakt rec1,
													 CTransaction_trakt_misc_data rec2,
													 vecTransactionTraktData &tdata_list,
													 vecTransactionSampleTree &sample_tree_list,
													 vecTransactionDCLSTree &dcls_tree_list,
													 vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);

};



#endif