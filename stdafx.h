// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// Identifer for functions
#define ID_BRANDELS					1000
#define ID_BRANDELS_UB				1100
#define ID_NASLUNDS					1001
#define ID_NASLUNDS_UB				1101
#define ID_HAGBERG_MATERN			1002	// Volumefunctions for "Ek" and "Book"
#define ID_HAGBERG_MATERN_UB		1102	// Volumefunctions for "Ek" and "Book" "Under bark"; 090305 p�d
#define ID_CYLINDER					1003	// Cylinder volume
// ID for tjeckien volume-functions; 090603 p�d
#define ID_CZ_VOLUME				9000	// CZ volume functions, start at a id not in the range of Swedish function; 090603 p�d

#define ID_H25						1500
#define ID_REGRESSION				1501
#define ID_SODERBERGS				1502

#define ID_BARK_JO					2000
#define ID_BARK_SF					2001	// Skogforsk Sf_tall och Sf_gran
#define ID_BARK_SO					2002	// S�derbergs barkfunktioner

#define ID_OMF_FACTOR_UB			3000	// Special value; 071016 p�d
// Defines for default values in functions; 070417 p�d
//#define H25_MIN_VALUE				5.0		// #4670: konstant finns numer i Funclib
//#define H25_MAX_VALUE				35.0	// #4670: konstant finns numer i Funclib
#define MAX_HGT_IN_REGRESSION		60.0
#define MAX_DBH_IN_SF				590.0						// "Max Br�sth�jdsdiameter" in Skogforsk formula for Pine (Tall)
#define MIN_BARK_IN_SF				2.0							// Min bark-thickness in Skogforsk formula for Spruce (Gran) and Pine (Tall)
#define MIN_NUMOF_SAMPLETREES_SODERBERGS 3		// Min. number of sampletrees for "S�derbergs h�jdfunktion"
#define MIN_NUMOF_SAMPLETREES_REGRESSION 2		// Min. number of sampletrees for "Regression h�jdfunktion"

// Defines for treetype
#define SAMPLE_TREE					0		// A sampletree. I.e. it has a heigth etc
#define TREE_OUTSIDE				1		// "Intr�ngsv�rdering; kanttr�d Provtr�d"
#define TREE_OUTSIDE_NO_SAMP		2		// "Intr�ngsv�rdering; kanttr�d Ej Provtr�d"
#define TREE_SAMPLE_REMAINIG		3		// "Provtr�d (Kvarl�mmnat)"
#define TREE_SAMPLE_EXTRA			4		// "Provtr�d (Extra inl�sta)"

#ifndef M_PI
#define M_PI						3.14159265358979323846
#endif

// Uncomment to run GROT caluclations; 100414 p�d
// NB! Also in UMEstimate!
#define CALCULATE_GROT

#define ID_GROT_PINE			6000	// "Tall grot"
#define ID_GROT_SPRUCE		6001	// "Gran grot (Contorta)"
#define ID_GROT_BEACH			6002	// "Bj�rk grot"
#define ID_GROT_LEAF			6003	// "�vrigt l�v grot"

#include "pad_hms_miscfunc.h"				// HMS_FuncLib
#include "pad_calculation_classes.h"
#include "xmllite.h"


// Interpret languagefile for UCCalculateXXX.xml; 090603 p�d
#define LANG_READ_FUNC_BARK				_T("function_bark")
#define LANG_READ_FUNC_BARK_TAG			_T("bark")

#define LANG_READ_FUNC_HEIGHT			_T("function_height")
#define LANG_READ_FUNC_HEIGHT_TAG		_T("height")

#define LANG_READ_FUNC_VOLUME			_T("function_volume")
#define LANG_READ_FUNC_VOLUME_TAG		_T("volume")

#define LANG_READ_FUNC_VOLUME_UB		_T("function_volume_ub")
#define LANG_READ_FUNC_VOLUME_UB_TAG	_T("volume_ub")

#define LANG_READ_FUNC_ID				_T("func_id")
#define LANG_READ_FUNC_NAME				_T("func_name")

#define LANG_READ_FUNC_ATTR_ID			_T("id")
#define LANG_READ_FUNC_ATTR_SUB_ID		_T("sub_id")
#define LANG_READ_FUNC_ATTR_SPC_NAME	_T("spc_name")
#define LANG_READ_FUNC_ATTR_FUNC_TYPE1	_T("func_type1")
#define LANG_READ_FUNC_ATTR_FUNC_TYPE2	_T("func_type2")
#define LANG_READ_FUNC_ATTR_FUNC_DESC	_T("desc")

const LPCTSTR NAMN_OF_MODULE			= _T("UCCalculate");


// Functions; 090603 p�d
#define VOL_FUNC_BRANDELS				_T("Brandels")
#define VOL_FUNC_NASLUNDS				_T("N�slunds")
#define VOL_FUNC_HAGBERG_MARTEN			_T("Hagberg/Marten")
#define VOL_FUNC_CYLINDER				_T("Cylinder")


//-----------------------------------------------------------------------------
//****************** OBS! Table also in UCLandValueNorm  *********************
//-----------------------------------------------------------------------------
// Table for conversion from "Bj�rk,Ek,Bok och Contorta"; 100311 p�d
#define NUMOF_COLS_IN_TABLE_CONVERS		5
#define NUMOF_ROWS_IN_TABLE_CONVERS		33
const short TABLE_CONVERS[NUMOF_ROWS_IN_TABLE_CONVERS][NUMOF_COLS_IN_TABLE_CONVERS] = 
{	{8,0,0,0,12},
	{9,0,0,0,12},
	{10,0,0,0,14},
	{11,0,0,0,14},
	{12,0,0,0,14},
	{13,0,0,0,16},
	{14,0,0,16,16},
	{15,0,0,16,18},
	{16,0,0,18,18},
	{17,0,0,18,20},
	{18,28,26,20,20},
	{19,30,26,20,22},
	{20,30,28,22,22},
	{21,30,28,24,22},
	{22,30,28,24,24},
	{23,30,28,26,24},
	{24,32,30,26,26},
	{25,32,30,28,26},
	{26,32,30,28,28},
	{27,32,32,30,28},
	{28,34,32,32,30},
	{29,34,32,32,30},
	{30,34,34,34,32},
	{31,34,0,0,32},
	{32,34,0,0,32},
	{33,36,0,0,0},
	{34,36,0,0,0},
	{35,36,0,0,0},
	{36,36,0,0,0},
	{37,0,0,0,0},
	{38,0,0,0,0},
	{39,0,0,0,0},
	{40,0,0,0,0}};



// Misc functions
CString getResStr(int id);

int SplitString(const CString& input,const CString& delimiter, CStringArray& results);

// Function added 2009-06-03 p�d
BOOL existsFile(LPCTSTR fn);

CString getLangSetInReg();
CString getLanguageDirInReg(void);
CString getPathToDatafile(CString fn);

CString getCalcLangFile(void);
BOOL readLangFile_functions(LPCTSTR xml_fn,LPCTSTR tag_type,vecUCFunctions &func);
BOOL readLangFile_function_list(LPCTSTR xml_fn,LPCTSTR main_tag,LPCTSTR tag,vecUCFunctionList &func_list);
