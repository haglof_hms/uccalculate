#include "StdAfx.h"
#include "VolumeCylinder.h"


// Calculate cylinder volume
double CVolumeCylinder::getCylinderVolume(double hgt,double dbh)
{
	double fD2 = (dbh*dbh)/4.0;
	return (fD2*M_PI)*hgt;
}

// PUBLIC:
CVolumeCylinder::CVolumeCylinder(void)
	: CCalculationBaseClass()
{
}

CVolumeCylinder::CVolumeCylinder(int spc_index,CTransaction_trakt rec1,
																							 CTransaction_trakt_misc_data rec2,
																							 vecTransactionTraktData &tdata_list,
																							 vecTransactionSampleTree &sample_tree_list,
																							 vecTransactionDCLSTree &dcls_tree_list,
																							 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}


BOOL CVolumeCylinder::calculate(vecTransactionTraktData &vec1,
																vecTransactionSampleTree &vec2,
																vecTransactionDCLSTree &vec4,		// Added 070820 p�d
																vecTransactionTraktSetSpc& vec3)
{
	double fM3Sk = 0.0;
	double fM3Fub = 0.0;
	double fOmfValue = 0.0;
	int nVolSpcID = -1;
	int nVolIndex = -1;
	int nSpcID = -1;
	int	nNumOfTrees = 0;

	// Check that there's any species; 070418 p�d
	if (m_nNumOfDCLSSpecies == 0)
		return FALSE;

	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nVolSpcID = getVolumeSpcID();	
	nVolIndex = getVolumeIndex();

	fOmfValue = getM3SkToM3Fub();

	// Loop through the tree list (m_vecSampleTrees) and
	// calculate volume for each tree; 070626 p�d
	for (int i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		if (tree.getSpcID() == nSpcID)
		{
			// Adaption for "Intr�ngsv�rdering":
			// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
			// OBS! This only allies to "Intr�ng"; 080307 p�d
			nNumOfTrees = tree.getNumOf() + tree.getNumOfRandTrees();
			// Depending on specie, run function accoring to
			// nBarkIndex value; 070417 p�d
			if (nVolSpcID == -1)	// All species
			{
				if (nVolIndex == 0)	// Including bark
				{
					fM3Sk = getCylinderVolume(tree.getHgt()/10.0,			// From dm to m
																					getClassMid(tree)/1000.0);	// From mm to m
					fM3Fub = fM3Sk * fOmfValue;

				}	// if (nVolIndex == 0)	// Including bark
				else if (nVolIndex == 1)	// Excluding bark
				{
					fM3Fub = getCylinderVolume(tree.getHgt()/10.0,			// From dm to m
																		 (getClassMid(tree)-(tree.getBarkThick()*2.0))/1000.0);	// From mm to m
					if (fOmfValue > 0.0)
						fM3Sk = fM3Fub / fOmfValue;
				}	// else if (nVolIndex == 1)	// Excluding bark
			}	// if (nVolSpcID == -1)

			if (nNumOfTrees > 0)
			{
				fM3Sk	*= nNumOfTrees;
				fM3Fub	*= nNumOfTrees;
			}

			m_vecDCLSTrees[i].setM3sk(fM3Sk);
			m_vecDCLSTrees[i].setM3fub(fM3Fub);

		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}