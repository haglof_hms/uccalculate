/* 2006-03-20 */
#if !defined(AFX_VOLUMENASLUND_UB_H)
#define AFX_VOLUMENASLUND_UB_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"


////////////////////////////////////////////////////////////////////////////////////////
// CVolumeNaslund_ub

class CVolumeNaslund_ub : public CCalculationBaseClass
{
//private:

protected:
//================================================================
//  Fromula: d2 + d2h + d2k + dhb
//================================================================
double getCalcUB_1(double dbh,double hgt,double kg,double bark,const double *const_val);
//================================================================
//  Fromula: d2 + d2h + d2k + dh2 + dhb
//================================================================
double getCalcUB_2(double dbh,double hgt,double kg,double bark,const double *const_val);
//================================================================
//  Fromula: d2 + d2h + dh2
//================================================================
double getCalcUB_3(double dbh,double hgt,const double *const_val);
//================================================================
//  Fromula: d2 + d2h + d2k + dh2 - h2
//================================================================
double getCalcUB_4(double dbh,double hgt,double kg,const double *const_val);
//================================================================
//  Fromula: d2 + d2h + dh2 - h2
//================================================================
double getCalcUB_5(double dbh,double hgt,const double *const_val);
//================================================================
//  Fromula: d2 + d2h + dh2 - h2 - dbh
//---------------------------------------------
double getCalcUB_6(double dbh,double hgt,double bark,const double *const_val);
//================================================================
//  Fromula: d2 + d2h + d2k + dh2
//---------------------------------------------
double getCalcUB_7(double dbh,double hgt,double kg,const double *const_val);

//================================================================
//  N�slunds volume calculation; 070413 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  kg      = GreenCrown (Krongr�ns)
//  bark    = bark reduction (barkavdrag)
//================================================================
double getVolumeForPine_ub(int n,double dbh,double hgt,double kg,double bark);
double getVolumeForSpruce_ub(int n,double dbh,double hgt,double kg,double bark);
double getVolumeForBirch_ub(int n,double dbh,double hgt,double kg,double bark);

public:
	CVolumeNaslund_ub(void);
	CVolumeNaslund_ub(int spc_index,CTransaction_trakt rec1,
															 CTransaction_trakt_misc_data rec2,
															 vecTransactionTraktData &tdata_list,
															 vecTransactionSampleTree &sample_tree_list,
															 vecTransactionDCLSTree &dcls_tree_list,
															 vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);
};

#endif