#include "StdAfx.h"

#include "Volume_m3sk_m3ub.h"

// PUBLIC:
CVolume_m3sk_m3ub::CVolume_m3sk_m3ub(void)
	: CCalculationBaseClass()
{
}

CVolume_m3sk_m3ub::CVolume_m3sk_m3ub(int spc_index,CTransaction_trakt rec1,
																						 CTransaction_trakt_misc_data rec2,
																						 vecTransactionTraktData &tdata_list,
																						 vecTransactionSampleTree &sample_tree_list,
																						 vecTransactionDCLSTree &dcls_tree_list,
																						 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}


BOOL CVolume_m3sk_m3ub::calculate(vecTransactionTraktData &vec1,
																	vecTransactionSampleTree &vec2,
																	vecTransactionDCLSTree &vec4,		// Added 070820 p�d
																	vecTransactionTraktSetSpc& vec3)
{
	double fM3Ub = 0.0;
	int nNumOfTrees = 0;	// Number of trees in Duameterclass
	int nVolSpcID = -1;
	int nVolIndex = -1;
	int nSpcID = -1;
	int nVolID = -1;
	// Check that there's any species; 070418 p�d
	if (m_nNumOfDCLSSpecies == 0)
		return FALSE;

	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nVolSpcID = getVolumeSpcID_ub();	
	nVolIndex = getVolumeIndex_ub();
	nVolID = getVolumeID_ub();

	// Loop through the tree list (m_vecSampleTrees) and
	// calculate volume for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getVolumeID() to get which species
	// method to use and getVolumeIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070417 p�d
	for (long i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		if (tree.getSpcID() == nSpcID)
		{
			// Adaption for "Intr�ngsv�rdering":
			// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
			// OBS! This only allies to "Intr�ng"; 080307 p�d
			nNumOfTrees = tree.getNumOf() + tree.getNumOfRandTrees();
			
			fM3Ub = tree.getM3sk() * getOmfValueForM3SkToM3Ub();

			if (nNumOfTrees > 0)
			{
//				fM3Ub	*= nNumOfTrees;
			}

			m_vecDCLSTrees[i].setM3ub(fM3Ub);

/* FOR DEBUG
			CString S;
			S.Format("CVolume_m3sk_m3ub::calculate\n\nfM3ub %f",
				fM3Ub);
			AfxMessageBox(S);
*/

		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}