#if !defined(AFX_DOCLACULATION_H)
#define AFX_DOCLACULATION_H

#include "StdAfx.h"

#include "VolumeBrandel.h"
#include "VolumeNaslund.h"
#include "VolumeHagbergMarten.h"
#include "VolumeCylinder.h"
#include "VolumeCZ.h"

#include "HeightH25.h"
#include "HeightSoderbergs.h"
#include "HeightRegression.h"

#include "BarkJO.h"
#include "BarkSF.h"
#include "BarkSoderbergs.h"

#include "VolumeBrandel_ub.h"
#include "VolumeNaslund_ub.h"
#include "Volume_m3sk_m3ub.h"

//=========================================================================
// Class handling actual calculation, depending on selections made by
// user. I.e. Volume-,Height- and Bark-functions; 070417 p�d

class UCDoCalculation
{
	//private:
	CTransaction_trakt m_recTrakt;
	CTransaction_trakt_misc_data m_recMiscData;
	vecTransactionTraktData m_vecTraktData;
	vecTransactionSampleTree m_vecSampleTrees;
	vecTransactionDCLSTree m_vecDCLSTrees;
	vecTransactionTraktSetSpc m_vecSpecies;
	vecTreeData m_vecTreeData;							// Information on sampletrees/specie in vecTransactionSampleTree; 070417 p�d

	int m_nVecSpecieIndex;
//	long m_nNumOfSampleTrees;			// Total number of trees in m_vecSampleTrees; 070424 p�d
//	UINT m_nNumOfSampleSpecies;		// Number of species in m_vecSampleTrees; 070417 p�d
//	UINT m_nNumOfTraktData;	// Number of items (for Trakt) in esti_trakt_data_table; 070524 p�d

	BOOL useForHeight(int id);
	BOOL useForBark(int id);
	BOOL useForVolume(int id);
	BOOL useForVolume_ub(int id);

	void setTreeData(void);
	void setProTraktData(void);
	void setPostTraktData(void);

	double m_fH25Calculated;

protected:
	double getClassMid(CTransaction_dcls_tree& v)
	{
		double fDCLS = (v.getDCLS_to() - v.getDCLS_from());
		double fMid = fDCLS / 2.0;
		return (v.getDCLS_from() + fMid)*10.0;	// From (cm) to (mm)
	}

public:
	UCDoCalculation(void);

	UCDoCalculation(CTransaction_trakt rec1,
									CTransaction_trakt_misc_data rec2,
									vecTransactionTraktData &tdata_list,
									vecTransactionSampleTree &sample_tree_list,
									vecTransactionDCLSTree &dcls_tree_list,
									vecTransactionTraktSetSpc &spc_list);

	BOOL doCalculation(vecTransactionTraktData&,
										 vecTransactionSampleTree&,
										 vecTransactionDCLSTree&,
										 vecTransactionTraktSetSpc&);
};


#endif