#include "StdAfx.h"

#include "VolumeHagbergMarten.h"

// PROTECTED


double CVolumeHagbergMarten::calculateCorrFactor(double dbh,double hgt)
{
	if (hgt > 0.0)
		return ((1-hgt/10.0)*(1-hgt/10.0))*(0.01682*(dbh*dbh)*hgt+0.01108*dbh*hgt-0.02167*dbh*(hgt*hgt)+0.04905*(dbh*dbh));
	else
		return 0.0;
}

//-----------------------------------------------------------------------------------------------
// "EK - INKLUSIVE BARK = M3SK"; 090304 p�d
//-----------------------------------------------------------------------------------------------

// Calculate Oak "Inklusive bark, hela stammmar"
// add_form_volume = to be able to seperate formulas.
// Calculate volume with or without the volume of Forks; 090304 p�d
double CVolumeHagbergMarten::setM3SkForOak_whole_trunk(double dbh,double hgt,bool add_form_volume)
{
	double fM3Sk = 0.0;
	double fGFactor = 0.0;
	double fDBH = dbh/10.0;	// From mm to cm
	double fHgt = hgt/10.0;	// From dm to m
	double fCorrFactor = 0.0;

	// If treeheight < 10.0 m, add this factor; 070625 p�d
	if (fHgt < 10.0)
	{
		fCorrFactor = calculateCorrFactor(fDBH,fHgt);
	}

	fM3Sk = (0.03522*(fDBH*fDBH)*fHgt + 0.08772*fDBH*fHgt - 0.04905*(fDBH*fDBH)) + fCorrFactor;

	fGFactor = 0.02813*(fDBH*fDBH)*fHgt - 0.3178*fDBH*fHgt - 0.0006658*(fDBH*fDBH)*(fHgt*fHgt);
/*
	CString S;
	S.Format(_T("P� BARK\nCVolumeHagbergMarten::setM3SkForOak_whole_trunk\nGFactor %f\nfCorrFactor %f\nM3Sk %f"),fGFactor,fCorrFactor,fM3Sk);
	AfxMessageBox(S);
*/
	if (fGFactor < 0.0)	fGFactor = 0.0;
	
	if (add_form_volume)	
		return (fM3Sk + fGFactor)/1000.0;	// From dm3 to mm3
	else
		return fM3Sk/1000.0;	// From dm3 to mm3
}

// Calculate Oak "Inklusive bark, klykstammar"
double CVolumeHagbergMarten::setM3SkForOak_forks(double dbh,double hgt,bool add_form_volume)
{
	double fM3Sk = 0.0;
	double fGFactor = 0.0;
	double fDBH = dbh/10.0;	// From mm to cm
	double fHgt = hgt/10.0;	// From dm to m
	double fCorrFactor = 0.0;

	// If treeheight < 10.0 m, add this factor; 070625 p�d
	if (fHgt < 10.0)
	{
		fCorrFactor = calculateCorrFactor(fDBH,fHgt);
	}

	fM3Sk = (0.03829*(fDBH*fDBH)*fHgt + 0.08772*fDBH*fHgt - 0.04905*(fDBH*fDBH)) + fCorrFactor;

	fGFactor = 0.02729*(fDBH*fDBH)*fHgt - 0.3178*fDBH*fHgt - 0.0006658*(fDBH*fDBH)*(fHgt*fHgt);
	if (fGFactor < 0.0)	fGFactor = 0.0;
	
	if (add_form_volume)	
		return (fM3Sk + fGFactor)/1000.0;	// From dm3 to mm3
	else
		return fM3Sk/1000.0;	// From dm3 to mm3
}
//-----------------------------------------------------------------------------------------------
// "EK - EXKLUSIVE BARK = M3UB"; 090304 p�d
//-----------------------------------------------------------------------------------------------
// Calculate Oak "Exklusive bark, hela stammmar"
double CVolumeHagbergMarten::setM3FubForOak_whole_trunk(double dbh,double hgt,bool add_form_volume)
{
	double fM3Fub = 0.0;
	double fGFactor = 0.0;
	double fDBH = dbh/10.0;	// From mm to cm
	double fHgt = hgt/10.0;	// From dm to m
	double fCorrFactor = 0.0;

	// If treeheight < 10.0 m, add this factor; 070625 p�d
	if (fHgt < 10.0)
	{
		fCorrFactor = calculateCorrFactor(fDBH,fHgt);
	}

	fM3Fub = (0.03474*fDBH*fDBH*fHgt + 0.07434*fDBH*fHgt - 0.04215*fDBH*fDBH) + fCorrFactor;

	fGFactor = 0.02366*fDBH*fDBH*fHgt - 0.20086*fDBH*fHgt - 0.00122*fDBH*fHgt*fHgt - 
						 0.0005601*fDBH*fDBH*fHgt*fHgt - 0.247*fHgt - 0.00067*fHgt*fHgt;
/*
	CString S;
	S.Format(_T("UNDER BARK\nCVolumeHagbergMarten::setM3FubForOak_whole_trunk\nGFactor %f\nfCorrFactor %f\nM3Fub %f"),fGFactor,fCorrFactor,fM3Fub);
	AfxMessageBox(S);
*/
	if (fGFactor < 0.0)	fGFactor = 0.0;
	
	if (add_form_volume)	
		return (fM3Fub + fGFactor)/1000.0;	// From dm3 to mm3
	else
		return fM3Fub/1000.0;	// From dm3 to mm3
}

// Calculate Oak "Exklusive bark, klykstammmar"
double CVolumeHagbergMarten::setM3FubForOak_forks(double dbh,double hgt,bool add_form_volume)
{
	double fM3Fub = 0.0;
	double fGFactor = 0.0;
	double fDBH = dbh/10.0;	// From mm to cm
	double fHgt = hgt/10.0;	// From dm to m
	double fCorrFactor = 0.0;

	fM3Fub = 0.03752*(fDBH*fDBH)*fHgt + 0.07434*fDBH*fHgt - 0.04215*(fDBH*fDBH);

	// If treeheight < 10.0 m, add this factor; 070625 p�d
	if (fHgt < 10.0)
	{
		fCorrFactor = calculateCorrFactor(fDBH,fHgt);
	}

	fGFactor = 0.02392*(fDBH*fDBH)*fHgt - 0.23085*fDBH*fHgt - 0.00065*fDBH*(fHgt*fHgt) - 
						 0.0005837*(fDBH*fDBH)*(fHgt*fHgt) - 0.137*fHgt - 0.00018*(fHgt*fHgt);
	if (fGFactor < 0.0)	fGFactor = 0.0;
	
	if (add_form_volume)	
		return (fM3Fub + fGFactor)/1000.0;	// From dm3 to mm3
	else
		return fM3Fub/1000.0;	// From dm3 to mm3
}

//-----------------------------------------------------------------------------------------------
// "BOOK - INKLUSIVE BARK = M3SK"; 090304 p�d
//-----------------------------------------------------------------------------------------------

// Calculate Beech "Inklusive bark, hela stammmar"
double CVolumeHagbergMarten::setM3SkForBeech_whole_trunk(double dbh,double hgt,bool add_form_volume)
{
	double fM3Sk = 0.0;
	double fGFactor = 0.0;
	double fDBH = dbh/10.0;	// From mm to cm
	double fHgt = hgt/10.0;	// From dm to m

	fM3Sk = 0.01275*(fDBH*fDBH)*fHgt + 0.12368*(fDBH*fDBH) + 0.0004701*(fDBH*fDBH)*(fHgt*fHgt) + 0.00622*fDBH*(fHgt*fHgt);
	fGFactor = 0.02080*(fDBH*fDBH)*fHgt - 0.24212*fDBH*fHgt - 0.0003486*(fDBH*fDBH)*(fHgt*fHgt);
	if (fGFactor < 0.0)	fGFactor = 0.0;

	if (add_form_volume)	
		return (fM3Sk + fGFactor)/1000.0;	// From dm3 to mm3
	else
		return fM3Sk/1000.0;	// From dm3 to mm3
}

// Calculate Beech "Inklusive bark, klykstammmar"
double CVolumeHagbergMarten::setM3SkForBeech_forks(double dbh,double hgt,bool add_form_volume)
{
	double fM3Sk = 0.0;
	double fGFactor = 0.0;
	double fDBH = dbh/10.0;	// From mm to cm
	double fHgt = hgt/10.0;	// From dm to m

	fM3Sk = 0.01681*(fDBH*fDBH)*fHgt + 0.12368*(fDBH*fDBH) + 0.0004701*(fDBH*fDBH)*(fHgt*fHgt) + 0.00622*fDBH*(fHgt*fHgt);
	fGFactor = 0.01936*(fDBH*fDBH)*fHgt - 0.24212*fDBH*fHgt - 0.0003486*(fDBH*fDBH)*(fHgt*fHgt);
	if (fGFactor < 0.0)	fGFactor = 0.0;

	if (add_form_volume)	
		return (fM3Sk + fGFactor)/1000.0;	// From dm3 to mm3
	else
		return fM3Sk/1000.0;	// From dm3 to mm3

}

//-----------------------------------------------------------------------------------------------
// "BOOK - EXKLUSIVE BARK = M3UB"; 090304 p�d
//-----------------------------------------------------------------------------------------------

// Calculate Beech "Exklusive bark, hela stammmar"
double CVolumeHagbergMarten::setM3FubForBeech_whole_trunk(double dbh,double hgt,bool add_form_volume)
{
	double fM3Fub = 0.0;
	double fGFactor = 0.0;
	double fDBH = dbh/10.0;	// From mm to cm
	double fHgt = hgt/10.0;	// From dm to m

	fM3Fub = 0.01442*(fDBH*fDBH)*fHgt + 0.11054*(fDBH*fDBH) + 0.00587*fDBH*(fHgt*fHgt) + 0.0004203*(fDBH*fDBH)*(fHgt*fHgt);
	fGFactor = 0.01932*(fDBH*fDBH)*fHgt - 0.21062*fDBH*fHgt - 0.00016*fDBH*(fHgt*fHgt) - 
						 0.0003239*(fDBH*fDBH)*(fHgt*fHgt) - 0.053*fHgt - 0.00002*(fHgt*fHgt);
	if (fGFactor < 0.0)	fGFactor = 0.0;

	if (add_form_volume)	
		return (fM3Fub + fGFactor)/1000.0;	// From dm3 to mm3
	else
		return fM3Fub/1000.0;	// From dm3 to mm3
}

// Calculate Beech "Exklusive bark, klykstammmar"
double CVolumeHagbergMarten::setM3FubForBeech_forks(double dbh,double hgt,bool add_form_volume)
{
	double fM3Fub = 0.0;
	double fGFactor = 0.0;
	double fDBH = dbh/10.0;	// From mm to cm
	double fHgt = hgt/10.0;	// From dm to m

	fM3Fub = 0.01831*(fDBH*fDBH)*fHgt + 0.11054*(fDBH*fDBH) + 0.00587*fDBH*(fHgt*fHgt) + 0.0004203*(fDBH*fDBH)*(fHgt*fHgt);
	fGFactor = 0.01809*(fDBH*fDBH)*fHgt - 0.21459*fDBH*fHgt - 0.00017*fDBH*(fHgt*fHgt) - 
						 0.0003258*(fDBH*fDBH)*(fHgt*fHgt) - 0.037*fHgt - 0.00001*(fHgt*fHgt);

	if (fGFactor < 0.0)	fGFactor = 0.0;

	if (add_form_volume)	
		return (fM3Fub + fGFactor)/1000.0;	// From dm3 to mm3
	else
		return fM3Fub/1000.0;	// From dm3 to mm3
}

// PUBLIC
CVolumeHagbergMarten::CVolumeHagbergMarten(void)
	: CCalculationBaseClass()
{
}

CVolumeHagbergMarten::CVolumeHagbergMarten(int spc_index,CTransaction_trakt rec1,
																												 CTransaction_trakt_misc_data rec2,
																												 vecTransactionTraktData &tdata_list,
																												 vecTransactionSampleTree &sample_tree_list,
																												 vecTransactionDCLSTree &dcls_tree_list,
																												 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}

BOOL CVolumeHagbergMarten::calculate(vecTransactionTraktData& vec1,
																		 vecTransactionSampleTree& vec2,
																		 vecTransactionDCLSTree& vec4,
																		 vecTransactionTraktSetSpc& vec3)
{
	double fM3Fub = 0.0;
	double fM3Sk = 0.0;
	double fOmfValue = 0.0;
	int nVolSpcID = -1;
	int nVolIndex = -1;
	int nSpcID = -1;
	int nNumOfTrees = 0;

	// Check that there's any species; 070418 p�d
	if (m_nNumOfSampleSpecies == 0)
		return FALSE;


	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nVolSpcID = getVolumeSpcID();	
	nVolIndex = getVolumeIndex();

	fOmfValue = getM3SkToM3Fub();


	// Loop through the tree list (m_vecSampleTrees) and
	// calculate height for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getHeightID() to get which species
	// method to use and getHeightIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070424 p�d
	for (int i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		// Check for Specie; 070424 p�d
		if (tree.getSpcID() == nSpcID)
		{
			// Adaption for "Intr�ngsv�rdering":
			// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
			// OBS! This only allies to "Intr�ng"; 080307 p�d
			nNumOfTrees = tree.getNumOf() + tree.getNumOfRandTrees();
			// Select function depending on specie and
			// index of function within specie; 070625 p�d
			if (nVolSpcID == 1)	// Beech
			{
				// Also, in this function, call method depending on
				// selection from user. This is because we can get both
				// fub and sk; 070625 p�d
				switch (nVolIndex)
				{
					case 0:	// "Hel stam"
						fM3Sk = setM3SkForBeech_whole_trunk(getClassMid(tree),tree.getHgt(),false /* "Inga grenar" */); 
//						AfxMessageBox(_T("Bok Hel stam"));
						break;
					case 1:	// "Hel stam + grenar"
						fM3Sk = setM3SkForBeech_whole_trunk(getClassMid(tree),tree.getHgt(),true /* "Inkl. grenar" */); 
//						AfxMessageBox(_T("Bok Hel stam + grenar"));
						break;
					case 2:	// "Klykstam"
						fM3Sk = setM3SkForBeech_forks(getClassMid(tree),tree.getHgt(),false /* "Inga grenar" */); 
//						AfxMessageBox(_T("Bok Klykstam"));
						break;
					case 3:	// "Klykstam + grenar"
						fM3Sk = setM3SkForBeech_forks(getClassMid(tree),tree.getHgt(),true /* "Inkl. grenar" */); 
//						AfxMessageBox(_T("Bok Klykstam + grenar"));
						break;
				};
			}
			else if (nVolSpcID == 2)	// Oak
			{
				// Also, in this function, call method depending on
				// selection from user. This is because we can get both
				// fub and sk; 070625 p�d
				switch (nVolIndex)
				{
					case 0:	// "Hel stam"
						fM3Sk = setM3SkForOak_whole_trunk(getClassMid(tree),tree.getHgt(),false /* "Inga grenar" */); 
//						AfxMessageBox(_T("Ek Hel stam"));
						break;
					case 1:	// "Hel stam + grenar"
						fM3Sk = setM3SkForOak_whole_trunk(getClassMid(tree),tree.getHgt(),true /* "Inkl. grenar" */); 
//						AfxMessageBox(_T("Ek Hel stam + grenar"));
						break;
					case 2:	// "Klykstam"
						fM3Sk = setM3SkForOak_forks(getClassMid(tree),tree.getHgt(),false /* "Inga grenar" */); 
//						AfxMessageBox(_T("Ek Klykstam"));
						break;
					case 3:	// "Klykstam + grenar"
						fM3Sk = setM3SkForOak_forks(getClassMid(tree),tree.getHgt(),true /* "Inkl. grenar" */); 
//						AfxMessageBox(_T("Ek Klykstam + grenar"));
						break;
				};
			}

			// Calculate M3Fub from M3Sk; 090305 p�d	
			fM3Fub = fM3Sk * fOmfValue;

			if (nNumOfTrees > 1)
			{
				fM3Sk	*= nNumOfTrees;
				fM3Fub	*= nNumOfTrees;
			}

			m_vecDCLSTrees[i].setM3fub(fM3Fub);
			m_vecDCLSTrees[i].setM3sk(fM3Sk);

		}	// if (tree.getSpcID() == nSpcID)

	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)


	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}

// Calculate volume under bark. Used in R. Ollars; 090305 p�d
BOOL CVolumeHagbergMarten::calculate_ub(vecTransactionTraktData& vec1,
																		 vecTransactionSampleTree& vec2,
																		 vecTransactionDCLSTree& vec4,
																		 vecTransactionTraktSetSpc& vec3)
{
//	CString S;
	double fM3Ub = 0.0;
	double fBarkThick = 0.0;	// "Dubbla barktjockleken"
	int nVolSpcID = -1;
	int nVolIndex = -1;
	int nSpcID = -1;
	int nNumOfTrees = 0;

	// Check that there's any species; 070418 p�d
	if (m_nNumOfSampleSpecies == 0)
		return FALSE;


	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nVolSpcID = getVolumeSpcID_ub();	
	nVolIndex = getVolumeIndex_ub();

	// Loop through the tree list (m_vecSampleTrees) and
	// calculate height for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getHeightID() to get which species
	// method to use and getHeightIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070424 p�d
	for (int i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		// Check for Specie; 070424 p�d
		if (tree.getSpcID() == nSpcID)
		{
			// "Dubbla barktjockleken"; 090305 p�d
			fBarkThick = tree.getBarkThick() * 2.0;	
			// Adaption for "Intr�ngsv�rdering":
			// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
			// OBS! This only allies to "Intr�ng"; 080307 p�d
			nNumOfTrees = tree.getNumOf() + tree.getNumOfRandTrees();
			// Select function depending on specie and
			// index of function within specie; 070625 p�d
			if (nVolSpcID == 1)	// Beech
			{
				// Also, in this function, call method depending on
				// selection from user. This is because we can get both
				// fub and sk; 070625 p�d
				switch (nVolIndex)
				{
					case 0:	// "Hel stam"
						fM3Ub = setM3FubForBeech_whole_trunk(getClassMid(tree)-fBarkThick,tree.getHgt(),false /* "Inga grenar" */); 
//						AfxMessageBox(_T("UB Bok Hel stam"));
						break;
					case 1:	// "Hel stam + grenar"
						fM3Ub = setM3FubForBeech_whole_trunk(getClassMid(tree)-fBarkThick,tree.getHgt(),true /* "Inkl. grenar" */); 
//						AfxMessageBox(_T("UB Bok Hel stam + grenar"));
						break;
					case 2:	// "Klykstam"
						fM3Ub = setM3FubForBeech_forks(getClassMid(tree)-fBarkThick,tree.getHgt(),false /* "Inga grenar" */); 
//						AfxMessageBox(_T("UB Bok Klykstam"));
						break;
					case 3:	// "Klykstam + grenar"
						fM3Ub = setM3FubForBeech_forks(getClassMid(tree)-fBarkThick,tree.getHgt(),true /* "Inkl. grenar" */); 
//						AfxMessageBox(_T("UB Bok Klykstam + grenar"));
						break;
				};
			}
			else if (nVolSpcID == 2)	// Oak
			{
				// Also, in this function, call method depending on
				// selection from user. This is because we can get both
				// fub and sk; 070625 p�d
				switch (nVolIndex)
				{
					case 0:	// "Hel stam"
						fM3Ub = setM3FubForOak_whole_trunk(getClassMid(tree)-fBarkThick,tree.getHgt(),false /* "Inga grenar" */); 
//						AfxMessageBox(_T("UB Ek Hel stam"));
						break;
					case 1:	// "Hel stam + grenar"
						fM3Ub = setM3FubForOak_whole_trunk(getClassMid(tree)-fBarkThick,tree.getHgt(),true /* "Inkl. grenar" */); 
//						AfxMessageBox(_T("UB Ek Hel stam + grenar"));
						break;
					case 2:	// "Klykstam"
						fM3Ub = setM3FubForOak_forks(getClassMid(tree)-fBarkThick,tree.getHgt(),false /* "Inga grenar" */); 
//						AfxMessageBox(_T("UB Ek Klykstam"));
						break;
					case 3:	// "Klykstam + grenar"
						fM3Ub = setM3FubForOak_forks(getClassMid(tree)-fBarkThick,tree.getHgt(),true /* "Inkl. grenar" */); 
//						AfxMessageBox(_T("UB Ek Klykstam + grenar"));
						break;
				};
			}

			if (nNumOfTrees > 1)
			{
				fM3Ub	*= nNumOfTrees;
			}

			m_vecDCLSTrees[i].setM3ub(fM3Ub);

		}	// if (tree.getSpcID() == nSpcID)

	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)


	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}
