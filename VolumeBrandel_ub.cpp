#include "StdAfx.h"

#include "VolumeBrandel_ub.h"
//----------------------------------------------------------------------------
//	Volumefunctions from the book : "Volymfunktioner f�renskilda tr�d 
//	av G�ran Brandel"
//----------------------------------------------------------------------------
/*
*     TALL
*/

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels mindre"; 021202 p�d
//	Page: 110; func: 200-01
//  Pine_Simple_N_ub[5]   = Pine (Tall) Norra	
//	Page: 104; func: 200-01
//  Pine_Simple_S_ub[5]   = Pine (Tall) S�dra	
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
const double Pine_Simple_N_ub[5]   = { -1.23242, 1.95242,-0.05839, 1.13440,-0.13476 };
const double Pine_Simple_S_ub[5]   = { -1.23602, 1.94126,-0.11924, 1.80842,-0.74261 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 142 func: 200-01	 FUNKTION SAKNAS
//  Pine_BG_63_LT_N_ub    =  Pine (Tall) + BG <63, Norra
//  Pine_BG_63_65_N_ub    =  Pine (Tall) + BG >=63 ... <65, Norra
//  Pine_BG_65_67_N_ub    =  Pine (Tall) + BG >=65 ... <67, Norra
//  Pine_BG_67_GT_N_ub    =  Pine (Tall) + BG >=67, Norra
//	Page: 138 func: 200-01	FUNKTION SAKNAS
//  Pine_BG_57_LT_S_ub    =  Pine (Tall) + BG <57, S�dra						
//  Pine_BG_57_59_S_ub    =  Pine (Tall) + BG >=57 ... <59, S�dra
//  Pine_BG_59_GT_S_ub    =  Pine (Tall) + BG >=59, S�dra
//----------------------------------------------------------------------------
//                                      a        b        c        d         e
/*
const double Pine_BG_63_LT_N_ub[5]    = {-1.40217, 1.96780,-0.11463, 1.70008, -0.60768 };
const double Pine_BG_63_65_N_ub[5]    = {-1.38595, 1.96780,-0.11463, 1.70008, -0.60768 };
const double Pine_BG_65_67_N_ub[5]    = {-1.37380, 1.96780,-0.11463, 1.70008, -0.60768 };
const double Pine_BG_67_GT_N_ub[5]    = {-1.37083, 1.96780,-0.11463, 1.70008, -0.60768 };

const double Pine_BG_57_LT_S_ub[5]    = {-1.06761, 2.03843, -0.53797, 2.84052, -1.54774 };
const double Pine_BG_57_59_S_ub[5]    = {-1.07272, 2.03843, -0.53797, 2.84052, -1.54774 };
const double Pine_BG_59_GT_S_ub[5]    = {-1.07118, 2.03843, -0.53797, 2.84052, -1.54774 };
*/

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG+KG)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 142 func: 200-02	FUNKTION SAKNAS
//  Pine_BG_KG_63_LT_N_ub    =  Pine (Tall) + BG <63, Norra
//  Pine_BG_KG_63_65_N_ub    =  Pine (Tall) + BG >=63 ... <65, Norra
//  Pine_BG_KG_65_67_N_ub    =  Pine (Tall) + BG >=65 ... <67, Norra
//  Pine_BG_KG_67_GT_N_ub    =  Pine (Tall) + BG >=67, Norra
//-----------------------------------------------------------------------------
//	Page: 138 func: 200-02	FUNKTION SAKNAS
//  Pine_BG_KG_57_LT_S_ub    =  Pine (Tall) + BG <57, S�dra
//  Pine_BG_KG_57_59_S_ub    =  Pine (Tall) + BG >=57 ... <59, S�dra
//  Pine_BG_KG_59_GT_S_ub    =  Pine (Tall) + BG >=59, S�dra
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
/*
const double Pine_BG_KG_63_LT_N_ub[6]  = {-1.33125, 2.02734,-0.18296, 1.81169, -0.79877, 0.05293 };
const double Pine_BG_KG_63_65_N_ub[6]  = {-1.31455, 2.02734,-0.18296, 1.81169, -0.79877, 0.05293 };
const double Pine_BG_KG_65_67_N_ub[6]  = {-1.30284, 2.02734,-0.18296, 1.81169, -0.79877, 0.05293 };
const double Pine_BG_KG_67_GT_N_ub[6]  = {-1.29765, 2.02734,-0.18296, 1.81169, -0.79877, 0.05293 };

const double Pine_BG_KG_57_LT_S_ub[6]  = {-1.37368, 1.85256, 0.05972, 2.04072, -1.06474, 0.03177 };
const double Pine_BG_KG_57_59_S_ub[6]  = {-1.38584, 1.85256, 0.05972, 2.04072, -1.06474, 0.03177 };
const double Pine_BG_KG_59_GT_S_ub[6]  = {-1.38066, 1.85256, 0.05972, 2.04072, -1.06474, 0.03177 };
*/

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (KG+BRK)"; 030829 p�d
//----------------------------------------------------------------------------
//	Page: 104 func: 200-04
//  Pine_KG_BRK_S_ub    =  Pine (Tall) + BG + Barkred.
//----------------------------------------------------------------------------
//                                    a        b        c        d        e      f         g

const double Pine_KG_BRK_S_ub[7]  = {-1.17618, 1.96576,-0.13599, 1.61678, -0.66662, 0.06204, 0.01333};

/*
*     GRAN
*/

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels mindre"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 112 func: 200-01
//  Spruce_Simple_N_ub[5] = Spruce (Gran) Norra
//	Page: 106 func: 200-01
//  Spruce_Simple_S_ub[5] = Spruce (Gran) S�dra
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
const double Spruce_Simple_N_ub[5] = { -0.77561, 2.06126,-0.77713, 3.27580,-1.90707 };
const double Spruce_Simple_S_ub[5] = { -1.07676, 1.97159,-0.42776, 2.84877,-1.58630 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (HH)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 137 func: 200-01
//  Spruce_HH_200_LT_N_ub[5]  = Height over sea <200
//  Spruce_HH_200_500_N_ub[5] = Height over sea >=200 ... <500
//  Spruce_HH_500_GT_N_ub[5]  = Height over sea >=500
//----------------------------------------------------------------------------
//                                        a        b         c        d         e
const double Spruce_HH_200_LT_N_ub[5]  = { -0.76202, 2.07263, -0.78064, 3.24914, -1.89541 };
const double Spruce_HH_200_500_N_ub[5] = { -0.76608, 2.07263, -0.78064, 3.24914, -1.89541 };
const double Spruce_HH_500_GT_N_ub[5]  = { -0.77410, 2.07263, -0.78064, 3.24914, -1.89541 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (HH+KG)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 137 func: 200-02
//  Spruce_HH_KG_200_LT_N_ub[5]  = Height over sea <200
//  Spruce_HH_KG_200_500_N_ub[5] = Height over sea >=200 ... <500
//  Spruce_HH_KG_500_GT_N_ub[5]  = Height over sea >=500
//----------------------------------------------------------------------------
//                                           a        b         c        d         e        f
const double Spruce_HH_KG_200_LT_N_ub[6]  = { -0.64161, 2.14844, -0.84787, 3.03897, -1.79534, 0.04111};
const double Spruce_HH_KG_200_500_N_ub[6] = { -0.64370, 2.14844, -0.84787, 3.03897, -1.79534, 0.04111};
const double Spruce_HH_KG_500_GT_N_ub[6]  = { -0.64832, 2.14844, -0.84787, 3.03897, -1.79534, 0.04111};

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 144 func: 200-01	FUNKTIONEN SAKNAS
//  Spruce_BG_63_LT_N_ub  =  Spruce (Gran) + BG <63, Norra
//  Spruce_BG_63_65_N_ub  =  Spruce (Gran) + BG >=63 ... <65, Norra
//  Spruce_BG_65_67_N_ub  =  Spruce (Gran) + BG >=65 ... <67, Norra
//  Spruce_BG_67_GT_N_ub  =  Spruce (Gran) + BG >=67, Norra
//----------------------------------------------------------------------------
//                                     a        b        c        d         e
/*
const double Spruce_BG_63_LT_N_ub[5] = {-0.78147, 2.14524,-0.91306, 3.41778, -1.99743 };
const double Spruce_BG_63_65_N_ub[5] = {-0.78943, 2.14524,-0.91306, 3.41778, -1.99743 };
const double Spruce_BG_65_67_N_ub[5] = {-0.79427, 2.14524,-0.91306, 3.41778, -1.99743 };
const double Spruce_BG_67_GT_N_ub[5] = {-0.80983, 2.14524,-0.91306, 3.41778, -1.99743 };
*/
//=================================================================================================
//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG+HH)"; 021203 p�d
//----------------------------------------------------------------------------
//	Page: 155 func: 200-01
//  Contants for Spruce (Gran) North : Height over sea = <200
//  Spruce_BG_HH_200_LT_63_LT_N_ub[6] = <63
//  Spruce_BG_HH_200_LT_63_65_N_ub[6] = >=63 ... <65
//  Spruce_BG_HH_200_LT_65_67_N_ub[6] = >=65 ... <67
//  Spruce_BG_HH_200_LT_67_GT_N_ub[6] = >=67
//----------------------------------------------------------------------------
//                                                a        b        c        d         e
const double Spruce_BG_HH_200_LT_63_LT_N_ub[5] = { -0.74346, 2.08706,-0.78814, 3.20560, -1.87006};
const double Spruce_BG_HH_200_LT_63_65_N_ub[5] = { -0.74666, 2.08706,-0.78814, 3.20560, -1.87006};
const double Spruce_BG_HH_200_LT_65_67_N_ub[5] = { -0.74751, 2.08706,-0.78814, 3.20560, -1.87006};
const double Spruce_BG_HH_200_LT_67_GT_N_ub[5] = { -0.75943, 2.08706,-0.78814, 3.20560, -1.87006};

//----------------------------------------------------------------------------
//  Contants for Spruce (Gran) North : Height over sea = >=200 ... <500
//----------------------------------------------------------------------------
//	Page: 155 func: 200-01
//  Spruce_BG_HH_500_200_63_LT_N_ub[5] = <63
//  Spruce_BG_HH_500_200_63_65_N_ub[5] = >=63 ... <65
//  Spruce_BG_HH_500_200_65_67_N_ub[5] = >=65 ... <67
//  Spruce_BG_HH_500_200_67_GT_N_ub[5] = >=67
//----------------------------------------------------------------------------
//                                                 a        b        c        d         e
const double Spruce_BG_HH_500_200_63_LT_N_ub[5] = { -0.74709, 2.08706,-0.78814, 3.20560, -1.87006};
const double Spruce_BG_HH_500_200_63_65_N_ub[5] = { -0.75029, 2.08706,-0.78814, 3.20560, -1.87006};
const double Spruce_BG_HH_500_200_65_67_N_ub[5] = { -0.75114, 2.08706,-0.78814, 3.20560, -1.87006};
const double Spruce_BG_HH_500_200_67_GT_N_ub[5] = { -0.76306, 2.08706,-0.78814, 3.20560, -1.87006};

//----------------------------------------------------------------------------
//  Contants for Spruce (Gran) North : Height over sea = >500
//----------------------------------------------------------------------------
//	Page: 154 func: 100-01
//  Spruce_BG_HH_500_GT_63_LT_N_ub[5] = <63
//  Spruce_BG_HH_500_GT_63_65_N_ub[5] = >=63 ... <65
//  Spruce_BG_HH_500_GT_65_67_N_ub[5] = >=65 ... <67
//  Spruce_BG_HH_500_GT_67_GT_N_ub[5] = >=67
//----------------------------------------------------------------------------

//                                                a        b        c        d         e
const double Spruce_BG_HH_500_GT_63_LT_N_ub[5] = { -0.75667, 2.08706,-0.78814, 3.20560, -1.87006};
const double Spruce_BG_HH_500_GT_63_65_N_ub[5] = { -0.75987, 2.08706,-0.78814, 3.20560, -1.87006};
const double Spruce_BG_HH_500_GT_65_67_N_ub[5] = { -0.76072, 2.08706,-0.78814, 3.20560, -1.87006};
const double Spruce_BG_HH_500_GT_67_GT_N_ub[5] = { -0.77264, 2.08706,-0.78814, 3.20560, -1.87006};

//=================================================================================================

//----------------------------------------------------------------------------
//  Contants for Spruce (Gran) South "Brandels (KG)"
//----------------------------------------------------------------------------
//	Page: 106 func: 200-02
//  Spruce_KG_S_ub[6]
//----------------------------------------------------------------------------
//                               a        b        c        d         e				 f
const double Spruce_KG_S_ub[6] = { -0.99639, 2.02342,-0.46158, 2.66057, -1.49433, 0.04050};


/*
*     BJ�RK
*/

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels mindre"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 114 func: 200-01
//  Birch_Simple_N_ub[5]  = Birch (Bj�rk) Norra
//	Page: 108 func: 200-01
//  Birch_Simple_S_ub[5]  = Birch (Bj�rk) S�dra
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
const double Birch_Simple_N_ub[5]  = { -0.72541, 2.36594,-1.10578, 4.76151,-3.40177 };
const double Birch_Simple_S_ub[5]  = { -1.09667, 2.20855,-0.85821, 5.81764,-4.34685 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG)"; 021203 p�d
//----------------------------------------------------------------------------
//	Page: 140 func: 200-01
//  Birch_BG_57_LT_S_ub[5] = <57
//  Birch_BG_57_59_S_ub[5] = >=57 ... <59
//  Birch_BG_59_GT_S_ub[5] = >=59
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
const double Birch_BG_57_LT_S_ub[5] = {-1.09588, 2.17999,-0.78580, 5.08267,-3.68585 };
const double Birch_BG_57_59_S_ub[5] = {-1.06101, 2.17999,-0.78580, 5.08267,-3.68585 };
const double Birch_BG_59_GT_S_ub[5] = {-1.05775, 2.17999,-0.78580, 5.08267,-3.68585 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG+KG)"; 021203 p�d
//----------------------------------------------------------------------------
//	Page: 140 func: 200-02	FUNKTIONEN SAKNAS
//  Birch_BG_KG_57_LT_S_ub[5] = <57
//  Birch_BG_KG_57_59_S_ub[5] = >=57 ... <59
//  Birch_BG_KG_59_GT_S_ub[5] = >=59
//----------------------------------------------------------------------------
//                                       a        b        c        d        e        f
/*
const double Birch_BG_KG_57_LT_S_ub[6] = {-1.00845, 2.17641,-1.14585, 6.20506,-4.49068,-0.04436};
const double Birch_BG_KG_57_59_S_ub[6] = {-0.95590, 2.17641,-1.14585, 6.20506,-4.49068,-0.04436};
const double Birch_BG_KG_59_GT_S_ub[6] = {-0.93856, 2.17641,-1.14585, 6.20506,-4.49068,-0.04436};
*/
//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (KG)"; 030903 p�d
//----------------------------------------------------------------------------
//	Page: 108 func: 200-02
//  Birch_KG_S_ub[5]
//----------------------------------------------------------------------------
//                              a        b        c        d        e        f
const double Birch_KG_S_ub[6] = {-1.12793, 2.18389,-0.83356, 5.77102,-4.26554,-0.01992};

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BRK)"; 030829 p�d
//----------------------------------------------------------------------------
//	Page: 108 func: 200-03
//  Birch_BRK_S_ub[5]
//----------------------------------------------------------------------------
//                              a        b        c        d        e        f

const double Birch_BRK_S_ub[6] = {-1.13118, 2.23767,-0.80618, 5.64812,-4.20660,-0.03458};


////////////////////////////////////////////////////////////////////////////////////////
// CVolumeBrandel_ub


/********************************************************************/
//  HANDLE CALCULATEIONS; 021202 p�d
/********************************************************************/
// Calculate volume
// Formula on page 26 (01)
double CVolumeBrandel_ub::getCalcSimple_ub(double hgt,double dbh,const double *const_val)
{
	if (dbh > 0.0 && hgt > 0.0 && (hgt-1.3) > 0.0)
	{
      double f_a  = pow(10.0,(double)(const_val[0]));
      double f_b  = pow((double)dbh,(double)(const_val[1]));
      double f_c  = pow((double)(dbh + 20.0),(double)(const_val[2]));
      double f_d  = pow((double)hgt,(double)(const_val[3]));
      double f_e  = pow((double)(hgt - 1.3),(double)(const_val[4]));
		
      return (f_a * f_b * f_c * f_d * f_e)/1000.0;  // From dm3 to m3; 020417 p�d
	}
	else
      return 0.0;
}

// Calculate volume using "Krongr�ns"
// Formula on page 27 (02)
double CVolumeBrandel_ub::getCalcKG_ub(double hgt,double dbh,double kg,const double *const_val)
{
	if (dbh > 0.0 && hgt > 0.0 && (hgt-1.3) > 0.0 && kg > 0.0)
	{
		double fCalcKG = (hgt - (hgt*(kg/100.0)));
    double f_a  = pow(10.0                ,(double)(const_val[0]));
    double f_b  = pow((double)dbh         ,(double)(const_val[1]));
	  double f_c  = pow((double)(dbh + 20.0),(double)(const_val[2]));
    double f_d  = pow((double)hgt         ,(double)(const_val[3]));
    double f_e  = pow((double)(hgt - 1.3) ,(double)(const_val[4]));
    double f_f  = pow((double)fCalcKG			,(double)(const_val[5]));
		
    return (f_a * f_b * f_c * f_d * f_e * f_f)/1000.0;  // From dm3 to m3; 021129 p�d
	}
	else
      return 0.0;
}

// Calculate volume using "Barkthickness"
// Formula on page 27 (02)
double CVolumeBrandel_ub::getCalcBRK_ub(double hgt,double dbh,double brk,const double *const_val)
{
	if (dbh > 0.0 && hgt > 0.0 && (hgt-1.3) > 0.0 && brk > 0.0)
	{
      double f_a  = pow(10.0                 ,(double)(const_val[0]));
      double f_b  = pow((double)dbh          ,(double)(const_val[1]));
      double f_c  = pow((double)(dbh + 20.0) ,(double)(const_val[2]));
      double f_d  = pow((double)hgt          ,(double)(const_val[3]));
      double f_e  = pow((double)(hgt - 1.3)  ,(double)(const_val[4]));
      double f_f  = pow((double)(brk*2.0)		 ,(double)(const_val[5]));
		
      return (f_a * f_b * f_c * f_d * f_e * f_f)/1000.0;  // From dm3 to m3; 021129 p�d
	}
	else
      return 0.0;
}

// Calculate volume using "Krongr�ns" and "Barkthickness".
// Formula on page 27 (04)
double CVolumeBrandel_ub::getCalcKG_BRK_ub(double hgt,double dbh,double kg,double brk,const double *const_val)
{
	if (dbh > 0.0 && hgt > 0.0 && (hgt-1.3) > 0.0 && brk > 0.0)
	{
		double fCalcKG = (hgt - (hgt*(kg/100.0)));
    double f_a  = pow(10.0								,(double)(const_val[0]));
    double f_b  = pow((double)dbh					,(double)(const_val[1]));
    double f_c  = pow((double)(dbh + 20.0),(double)(const_val[2]));
    double f_d  = pow((double)hgt					,(double)(const_val[3]));
    double f_e  = pow((double)(hgt - 1.3)	,(double)(const_val[4]));
    double f_f  = pow((double)(fCalcKG)		,(double)(const_val[5]));
    double f_g  = pow((double)(brk*2.0)		,(double)(const_val[6]));
		
    return (f_a * f_b * f_c * f_d * f_e * f_f * f_g)/1000.0;  // From dm3 to m3; 030829 p�d
	}
	else
      return 0.0;
}


//================================================================
//  Brandels volume calculation; 070412 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  hgt_sea = Height over sea
//  lng     = Longitude for tree
//  kg      = GreenCrown (Krongr�ns)
//	bark		= Barkavdrag
//================================================================
double CVolumeBrandel_ub::getVolumeForPine_ub(int n,double dbh,double hgt,int hgt_sea,int lng,double kg,double brk)
{
	switch (n)
	{
		//**************************************************
		// "Brandels, mindre, Tall , Norra"
		//**************************************************
		case 0 :
		{
			return getCalcSimple_ub(hgt,dbh,Pine_Simple_N_ub);
		} /* case 0 */
		//**************************************************
		// "Brandels, mindre, Tall , S�dra"
		//**************************************************
		case 1 :
		{
			return getCalcSimple_ub(hgt,dbh,Pine_Simple_S_ub);
		} /* case 1 */
/*
		//**************************************************
		// "Brandels, (BG), Tall , Norra"
		//**************************************************
		case 2 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 63)
				return getCalcSimple_ub(hgt,dbh,Pine_BG_63_LT_N_ub);
			else if (lng >= 63 && lng < 65)
				return getCalcSimple_ub(hgt,dbh,Pine_BG_63_65_N_ub);
			else if (lng >= 65 && lng < 67)
				return getCalcSimple_ub(hgt,dbh,Pine_BG_65_67_N_ub);
			else if (lng >= 67)
				return getCalcSimple_ub(hgt,dbh,Pine_BG_67_GT_N_ub);
		} /* case 2 * /

		//**************************************************
		// "Brandels, (BG), Tall , S�dra"
		//**************************************************
		case 3 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 57)
				return getCalcSimple_ub(hgt,dbh,Pine_BG_57_LT_S_ub);
			else if (lng >= 57 && lng < 59)
				return getCalcSimple_ub(hgt,dbh,Pine_BG_57_59_S_ub);
			else if (lng >= 59)
				return getCalcSimple_ub(hgt,dbh,Pine_BG_59_GT_S_ub);
		} /* case 3 * /
		
		//**************************************************
		// "Brandels, (BG+KG), Tall , Norra"
		//**************************************************
		case 2 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG ("krongr�ns"), same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 63)
				return getCalcKG_ub(hgt,dbh,kg,Pine_BG_KG_63_LT_N_ub);
			else if (lng >= 63 && lng < 65)
				return getCalcKG_ub(hgt,dbh,kg,Pine_BG_KG_63_65_N_ub);
			else if (lng >= 65 && lng < 67)
				return getCalcKG_ub(hgt,dbh,kg,Pine_BG_KG_65_67_N_ub);
			else if (lng >= 67)
				return getCalcKG_ub(hgt,dbh,kg,Pine_BG_KG_67_GT_N_ub);
		} /* case 4 * /

	No costants for m3ub
		//**************************************************
		// "Brandels, (BG+KG), Tall , S�dra"
		//**************************************************
		case 5 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG ("krongr�ns"), same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 57)
				return getCalcKG_ub(hgt,dbh,kg,Pine_BG_KG_57_LT_S_ub);
			else if (lng >= 57 && lng < 59)
				return getCalcKG_ub(hgt,dbh,kg,Pine_BG_KG_57_59_S_ub);
			else if (lng >= 59)
				return getCalcKG_ub(hgt,dbh,kg,Pine_BG_KG_59_GT_S_ub);
		} /* case 5 * /
*/		
		//**************************************************
		// "Brandels, (KG+BRK), Tall , S�dra"
		//**************************************************
		case 2 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG ("krongr�ns"); 030829 p�d
			//---------------------------------------------------------
			return getCalcKG_BRK_ub(hgt,dbh,kg,brk,Pine_KG_BRK_S_ub);
		} /* case 6 */

	}	// switch
    return 0.0;
}

//================================================================
//  Brandels volume calculation; 070412 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  hgt_sea = Height over sea
//  lng     = Longitude for tree
//  kg      = GreenCrown (Krongr�ns)
//	bark		= Barkavdrag
//================================================================
double CVolumeBrandel_ub::getVolumeForSpruce_ub(int n,double dbh,double hgt,int hgt_sea,int lng,double kg,double brk)
{
	switch (n)
	{
		//**************************************************
		// "Brandels, mindre, Gran , Norra"
		//**************************************************
		case 0 :
		{
			return getCalcSimple_ub(hgt,dbh,Spruce_Simple_N_ub);
		} /* case 0 */
		//**************************************************
		// "Brandels, mindre, Gran , S�dra"
		//**************************************************
		case 1 :
		{
			return getCalcSimple_ub(hgt,dbh,Spruce_Simple_S_ub);
		} /* case 1 */
		
		//**************************************************
		// "Brandels, (HH),  Gran , Norra"
		//**************************************************
		case 2 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "H�jd �ver havet"; 021202 p�d
			//---------------------------------------------------------
			if (hgt_sea < 200)
				return getCalcSimple_ub(hgt,dbh,Spruce_HH_200_LT_N_ub);
			else if (hgt_sea >= 200 && hgt_sea < 500)
				return getCalcSimple_ub(hgt,dbh,Spruce_HH_200_500_N_ub);
			else if (hgt_sea >= 500)
				return getCalcSimple_ub(hgt,dbh,Spruce_HH_500_GT_N_ub);
		} /* case 2 */
		
		//**************************************************
		// "Brandels, (HH+KG),  Gran , Norra"
		//**************************************************
		case 3 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG, based on "H�jd �ver havet"
			//  and "Krongr�ns"; 021202 p�d
			//---------------------------------------------------------
			if (hgt_sea < 200)
				return getCalcKG_ub(hgt,dbh,kg,Spruce_HH_KG_200_LT_N_ub);
			else if (hgt_sea >= 200 && hgt_sea < 500)
				return getCalcKG_ub(hgt,dbh,kg,Spruce_HH_KG_200_500_N_ub);
			else if (hgt_sea >= 500)
				return getCalcKG_ub(hgt,dbh,kg,Spruce_HH_KG_500_GT_N_ub);
		} /* case 3 */
/*		
		//**************************************************
		// "Brandels, (BG),  Gran , Norra"
		//**************************************************
		case 4 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 63)
				return getCalcSimple_ub(hgt,dbh,Spruce_BG_63_LT_N_ub);
			else if (lng >= 63 && lng < 65)
				return getCalcSimple_ub(hgt,dbh,Spruce_BG_63_65_N_ub);
			else if (lng >= 65 && lng < 67)
				return getCalcSimple_ub(hgt,dbh,Spruce_BG_65_67_N_ub);
			else if (lng >= 67)
				return getCalcSimple_ub(hgt,dbh,Spruce_BG_67_GT_N_ub);
		} /* case 4 * /
*/		
		//**************************************************
		// "Brandels, (BG+HH),  Gran , Norra"
		//**************************************************
		case 4 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "H�jd �ver havet"
			//  and "Breddgrad"; 021203 p�d
			//---------------------------------------------------------
			if (hgt_sea < 200)
			{
				if (lng < 63)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_200_LT_63_LT_N_ub);
				else if (lng >= 63 && lng < 65)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_200_LT_63_65_N_ub);
				else if (lng >= 65 && lng < 67)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_200_LT_65_67_N_ub);
				else if (lng >= 67)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_200_LT_67_GT_N_ub);
			}
			else if (hgt_sea >= 200 && hgt_sea < 500)
			{
				if (lng < 63)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_500_200_63_LT_N_ub);
				else if (lng >= 63 && lng < 65)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_500_200_63_65_N_ub);
				else if (lng >= 65 && lng < 67)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_500_200_65_67_N_ub);
				else if (lng >= 67)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_500_200_67_GT_N_ub);
			}
			else if (hgt_sea >= 500)
			{
				if (lng < 63)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_500_GT_63_LT_N_ub);
				else if (lng >= 63 && lng < 65)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_500_GT_63_65_N_ub);
				else if (lng >= 65 && lng < 67)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_500_GT_65_67_N_ub);
				else if (lng >= 67)
					return getCalcSimple_ub(hgt,dbh,Spruce_BG_HH_500_GT_67_GT_N_ub);
			}
		} /* case 5 */
		//**************************************************
		// "Brandels, (KG+Bark),  Gran , S�dra"
		//**************************************************
		case 5 :
		{
			return getCalcKG_ub(hgt,dbh,kg,Spruce_KG_S_ub);
		}	/* Case 6 */		
  } // switch ((*n))
	
    return 0.0;
}

//================================================================
//  Brandels volume calculation; 070412 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  hgt_sea = Height over sea
//  lng     = Longitude for tree
//  kg      = GreenCrown (Krongr�ns)
//	brk			= Barkavdrag
//================================================================
double CVolumeBrandel_ub::getVolumeForBirch_ub(int n,double dbh,double hgt,int hgt_sea,int lng,double kg,double brk)
{
	switch (n)
	{
		//**************************************************
		// "Brandels, mindre, Bj�rk, Norra"
		//**************************************************
		case 0 :
		{
			return getCalcSimple_ub(hgt,dbh,Birch_Simple_N_ub);
		} /* case 0 */
		//**************************************************
		// "Brandels, mindre, Bj�rk, S�dra"
		//**************************************************
		case 1 :
		{
			return getCalcSimple_ub(hgt,dbh,Birch_Simple_S_ub);
		} /* case 1 */
		
		//**************************************************
		// "Brandels, (BG), Bj�rk, S�dra"
		//**************************************************
		case 2 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 57)
				return getCalcSimple_ub(hgt,dbh,Birch_BG_57_LT_S_ub);
			else if (lng >= 57 && lng < 59)
				return getCalcSimple_ub(hgt,dbh,Birch_BG_57_59_S_ub);
			else if (lng >= 59)
				return getCalcSimple_ub(hgt,dbh,Birch_BG_59_GT_S_ub);
		} /* case 2 */
/*		
		//**************************************************
		// "Brandels, (KG), Bj�rk, S�dra"
		//**************************************************
		case 3 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG, based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 57)
			{
				return getCalcKG_ub(hgt,dbh,kg,Birch_BG_KG_57_LT_S_ub);
			}
			else if (lng >= 57 && lng < 59)
			{
				return getCalcKG_ub(hgt,dbh,kg,Birch_BG_KG_57_59_S_ub);
			}
			else if (lng >= 59)
			{
				return getCalcKG_ub(hgt,dbh,kg,Birch_BG_KG_59_GT_S_ub);
			}
		} /* case 3 * /
*/
		//**************************************************
		// "Brandels, (KG),  Bj�rk , S�dra"
		//**************************************************
		case 3 :
		{
			return getCalcKG_ub(hgt,dbh,kg,Birch_KG_S_ub);
		}	/* Case 3 */		

		//**************************************************
		// "Brandels, (BRK),  Bj�rk , S�dra"
		//**************************************************
		case 4 :
		{
			return getCalcBRK_ub(hgt,dbh,brk,Birch_BRK_S_ub);
		}	/* Case 4 */		

   } // switch ((*n))
	
   return 0.0;
}

// PUBLIC:
CVolumeBrandel_ub::CVolumeBrandel_ub(void)
	: CCalculationBaseClass()
{
}

CVolumeBrandel_ub::CVolumeBrandel_ub(int spc_index,CTransaction_trakt rec1,
																						 CTransaction_trakt_misc_data rec2,
																						 vecTransactionTraktData &tdata_list,
																						 vecTransactionSampleTree &sample_tree_list,
																						 vecTransactionDCLSTree &dcls_tree_list,
																						 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}


BOOL CVolumeBrandel_ub::calculate(vecTransactionTraktData &vec1,
																	vecTransactionSampleTree &vec2,
																	vecTransactionDCLSTree &vec4,		// Added 070820 p�d
																	vecTransactionTraktSetSpc& vec3)
{
	double f_dcls_mid = 0.0;
	double f_dcls_mid_ub = 0.0;
	double f_bark_reduction = 0.0;

	double fM3Ub = 0.0;
	double fGreenCrown = 0.0;
	int nNumOfTrees = 0;	// Number of trees in Duameterclass
	int nVolSpcID = -1;
	int nVolIndex = -1;
	int nSpcID = -1;
	// Check that there's any species; 070418 p�d
	if (m_nNumOfDCLSSpecies == 0)
		return FALSE;

	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nVolSpcID = getVolumeSpcID_ub();	
	nVolIndex = getVolumeIndex_ub();

	// Loop through the tree list (m_vecSampleTrees) and
	// calculate volume for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getVolumeID() to get which species
	// method to use and getVolumeIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070417 p�d
	for (long i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		if (tree.getSpcID() == nSpcID)
		{
			// Adaption for "Intr�ngsv�rdering":
			// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
			// OBS! This only allies to "Intr�ng"; 080307 p�d
			nNumOfTrees = tree.getNumOf() + tree.getNumOfRandTrees();
			// Get "Gr�nkroneprocent f�r tr�dslag"; 080625 p�d
			fGreenCrown = getGreenCrownPercForSpc(tree.getSpcID());
			// Diameter for calculation of volume under bark must be
			// under bark. I.e. use bark reduction; 071023 p�d
			f_dcls_mid = getClassMid(tree)/10.0;
			f_bark_reduction = tree.getBarkThick()*2.0;	// Double bark thickess; 071023 p�d
			f_dcls_mid_ub = f_dcls_mid - f_bark_reduction/10.0;

			// Depending on specie, run function accoring to
			// nBarkIndex value; 070417 p�d
			if (nVolSpcID == 1)	// Tall
			{
				fM3Ub = getVolumeForPine_ub(nVolIndex,f_dcls_mid_ub,	// dm
																					 tree.getHgt()/10.0,	// m
																					 getTrakt().getTraktHgtOverSea(),
																					 getTrakt().getTraktLatitude(),
																					 fGreenCrown, //tree.getGCrownPerc(),
																					 tree.getBarkThick());
			}	// if (nVolSpcID == 1)	Tall
			if (nVolSpcID == 2) // Gran
			{
				fM3Ub = getVolumeForSpruce_ub(nVolIndex,f_dcls_mid_ub,	// dm
																						 tree.getHgt()/10.0,	// m
																						 getTrakt().getTraktHgtOverSea(),
																						 getTrakt().getTraktLatitude(),
																						 fGreenCrown, //tree.getGCrownPerc(),
																						 tree.getBarkThick());
			}	// if (nVolSpcID == 2) Gran
			if (nVolSpcID == 3)	// Bj�rk
			{
				fM3Ub = getVolumeForBirch_ub(nVolIndex,f_dcls_mid_ub,	// dm
																						tree.getHgt()/10.0,	// m
																						getTrakt().getTraktHgtOverSea(),
																						getTrakt().getTraktLatitude(),
																						fGreenCrown, //tree.getGCrownPerc(),
																						tree.getBarkThick());
			}	// if (nVolSpcID == 3)	Bj�rk

			if (nNumOfTrees > 0)
			{
				fM3Ub	*= nNumOfTrees;
			}

			m_vecDCLSTrees[i].setM3ub(fM3Ub);

			// Also set Calculated "Gr�nkrona", from SampleTrees/Specie; 080625 p�d
			setTraktData_gcrown(nSpcID,fGreenCrown);

/* FOR DEBUG
			CString S;
			S.Format(_T("CVolumeBrandel_ub::calculate\n\nfM3ub %f"),fM3Ub);
			AfxMessageBox(S);
*/

		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}