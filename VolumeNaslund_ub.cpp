#include "StdAfx.h"

#include "VolumeNaslund_ub.h"

//----------------------------------------------------------------
//  Constans for volume function "N�slunds"
//----------------------------------------------------------------

/*
*     TALL
*/
//                                 a        b        c         d
const double Pine_Large_N_ub[4]  = { 0.06059, 0.03153, 0.007919, 0.001773 };
const double Pine_Small_N_ub[3]  = { 0.05491, 0.03641, 0.002699 };
const double Pine_Large_S_ub[5]  = { 0.07141, 0.02580, 0.009430, 0.003511, 0.001052 };
const double Pine_Small_S_ub[3]  = { 0.06271, 0.03208, 0.005725 };


/*
*     GRAN
*/
//                                   a        b        c         d        f
const double Spruce_Large_N_ub[5]  = { 0.10570, 0.01658, 0.006267, 0.01782, 0.04681 };
const double Spruce_Small_N_ub[4]  = { 0.11530, 0.01522, 0.02170 , 0.05501 };
const double Spruce_Large_S_ub[5]  = { 0.10390, 0.01959, 0.005942, 0.01417, 0.04332 };
const double Spruce_Small_S_ub[4]  = { 0.10760, 0.01929, 0.01723 , 0.04615 };

/*
*     BJ�RK
*/
//                                   a        b        c         d        f
const double Birch_Large_N_ub[4]  = { 0.03328, 0.028760, 0.002991, 0.003695 };
const double Birch_Small_N_ub[3]  = { 0.02703, 0.030230, 0.004346 };
const double Birch_Large_S_ub[5]  = { 0.08953, 0.021010, 0.011710, 0.031890, 0.0007244};
const double Birch_Small_S_ub[4]  = { 0.09944, 0.01862, 0.012780, 0.03544};



/********************************************************************/
//  HANDLE CALCULATEIONS; 070413 p�d
/********************************************************************/
//---------------------------------------------
//  Fromula: d2 + d2h + d2k - dhb
//---------------------------------------------
double CVolumeNaslund_ub::getCalcUB_1(double dbh,double hgt,double kg,double bark,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
	//  if (dbh == 0.0 || hgt <= 1.3 || kg == 0.0 || bark == 0.0) return 0.0;

	// kg in percent of total height; 071023 p�d
  double f_kg   = (hgt - hgt*(kg/100.0));

	// Diameter for calculation of volume under bark must be
	// under bark. I.e. use bark reduction; 071023 p�d
	double f_dbh_ub = dbh;

  double f_d2   = pow(f_dbh_ub, 2) * const_val[0];
  double f_d2h  = pow(f_dbh_ub, 2) * hgt * const_val[1];
  double f_d2k  = pow(f_dbh_ub, 2) * f_kg * const_val[2];
  double f_dbh  = f_dbh_ub * hgt * bark * const_val[3];

  return (f_d2 + f_d2h + f_d2k + f_dbh)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + d2k + dh2 + dhb
//---------------------------------------------
double CVolumeNaslund_ub::getCalcUB_2(double dbh,double hgt,double kg,double bark,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  //	if (dbh == 0.0 || hgt <= 1.3 || kg == 0.0 || bark == 0.0) return 0.0;

	// kg in percent of total height; 071023 p�d
  double f_kg   = (hgt-hgt*(kg/100.0));
	// Diameter for calculation of volume under bark must be
	// under bark. I.e. use bark reduction; 071023 p�d
	double f_dbh_ub = dbh;

	double f_d2   = pow(f_dbh_ub, 2) * const_val[0];
  double f_d2h  = pow(f_dbh_ub, 2) * hgt * const_val[1];
  double f_d2k  = pow(f_dbh_ub, 2) * f_kg * const_val[2];
  double f_dh2  = f_dbh_ub * pow(hgt,2) * const_val[3];
  double f_dbh  = f_dbh_ub*hgt*bark * const_val[4];

  return (f_d2 + f_d2h + f_d2k + f_dh2 + f_dbh)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + dh2
//---------------------------------------------
double CVolumeNaslund_ub::getCalcUB_3(double dbh,double hgt,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3) return 0.0;

	// Diameter for calculation of volume under bark must be
	// under bark. I.e. use bark reduction; 071023 p�d
	double f_dbh_ub = dbh;

  double f_d2   = pow(f_dbh_ub, 2) * const_val[0];
  double f_d2h  = pow(f_dbh_ub, 2) * hgt * const_val[1];
  double f_dh2  = f_dbh_ub * pow(hgt,2) * const_val[2];

/*
	CString S;
	S.Format("f_dbh_ub %f\nhgt %f\nconst_val[0] %f\nconst_val[1] %f\nconst_val[2] %f",
			f_dbh_ub,hgt,const_val[0],const_val[1],const_val[2]);
	AfxMessageBox(S);
*/

  return (f_d2 + f_d2h + f_dh2)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + d2k + dh2 - h2
//---------------------------------------------
double CVolumeNaslund_ub::getCalcUB_4(double dbh,double hgt,double kg,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3 || kg == 0.0) return 0.0;

	// kg in percent of total height; 071023 p�d
  double f_kg   = (hgt-hgt*(kg/100.0));
	// Diameter for calculation of volume under bark must be
	// under bark. I.e. use bark reduction; 071023 p�d
	double f_dbh_ub = dbh;

	double f_d2   = pow(f_dbh_ub, 2) * const_val[0];
  double f_d2h  = pow(f_dbh_ub, 2) * hgt * const_val[1];
  double f_d2k  = pow(f_dbh_ub, 2) * f_kg * const_val[2];
  double f_dh2  = f_dbh_ub * pow(hgt,2) * const_val[3];
  double f_h2   = pow(hgt, 2) * const_val[4];

  return (f_d2 + f_d2h + f_d2k + f_dh2 - f_h2)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + dh2 - h2
//---------------------------------------------
double CVolumeNaslund_ub::getCalcUB_5(double dbh,double hgt,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3) return 0.0;

	// Diameter for calculation of volume under bark must be
	// under bark. I.e. use bark reduction; 071023 p�d
	double f_dbh_ub = dbh;

  double f_d2   = pow(f_dbh_ub, 2) * const_val[0];
  double f_d2h  = pow(f_dbh_ub, 2) * hgt * const_val[1];
  double f_dh2  = f_dbh_ub * pow(hgt,2) * const_val[2];
  double f_h2   = pow(hgt, 2) * const_val[3];

  return (f_d2 + f_d2h + f_dh2 - f_h2)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + dh2 - h2 - dbh
//---------------------------------------------
double CVolumeNaslund_ub::getCalcUB_6(double dbh,double hgt,double bark,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3 || bark == 0.0) return 0.0;

	// Diameter for calculation of volume under bark must be
	// under bark. I.e. use bark reduction; 071023 p�d
	double f_dbh_ub = dbh;

  double f_d2   = pow(f_dbh_ub, 2) * const_val[0];
  double f_d2h  = pow(f_dbh_ub, 2) * hgt * const_val[1];
  double f_dh2  = f_dbh_ub * pow(hgt,2) * const_val[2];
  double f_h2   = pow(hgt, 2) * const_val[3];
  double f_dbh  = f_dbh_ub*hgt*bark * const_val[4];

  return (f_d2 + f_d2h + f_dh2 - f_h2 - f_dbh)/1000.0; // From dm3 to m3
}

//---------------------------------------------
//  Fromula: d2 + d2h + d2k + dh2
//---------------------------------------------
double CVolumeNaslund_ub::getCalcUB_7(double dbh,double hgt,double kg,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3 || kg == 0.0) return 0.0;

	// kg in percent of total height; 071023 p�d
  double f_kg   = (hgt-hgt*(kg/100.0));

	// Diameter for calculation of volume under bark must be
	// under bark. I.e. use bark reduction; 071023 p�d
	double f_dbh_ub = dbh;

  double f_d2   = pow(f_dbh_ub, 2) * const_val[0];
  double f_d2h  = pow(f_dbh_ub, 2) * hgt * const_val[1];
	double f_d2k  = pow(f_dbh_ub, 2) * f_kg * const_val[2];
  double f_dh2  = f_dbh_ub * pow(hgt,2) * const_val[3];

  return (f_d2 + f_d2h + f_d2k + f_dh2)/1000.0; // From dm3 to m3
}


//================================================================
//  N�slunds volume calculation; 070413 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  kg      = GreenCrown (Krongr�ns)
//  bark    = bark reduction (barkavdrag)
//================================================================
double CVolumeNaslund_ub::getVolumeForPine_ub(int n,double dbh,double hgt,double kg,double bark)
{
	switch (n)
	{
		//**************************************************
    //  "N�slunds mindre, Tall, Norra"; 070413 p�d
		//**************************************************
		case 0 :
    {
			return getCalcUB_3(dbh,hgt,Pine_Small_N_ub);
    }
	
		//**************************************************
    //  "N�slunds mindre, Tall, S�dra"; 070413 p�d
		//**************************************************
		case 1 :
    {
			return getCalcUB_3(dbh,hgt,Pine_Small_S_ub);
    }
	
		//**************************************************
    //  "N�slunds st�rre, Tall, Norra"; 070413 p�d
		//**************************************************
		case 2 :
    {
			return getCalcUB_1(dbh,hgt,kg,bark,Pine_Large_N_ub);
    }
			
		//**************************************************
    //  "N�slunds st�rre, Tall, S�dra"; 070413 p�d
		//**************************************************
		case 3 :
    {
			return getCalcUB_2(dbh,hgt,kg,bark,Pine_Large_S_ub);
    }
		
	}
	return 0.0;
}

//================================================================
//  N�slunds volume calculation; 070413 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  kg      = GreenCrown (Krongr�ns)
//  bark    = bark reduction (barkavdrag)
//================================================================
double CVolumeNaslund_ub::getVolumeForSpruce_ub(int n,double dbh,double hgt,double kg,double bark)
{
	switch (n)
	{

		//**************************************************
    //  "N�slunds mindre, Gran, Norra"; 070413 p�d
		//**************************************************
		case 0 :
    {
			return getCalcUB_5(dbh,hgt,Spruce_Small_N_ub);
    }
	
		//**************************************************
    //  "N�slunds mindre, Gran, S�dra"; 070413 p�d
		//**************************************************
		case 1 :
    {
			return getCalcUB_5(dbh,hgt,Spruce_Small_S_ub);
    }
	
		//**************************************************
    //  "N�slunds st�rre, Gran, Norra"; 070413 p�d
		//**************************************************
		case 2 :
    {
			return getCalcUB_4(dbh,hgt,kg,Spruce_Large_N_ub);
    }
			
		//**************************************************
    //  "N�slunds st�rre, Gran, S�dra"; 070413 p�d
		//**************************************************
		case 3 :
    {
			return getCalcUB_4(dbh,hgt,kg,Spruce_Large_S_ub);
    }
	}
	return 0.0;
}



//================================================================
//  N�slunds volume calculation; 070413 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  kg      = GreenCrown (Krongr�ns)
//  bark    = bark reduction (barkavdrag)
//================================================================
double CVolumeNaslund_ub::getVolumeForBirch_ub(int n,double dbh,double hgt,double kg,double bark)
{
	switch (n)
	{

		//**************************************************
    //  "N�slunds mindre, Bj�rk, Norra"; 070413 p�d
		//**************************************************
		case 0 :
    {
			return getCalcUB_3(dbh,hgt,Birch_Small_N_ub);
    }
	
		//**************************************************
    //  "N�slunds mindre, Bj�rk, S�dra"; 070413 p�d
		//**************************************************
		case 1 :
    {
			return getCalcUB_5(dbh,hgt,Birch_Small_S_ub);
    }
	
		//**************************************************
    //  "N�slunds st�rre, Bj�rk, Norra"; 070413 p�d
		//**************************************************
		case 2 :
    {
			return getCalcUB_7(dbh,hgt,kg,Birch_Large_N_ub);
    }
			
		//**************************************************
    //  "N�slunds st�rre, Bj�rk, S�drea"; 070413 p�d
		//**************************************************
		case 3 :
    {
			return getCalcUB_6(dbh,hgt,bark,Birch_Large_S_ub);
    }
	}
	return 0.0;
}

// PUBLIC:
CVolumeNaslund_ub::CVolumeNaslund_ub(void)
	: CCalculationBaseClass()
{
}

CVolumeNaslund_ub::CVolumeNaslund_ub(int spc_index,CTransaction_trakt rec1,
																						 CTransaction_trakt_misc_data rec2,
																						 vecTransactionTraktData &tdata_list,
																						 vecTransactionSampleTree &sample_tree_list,
																						 vecTransactionDCLSTree &dcls_tree_list,
																						 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}

BOOL CVolumeNaslund_ub::calculate(vecTransactionTraktData& vec1,
																  vecTransactionSampleTree& vec2,
																	vecTransactionDCLSTree& vec4,	// Added 070820 p�d
																	vecTransactionTraktSetSpc& vec3)
{
	double f_dcls_mid = 0.0;
	double f_dcls_mid_ub = 0.0;
	double f_bark_reduction = 0.0;

	double fM3Ub = 0.0;
	double fGreenCrown = 0.0;
	int nVolSpcID = -1;
	int nVolIndex = -1;
	int nSpcID = -1;
	int nNumOfTrees = 0;
	// Check that there's any species; 070418 p�d
	if (m_nNumOfDCLSSpecies == 0)
		return FALSE;

	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nVolSpcID = getVolumeSpcID_ub();	
	nVolIndex = getVolumeIndex_ub();
	// Loop through the tree list (m_vecSampleTrees) and
	// calculate volume for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getVolumeID() to get which species
	// method to use and getVolumeIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070417 p�d
	for (long i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		if (tree.getSpcID() == nSpcID)
		{
			// Adaption for "Intr�ngsv�rdering":
			// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
			// OBS! This only allies to "Intr�ng"; 080307 p�d
			nNumOfTrees = tree.getNumOf() + tree.getNumOfRandTrees();
			// Get "Gr�nkroneprocent f�r tr�dslag"; 080626 p�d
			fGreenCrown = getGreenCrownPercForSpc(tree.getSpcID());
			// Diameter for calculation of volume under bark must be
			// under bark. I.e. use bark reduction; 071023 p�d
			f_dcls_mid = getClassMid(tree)/10.0;
			f_bark_reduction = tree.getBarkThick()*2.0;	// Double bark thickess; 071023 p�d
			f_dcls_mid_ub = f_dcls_mid - f_bark_reduction/10.0;
/*
			CString S;
			S.Format("f_dcls_mid %f\nf_bark_reduction %f\nf_dcls_mid_ub %f",
				f_dcls_mid,f_bark_reduction,f_dcls_mid_ub);
			AfxMessageBox(S);
*/
			// Depending on specie, run function accoring to
			// nBarkIndex value; 070417 p�d
			if (nVolSpcID == 1)	// Tall
			{
				fM3Ub = getVolumeForPine_ub(nVolIndex,f_dcls_mid_ub,	
																					 tree.getHgt()/10.0,	
																					 fGreenCrown, //tree.getGCrownPerc(),
																					 tree.getBarkThick());

			}	// if (nVolID == 1)	Tall
			if (nVolSpcID == 2) // Gran
			{
				fM3Ub = getVolumeForSpruce_ub(nVolIndex,f_dcls_mid_ub,
																					 tree.getHgt()/10.0,		
																					 fGreenCrown, //tree.getGCrownPerc(),
																					 tree.getBarkThick());
			}	// if (nVolID == 2) Gran
			if (nVolSpcID == 3)	// Bj�rk
			{
				fM3Ub = getVolumeForBirch_ub(nVolIndex,f_dcls_mid_ub,	
																  				 tree.getHgt()/10.0,	
																					 fGreenCrown, //tree.getGCrownPerc(),
																					 tree.getBarkThick());
			}	// if (nVolID == 3)	Bj�rk

			if (nNumOfTrees > 0)
			{
				fM3Ub	*= nNumOfTrees;
			}

			m_vecDCLSTrees[i].setM3ub(fM3Ub);

			// Also set Calculated "Gr�nkrona", from SampleTrees/Specie; 080625 p�d
			setTraktData_gcrown(nSpcID,fGreenCrown);

		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}
