// UCCalculate.h : main header file for the UCCalculate DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CUCCalculateApp
// See UCCalculate.cpp for the implementation of this class
//

class CUCCalculateApp : public CWinApp
{
public:
	CUCCalculateApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
