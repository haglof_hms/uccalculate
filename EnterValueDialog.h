#pragma once

#include "Resource.h"

// CNumericEdit

///
/// <ramarks></remarks>
///
class CNumericEdit : public CEdit
{
	DECLARE_DYNAMIC(CNumericEdit)

public:
	///
	/// <summary></summary>
	///
	CNumericEdit();
	///
	/// <summary></summary>
	///
	virtual ~CNumericEdit();

protected:
	DECLARE_MESSAGE_MAP()

protected:
	///
	/// <summary></summary>
	///
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);

	///
	/// <summary></summary>
	///
	CString m_strDelim;
};

// CEnterValueDialog dialog

class CEnterValueDialog : public CDialog
{
	DECLARE_DYNAMIC(CEnterValueDialog)

	CNumericEdit m_wndEdit;
	CStatic m_wndLabel;

	double m_fValue;

	CString m_sSpcName;
public:
	CEnterValueDialog(LPCTSTR spc_name = _T(""),CWnd* pParent = NULL);   // standard constructor
	virtual ~CEnterValueDialog();

	virtual INT_PTR DoModal();

	double getValue(void);

// Dialog Data
	enum { IDD = IDD_DIALOG1_1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
