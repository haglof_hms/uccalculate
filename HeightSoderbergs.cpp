#include "StdAfx.h"

#include "EnterValueDialog.h"

#include "HeightSoderbergs.h"

//==========================================================================
// PRIVATE

// Constants for "S�derberg" height function
// OBS! "F�r Gran (Spruce), Bj�rk (Birch) och �vrigt l�f (Other leaf) Norra och Mellersta Sverige slagits samman.

// PINE (Tall)
const double Pine_N[14]	= { -283.90, 6416.8, 0.0063874, -0.000030707, 0.00127740, -0.0155970,-0.0000048527, -0.44962     , 0.070355, 0.087350,-0.056157, -0.072392, 6.8125, 0.152}; 
const double Pine_M[17] = { -292.49, 6183.2, 0.0052675, -0.000025358, 0.00137210, 0.0697710 , 0.0058106   , -0.0001001800, -0.61165, 0.131320, 0.222170,  0.245040, 0.232510, -0.055749, -0.101860, 1.5712, 0.137};
const double Pine_S[17] = { -303.45, 8842.7, 0.0068724, -0.000038585, 0.00166460, -0.0047335, 0.0000826790,  0.091429    , -0.28115, 0.205700, 0.29485 ,  0.139090, 0.036444, -0.060312, -0.198550, 5.2706, 0.159};

// Gran (Spruce)
const double Spruce_N_M[15] = { -286.63, 4783.1, 0.0031669, -0.000016854, 0.00108550, -0.0099681,  0.00051262 , -0.000012449, -0.19831, 0.060923, 0.090784, -0.030688, -0.062548, 6.5200, 0.148};
const double Spruce_S[15]		= { -274.21, 3801.3, 0.0031094, -0.000020764, 0.00101610,  0.0015166, -0.000025385, -0.237600   ,  0.10172, 0.24012 , 0.068141, -0.047848, -0.069386, 5.7495, 0.145};

// Bj�rk (Birch)
const double Birch_N_M[14] = { -266.07, 7141.5, 0.0032789, -0.000022514, 0.00085255, -0.0184620, -0.0000072180, -0.39250,  0.076500, -0.074398, -0.022539, -0.035918, 7.2446, 0.158};
const double Birch_S[15]   = { -225.52, 3917.1, 0.0017264, -0.000011572, 0.00089953, -0.0090184,  0.0001580400, -0.322960, -0.044799, 0.11728, 0.101040, 0.042911, -0.068048, 5.7820, 0.164};

// �vtigt L�v (Other leaf)
const double OthLeaf_N_M[10] = { -145.46, 0.0053659, -0.000029042, 0.0017639, -0.0342, 0.075841, -0.082953, 0.15566, 7.0706, 0.191 };
const double OthLeaf_S[14]		= { -220.78, 5392.0, 0.0053701, -0.000041932, 0.00053968, -0.010758, 0.00018781, -0.17045, -0.17291, 0.10783, -0.055868, -0.051887, 5.6569, 0.191 };

// Bok (Beech)
const double Beech_All[10] = { -144.07, 0.0072319, -0.000027244, -0.000005781, 0.1804, 0.188, -0.18416, -0.1741, 5.2974, 0.161 };

// Ek (Oak)
const double Oak_All[11] = { -258.11, 6310.0, 0.0013039, -0.0000041543, -0.32505, 0.059855, 0.17355, -0.047987, -0.069304, 5.7884, 0.178 };


// PRIVATE

//======================================================
// Tall (Pine)

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Delad yta"  Y/N; Y = 1, N = 0
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtPine_N(CTransaction_dcls_tree tree)
{
/*	JUST FOR DEBUG OF DATA
	CString S;

	S.Format(_T("getHgtPine_N %d\n\nDBH = %f\nA %f %f\nB %f %f\nC %f %d\nD %f %f\nE %f %d\nF %f %d\nG %f %d\nH %f %f\nI %f %f\nJ %f %f %f %f\nK %f %d\nL %f %d\n%f  %f"),
									tree.getSpcID(),getClassMid(tree),
									Pine_N[0],(getClassMid(tree) + 50.0),
									Pine_N[1],(pow(getClassMid(tree) + 50.0,2)),
									Pine_N[2],getTrakt().getTraktAge(),
									Pine_N[3],pow((double)getTrakt().getTraktAge(),2),
									Pine_N[4],getSIfromH100(getTrakt().getTraktSIH100()),
									Pine_N[5],getTrakt().getTraktLatitude(),
									Pine_N[6],getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude(),
									Pine_N[7],(getDiamQuata(getClassMid(tree))),
									Pine_N[8],pow(getDiamQuata(getClassMid(tree)),2),
									Pine_N[9],getSpcPercent_GY(1),getSpcPercent_GY(2),getSpcPercent_GY(3),
									Pine_N[10],getTrakt().getTraktPartOfPlot(),
									Pine_N[11],getTrakt().getTraktNearCoast(),
									Pine_N[12],
									pow(Pine_N[13],2)/2.0);
	AfxMessageBox(S);
*/
		
	// Calculate height
	double fValue = (Pine_N[0]/(getClassMid(tree) + 50.0)) +
									(Pine_N[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Pine_N[2]*getTrakt().getTraktAge()) +
									(Pine_N[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(Pine_N[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Pine_N[5]*getTrakt().getTraktLatitude()) +
									(Pine_N[6]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Pine_N[7]*(getDiamQuata(getClassMid(tree)))) +
									(Pine_N[8]*pow(getDiamQuata(getClassMid(tree)),2)) +
									(Pine_N[9]*getSpcPercent_GY(1)) +	// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Pine_N[10]*getTrakt().getTraktPartOfPlot()) +
									(Pine_N[11]*getTrakt().getTraktNearCoast()) +
									(Pine_N[12]) +	// A constant
									(pow(Pine_N[13],2)/2.0);
	return exp(fValue);
}

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Gran"
// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
// - "Delad yta" Y/N; Y = 1, N = 0
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtPine_M(CTransaction_dcls_tree tree)
{
	// Calculate height
	double fValue = (Pine_M[0]/(getClassMid(tree) + 50.0)) +
									(Pine_M[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Pine_M[2]*getTrakt().getTraktAge()) +
									(Pine_M[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(Pine_M[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Pine_M[5]*getTrakt().getTraktLatitude()) +
									(Pine_M[6]*getTrakt().getTraktHgtOverSea()) +
									(Pine_M[7]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Pine_M[8]*(getDiamQuata(getClassMid(tree)))) +
									(Pine_M[9]*pow(getDiamQuata(getClassMid(tree)),2)) +
									(Pine_M[10]*getSpcPercent_GY(1)) +		// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Pine_M[11]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Pine_M[12]*getSpcPercent_GY(3)) +		// Get percentage for specie id = 3 i.e. Birch (Bj�rk)
									(Pine_M[13]*getTrakt().getTraktPartOfPlot()) +
									(Pine_M[14]*getTrakt().getTraktNearCoast()) +
									(Pine_M[15]) +	// A constant
									(pow(Pine_M[16],2)/2.0);
	return exp(fValue);
}

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Gran"
// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
// - "Delad yta" Y/N; Y = 1, N = 0
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtPine_S(CTransaction_dcls_tree tree)
{
	// Calculate height
	double fValue = (Pine_S[0]/(getClassMid(tree) + 50.0)) +
									(Pine_S[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Pine_S[2]*getTrakt().getTraktAge()) +
									(Pine_S[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(Pine_S[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Pine_S[5]*getTrakt().getTraktHgtOverSea()) +
									(Pine_S[6]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Pine_S[7]*(getDiamQuata(getClassMid(tree)))) +
									(Pine_S[8]*pow(getDiamQuata(getClassMid(tree)),2)) +
									(Pine_S[9]*getSpcPercent_GY(1)) +		// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Pine_S[10]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Pine_S[11]*getSpcPercent_GY(3)) +		// Get percentage for specie id = 3 i.e. Birch (Bj�rk)
									(Pine_S[12]*getTrakt().getTraktSoutheast()) +
									(Pine_S[13]*getTrakt().getTraktPartOfPlot()) +
									(Pine_S[14]*getTrakt().getTraktNearCoast()) +
									(Pine_S[15]) +	// A constant
									(pow(Pine_S[16],2)/2.0);

	return exp(fValue);
}

//======================================================
// Spruce (Gran)

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Gran"
// - "Delad yta" Y/N; Y = 1, N = 0
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtSpruce_NandM(CTransaction_dcls_tree tree)
{
/*	JUST FOR DEBUG OF DATA
	CString S;
	S.Format("getHgtSpruce_NandM %d\n\nDBH = %f\nA %f %f\nB %f %f\nC %f %d\nD %f %f\nE %f %d\nF %f %d\nG %f %d\nH %f %f\nI %f %f\nJ %f %f %f %f\nK %f\nL %f",
									tree.getSpcID(),getClassMid(tree),
									Pine_N[0],(getClassMid(tree) + 50.0),
									Pine_N[1],(pow(getClassMid(tree) + 50.0,2)),
									Pine_N[2],getTrakt().getTraktAge(),
									Pine_N[3],pow((double)getTrakt().getTraktAge(),2),
									Pine_N[4],getSIfromH100(getTrakt().getTraktSIH100()),
									Pine_N[5],getTrakt().getTraktLatitude(),
									Pine_N[6],getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude(),
									Pine_N[7],(getDiamQuata(getClassMid(tree))),
									Pine_N[8],pow(getDiamQuata(getClassMid(tree)),2),
									Pine_N[9],getSpcPercent_GY(1),getSpcPercent_GY(2),getSpcPercent_GY(3),
									(Pine_N[10]*getTrakt().getTraktPartOfPlot()) +
									(Pine_N[11]*getTrakt().getTraktNearCoast()) +
									Pine_N[12],
									pow(Pine_N[13],2)/2.0);
	AfxMessageBox(S);
*/	
	// Calculate height
	double fValue = (Spruce_N_M[0]/(getClassMid(tree) + 50.0)) +
									(Spruce_N_M[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Spruce_N_M[2]*getTrakt().getTraktAge()) +
									(Spruce_N_M[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(Spruce_N_M[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Spruce_N_M[5]*getTrakt().getTraktLatitude()) +
									(Spruce_N_M[6]*getTrakt().getTraktHgtOverSea()) +
									(Spruce_N_M[7]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Spruce_N_M[8]*(getDiamQuata(getClassMid(tree)))) +
									(Spruce_N_M[9]*getSpcPercent_GY(1)) +		// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Spruce_N_M[10]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Spruce_N_M[11]*getTrakt().getTraktPartOfPlot()) +
									(Spruce_N_M[12]*getTrakt().getTraktNearCoast()) +
									(Spruce_N_M[13]) +	// A constant
									(pow(Spruce_N_M[14],2)/2.0);

	return exp(fValue);
}

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Gran"
// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
// - "Delad yta" Y/N; Y = 1, N = 0
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtSpruce_S(CTransaction_dcls_tree tree)
{
	// Calculate height
	double fValue = (Spruce_S[0]/(getClassMid(tree) + 50.0)) +
									(Spruce_S[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Spruce_S[2]*getTrakt().getTraktAge()) +
									(Spruce_S[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(Spruce_S[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Spruce_S[5]*getTrakt().getTraktHgtOverSea()) +
									(Spruce_S[6]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Spruce_S[7]*(getDiamQuata(getClassMid(tree)))) +
									(Spruce_S[8]*getSpcPercent_GY(1)) +		// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Spruce_S[9]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Spruce_S[10]*getSpcPercent_GY(3)) +		// Get percentage for specie id = 3 i.e. Birch (Bj�rk)
									(Spruce_S[11]*getTrakt().getTraktPartOfPlot()) +
									(Spruce_S[12]*getTrakt().getTraktNearCoast()) +
									(Spruce_S[13]) +	// A constant
									(pow(Spruce_S[14],2)/2.0);

	return exp(fValue);
}

//======================================================
// Birch (Bj�rk)

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Gran"
// - "Delad yta" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtBirch_NandM(CTransaction_dcls_tree tree)
{
	// Calculate height
	double fValue = (Birch_N_M[0]/(getClassMid(tree) + 50.0)) +
									(Birch_N_M[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Birch_N_M[2]*getTrakt().getTraktAge()) +
									(Birch_N_M[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(Birch_N_M[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Birch_N_M[5]*getTrakt().getTraktLatitude()) +
									(Birch_N_M[6]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Birch_N_M[7]*(getDiamQuata(getClassMid(tree)))) +
									(Birch_N_M[8]*(pow(getDiamQuata(getClassMid(tree)),2))) +
									(Birch_N_M[9]*getSpcPercent_GY(1)) +		// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Birch_N_M[10]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Birch_N_M[11]*getTrakt().getTraktPartOfPlot()) +
									(Birch_N_M[12]) +	// A constant
									(pow(Birch_N_M[13],2)/2.0);

/*
	CString S;
	S.Format("double CHeightSoderbergs::getHgtBirch_NandM(CTransaction_dcls_tree tree)\ngetClassMid(tree) %f\nfValue %f",
		getClassMid(tree),(fValue));
	AfxMessageBox(S);
*/

	return exp(fValue);
}

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Gran"
// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
// - "Delad yta" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtBirch_S(CTransaction_dcls_tree tree)
{
	double fValue = (Birch_S[0]/(getClassMid(tree) + 50.0)) +
									(Birch_S[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Birch_S[2]*getTrakt().getTraktAge()) +
									(Birch_S[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(Birch_S[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Birch_S[5]*getTrakt().getTraktHgtOverSea()) +
									(Birch_S[6]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Birch_S[7]*(getDiamQuata(getClassMid(tree)))) +
									(Birch_S[8]*getSpcPercent_GY(1)) +		// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Birch_S[9]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Birch_S[10]*getSpcPercent_GY(3)) +		// Get percentage for specie id = 3 i.e. Birch (Bj�rk)
									(Birch_S[11]*getTrakt().getTraktSoutheast()) +
									(Birch_S[12]*getTrakt().getTraktPartOfPlot()) +
									(Birch_S[13]) +	// A constant
									(pow(Birch_S[14],2)/2.0);

/*
	CString S;
	S.Format("double CHeightSoderbergs::getHgtBirch_S(CTransaction_dcls_tree tree)\ngetClassMid(tree) %f\nfValue %f",
		getClassMid(tree),(fValue));
	AfxMessageBox(S);
*/
	return exp(fValue);
}

//======================================================
// Other Leaf (�vrigt L�v)

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - "Tr�dslagsf�rdelning Gran"
// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtOthLeaf_NandM(CTransaction_dcls_tree tree)
{
	double fValue = (OthLeaf_N_M[0]/(getClassMid(tree) + 50.0)) +
									(OthLeaf_N_M[1]*getTrakt().getTraktAge()) +
									(OthLeaf_N_M[2]*pow((double)getTrakt().getTraktAge(),2)) +
									(OthLeaf_N_M[3]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(OthLeaf_N_M[4]*getTrakt().getTraktLatitude()) +
									(OthLeaf_N_M[5]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(OthLeaf_N_M[6]*getTrakt().getTraktPartOfPlot()) +
									(OthLeaf_N_M[7]*getTrakt().getTraktNearCoast()) +
									(OthLeaf_N_M[8]) +	// A constant
									(pow(OthLeaf_N_M[9],2)/2.0);

	return exp(fValue);
}

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Gran"
// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
// - "Delad yta" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtOthLeaf_S(CTransaction_dcls_tree tree)
{

	double fValue = (OthLeaf_S[0]/(getClassMid(tree) + 50.0)) +
									(OthLeaf_S[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(OthLeaf_S[2]*getTrakt().getTraktAge()) +
									(OthLeaf_S[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(OthLeaf_S[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(OthLeaf_S[5]*getTrakt().getTraktHgtOverSea()) +
									(OthLeaf_S[6]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(OthLeaf_S[7]*(getDiamQuata(getClassMid(tree)))) +
									(OthLeaf_S[8]*getSpcPercent_GY(1)) +		// Get percentage for specie id = 1 i.e. Pine (Tall)
									(OthLeaf_S[9]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(OthLeaf_S[10]*getSpcPercent_GY(3)) +		// Get percentage for specie id = 3 i.e. Birch (Bj�rk)
									(OthLeaf_S[11]*getTrakt().getTraktPartOfPlot()) +
									(OthLeaf_S[12]) +	// A constant
									(pow(OthLeaf_S[13],2)/2.0);

	return exp(fValue);
}

//======================================================
// Beech (Bok) All species

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - "Breddgrad"
// - Height over the sea
// - "Tr�dslagsf�rdelning Gran"
// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
// - "Region 5" Y/N; Y = 1, N = 0
// - "Delad yta" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtBeech(CTransaction_dcls_tree tree)
{
	double fValue = (Beech_All[0]/(getClassMid(tree) + 50.0)) +
									(Beech_All[1]*getTrakt().getTraktAge()) +
									(Beech_All[2]*pow((double)getTrakt().getTraktAge(),2)) +
									(Beech_All[3]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Beech_All[4]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Beech_All[5]*getTrakt().getTraktSoutheast()) +
									(Beech_All[6]*getTrakt().getTraktRegion5()) +
									(Beech_All[7]*getTrakt().getTraktPartOfPlot()) +
									(Beech_All[8]) +	// A constant
									(pow(Beech_All[9],2)/2.0);

	return exp(fValue);
}


//======================================================
// Oak (Ek) All species

//---------------------------------------------------
// Arguments used in this function; 0700424 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Gran"
// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
// - "Region 5" Y/N; Y = 1, N = 0
// - "Delad yta" Y/N; Y = 1, N = 0
double CHeightSoderbergs::getHgtOak(CTransaction_dcls_tree tree)
{
	double fValue = (Oak_All[0]/(getClassMid(tree) + 50.0)) +
									(Oak_All[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Oak_All[2]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Oak_All[3]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Oak_All[4]*(getDiamQuata(getClassMid(tree)))) +
									(Oak_All[5]*getSpcPercent_GY(2)) +		// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Oak_All[6]*getTrakt().getTraktSoutheast()) +
									(Oak_All[7]*getTrakt().getTraktRegion5()) +
									(Oak_All[8]*getTrakt().getTraktPartOfPlot()) +
									(Oak_All[9]) +	// A constant
									(pow(Oak_All[10],2)/2.0);

	return exp(fValue);
}

// Calculate a "calibration factor", based on J Nygren (also Ola Lindgrens paper dated 1995-03-31)
double CHeightSoderbergs::getCalibrationFactor(void)
{
	CString S;
	double fSumSampleTreeHgt = 0.0;	// Sum. of sampletree heights
	double fSumSoderbergHgt = 0.0;	// Sum. of heights calculated in "S�derbegs" function
	double fSampleHgt = 0.0;
	double fHgt = 0.0;
	double fFactor = 1.0;
	double fQuota = 0.0;
	long lNumOfSampleTrees = 0;
	int nHgtSpcID = -1;
	int nHgtIndex = -1;
	int nSpcID = -1;

	// Check that there's any species; 070418 p�d
	if (m_nNumOfDCLSSpecies == 0)
		return FALSE;

	// Check that there's any trees; 070418 p�d
	if (m_nNumOfSampleTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating heights per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get HeightFuncionID and
	// HeightFunctionIndex; 070425 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070425 p�d
	nSpcID = getSpecieID();	
	nHgtSpcID = getHeightSpcID();	
	nHgtIndex = getHeightIndex();

/*	JUST FOR DEBUG OF DATA
	CString S;
	S.Format("CHeightSoderbergs::getCalibrationFactor\nm_nSpcIndex %d\nnSpcID %d\nnHgtSpcID %d\nHgtIndex %d",
			m_nSpcIndex,nSpcID,nHgtSpcID,nHgtIndex);
		AfxMessageBox(S);
*/
	for (long i = 0;i < m_nNumOfSampleTrees;i++)
	{
		CTransaction_sample_tree tree_sample = m_vecSampleTrees[i];
		CTransaction_dcls_tree tree = CTransaction_dcls_tree(tree_sample.getTreeID(),tree_sample.getTraktID(),
																												 tree_sample.getPlotID(),tree_sample.getSpcID(),
																												 _T(""),
																												 tree_sample.getDCLS_from(),tree_sample.getDCLS_to(),
																												 0.0,1,0.0,tree_sample.getBarkThick()*2.0,0.0,0.0,0.0,0,0,0,0,_T(""));

		// Check for Specie; 070425 p�d
		// OBS! Only do this for trees that isn't a sampletree; 070425 p�d
		if (tree.getSpcID() == nSpcID)
		{
			// Select function depending on specie and
			// index of function within specie; 070424 p�d
			if (nHgtSpcID == 1)	// Pine
			{
				fHgt = setHeightForPine(nHgtIndex,tree);
			}
			else if (nHgtSpcID == 2)	// Spruce
			{
				fHgt = setHeightForSpruce(nHgtIndex,tree);
			}
			else if (nHgtSpcID == 3)	// Birch
			{
				fHgt = setHeightForBirch(nHgtIndex,tree);
			}
			else if (nHgtSpcID == 4)	// Oth. Leaf
			{
				fHgt = setHeightForOtherLeaf(nHgtIndex,tree);
			}
			else if (nHgtSpcID == 5)	// Beech
			{
				fHgt = setHeightForBeech(nHgtIndex,tree);
			}
			else if (nHgtSpcID == 6)	// Oak
			{
				fHgt = setHeightForOak(nHgtIndex,tree);
			}
			fSumSoderbergHgt += fHgt;
		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	// Calculate the actual factor. Based on (J. Nygren & Ola Lindgren)
	// OBS! One way to do it for Spruce; 070425 p�d
	lNumOfSampleTrees = getNumOfSampleTrees(nSpcID);
	fSumSampleTreeHgt = getSpcSumSampleTreeHgt(nSpcID);
/*
	S.Format(_T("lNumOfSampleTrees %d\nfSumSampleTreeHgt %f\nfSumSoderbergHgt %f"),
				lNumOfSampleTrees,fSumSampleTreeHgt,fSumSoderbergHgt);
	AfxMessageBox(S);
*/

	if (fSumSoderbergHgt > 0.0 && fSumSampleTreeHgt > 0.0)
	{
		if (nSpcID == 2)
		{
			fFactor = (fSumSampleTreeHgt/fSumSoderbergHgt);
			// Check if we need to do some calibration; 070523 p�d
			if (lNumOfSampleTrees < 10)
			{
				fQuota = (lNumOfSampleTrees / 10.0);
				fFactor = fQuota * fFactor + (1.0 - fQuota);
			}	// if (lNumOfSampleTrees < 10)
		}	// if (nSpcID == 2)
		else
		{
			if (lNumOfSampleTrees > 0 )	// We have sampletrees
			{
				fFactor = (fSumSampleTreeHgt-fSumSoderbergHgt)/lNumOfSampleTrees;
				// Check if we need to do some calibration; 070523 p�d
				if (lNumOfSampleTrees < 10)
				{

					fQuota = (lNumOfSampleTrees / 10.0);
					fFactor *= fQuota;
				}	// if (if (lNumOfSampleTrees < 10)
/*
					S.Format(_T("lNumOfSampleTrees %d\nfQuota %f\nfFactor %f\nfSumSampleTreeHgt %f\nfSumSoderbergHgt %f"),
						lNumOfSampleTrees,fQuota,fFactor,fSumSampleTreeHgt,fSumSoderbergHgt);
					AfxMessageBox(S);
/*/
			}	// if (lNumOfSampleTrees > 0)
		}
	}	// if (fSumSoderbergHgt > 0.0 && fSumSampleTreeHgt > 0.0)

	return fFactor;

}

//==========================================================================
// PROTECTED

double CHeightSoderbergs::setHeightForPine(int hgt_index,CTransaction_dcls_tree tree)
{
	double fHgt;
	// Setup for calculating a H25 value; 070524 p�d
	CTransaction_dcls_tree h25_tree = CTransaction_dcls_tree(tree);
	h25_tree.setDCLS_from(24.0);
	h25_tree.setDCLS_to(26.0);
	// Setup for calculating HGV based on DBH = 25.0 cm and DGV; 070525 p�d
	CTransaction_dcls_tree hgv_tree = CTransaction_dcls_tree(tree);
	hgv_tree.setDCLS_from(24.0);
	hgv_tree.setDCLS_to(26.0);
	if (getTraktData(tree.getSpcID()) != NULL)
		hgv_tree.setHgt(getTraktData(tree.getSpcID())->getDGV());
	else
		hgv_tree.setHgt(18.0);
	// Check index of function to use
	switch (hgt_index)
	{
		case 0 :	// Tall norra Sverige
		{
			fHgt = getHgtPine_N(tree);
			if (!m_bIsCalculated)
			{
				// Calculate a H25 value; 070524 p�d
				m_fSoderbergsH25Value = getHgtPine_N(h25_tree);
				// Calculate HGV based on DBH = 25.0 cm and DGV; 070525 p�d
				m_fSoderbergsHGVValue = getHgtPine_N(hgv_tree);
				m_bIsCalculated = TRUE;
			}
			return fHgt;
		}
		case 1 :	// Tall mellersta Sverige
		{
			fHgt = getHgtPine_M(tree);
			if (!m_bIsCalculated)
			{
				// Calculate a H25 value; 070524 p�d
				m_fSoderbergsH25Value = getHgtPine_M(h25_tree);
				// Calculate HGV based on DBH = 25.0 cm and DGV; 070525 p�d
				m_fSoderbergsHGVValue = getHgtPine_M(hgv_tree);
				m_bIsCalculated = TRUE;
			}
			return fHgt;
		}
		case 2 :	// Tall s�dra Sverige
		{
			fHgt = getHgtPine_S(tree);
			if (!m_bIsCalculated)
			{
				// Calculate a H25 value; 070524 p�d
				m_fSoderbergsH25Value = getHgtPine_S(h25_tree);
				// Calculate HGV based on DBH = 25.0 cm and DGV; 070525 p�d
				m_fSoderbergsHGVValue = getHgtPine_S(hgv_tree);
				m_bIsCalculated = TRUE;
			}
			return fHgt;
		}
	};
	return 0.0;
}

double CHeightSoderbergs::setHeightForSpruce(int hgt_index,CTransaction_dcls_tree tree)
{
	double fHgt;
	// Setup for calculating a H25 value; 070524 p�d
	CTransaction_dcls_tree h25_tree = CTransaction_dcls_tree(tree);
	h25_tree.setDCLS_from(24.0);
	h25_tree.setDCLS_to(26.0);
	// Setup for calculating HGV based on DBH = 25.0 cm and DGV; 070525 p�d
	CTransaction_dcls_tree hgv_tree = CTransaction_dcls_tree(tree);
	hgv_tree.setDCLS_from(24.0);
	hgv_tree.setDCLS_to(26.0);
	if (getTraktData(tree.getSpcID()) != NULL)
		hgv_tree.setHgt(getTraktData(tree.getSpcID())->getDGV());
	else
		hgv_tree.setHgt(18.0);
	// Check index of function to use
	switch (hgt_index)
	{
		case 0 :	// Gran norra och mellersta Sverige
		{
			fHgt = getHgtSpruce_NandM(tree);
			if (!m_bIsCalculated)
			{
				// Calculate a H25 value; 070524 p�d
				m_fSoderbergsH25Value = getHgtSpruce_NandM(h25_tree);
				// Calculate HGV based on DBH = 25.0 cm and DGV; 070525 p�d
				m_fSoderbergsHGVValue = getHgtSpruce_NandM(hgv_tree);
				m_bIsCalculated = TRUE;
			}
			return fHgt;
		}
		case 1 :	// Gran s�dra Sverige
		{
			fHgt = getHgtSpruce_S(tree);
			if (!m_bIsCalculated)
			{
				// Calculate a H25 value; 070524 p�d
				m_fSoderbergsH25Value = getHgtSpruce_S(h25_tree);
				// Calculate HGV based on DBH = 25.0 cm and DGV; 070525 p�d
				m_fSoderbergsHGVValue = getHgtSpruce_S(hgv_tree);
				m_bIsCalculated = TRUE;
			}
			return fHgt;
		}
	};
	return 0.0;
}

double CHeightSoderbergs::setHeightForBirch(int hgt_index,CTransaction_dcls_tree tree)
{
	double fHgt;
	// Setup for calculating a H25 value; 070524 p�d
	CTransaction_dcls_tree h25_tree = CTransaction_dcls_tree(tree);
	h25_tree.setDCLS_from(24.0);
	h25_tree.setDCLS_to(26.0);
	// Setup for calculating HGV based on DBH = 25.0 cm and DGV; 070525 p�d
	CTransaction_dcls_tree hgv_tree = CTransaction_dcls_tree(tree);
	hgv_tree.setDCLS_from(24.0);
	hgv_tree.setDCLS_to(26.0);
	hgv_tree.setHgt(getTraktData(tree.getSpcID())->getDGV());
	// Check index of function to use
	switch (hgt_index)
	{
		case 0 :	// Bj�rk norra och mellersta Sverige
		{
			fHgt = getHgtBirch_NandM(tree);
			if (!m_bIsCalculated)
			{
				// Calculate a H25 value; 070524 p�d
				m_fSoderbergsH25Value = getHgtBirch_NandM(h25_tree);
				// Calculate HGV based on DBH = 25.0 cm and DGV; 070525 p�d
				m_fSoderbergsHGVValue = getHgtBirch_NandM(hgv_tree);
				m_bIsCalculated = TRUE;
			}
			return fHgt;
		}
		case 1 :	// Bj�rk s�dra Sverige
		{
			fHgt = getHgtBirch_S(tree);
			if (!m_bIsCalculated)
			{
				// Calculate a H25 value; 070524 p�d
				m_fSoderbergsH25Value = getHgtBirch_S(h25_tree);
				// Calculate HGV based on DBH = 25.0 cm and DGV; 070525 p�d
				m_fSoderbergsHGVValue = getHgtBirch_S(hgv_tree);
				m_bIsCalculated = TRUE;
			}
			return fHgt;
		}
	};
	return 0.0;
}

double CHeightSoderbergs::setHeightForOtherLeaf(int hgt_index,CTransaction_dcls_tree tree)
{
	double fHgt;
	// Setup for calculating a H25 value; 070524 p�d
	CTransaction_dcls_tree h25_tree = CTransaction_dcls_tree(tree);
	h25_tree.setDCLS_from(24.0);
	h25_tree.setDCLS_to(26.0);
	// Setup for calculating HGV based on DBH = 25.0 cm and DGV; 070525 p�d
	CTransaction_dcls_tree hgv_tree = CTransaction_dcls_tree(tree);
	hgv_tree.setDCLS_from(24.0);
	hgv_tree.setDCLS_to(26.0);
	hgv_tree.setHgt(getTraktData(tree.getSpcID())->getDGV());
	// Check index of function to use
	switch (hgt_index)
	{
		case 0 :	// �vrigt l�v norra och mellersta Sverige
		{
			fHgt = getHgtOthLeaf_NandM(tree);
			if (!m_bIsCalculated)
			{
				// Calculate a H25 value; 070524 p�d
				m_fSoderbergsH25Value = getHgtOthLeaf_NandM(h25_tree);
				// Calculate HGV based on DBH = 25.0 cm and DGV; 070525 p�d
				m_fSoderbergsHGVValue = getHgtOthLeaf_NandM(hgv_tree);
				m_bIsCalculated = TRUE;
			}
			return fHgt;
		}
		case 1 :	//  �vrig l�v s�dra Sverige
		{
			fHgt = getHgtOthLeaf_S(tree);
			if (!m_bIsCalculated)
			{
				// Calculate a H25 value; 070524 p�d
				m_fSoderbergsH25Value = getHgtOthLeaf_S(h25_tree);
				// Calculate HGV based on DBH = 25.0 cm and DGV; 070525 p�d
				m_fSoderbergsHGVValue = getHgtOthLeaf_S(hgv_tree);
				m_bIsCalculated = TRUE;
			}
			return fHgt;
		}
	};
	return 0.0;
}

double CHeightSoderbergs::setHeightForBeech(int hgt_index,CTransaction_dcls_tree tree)
{
	double fHgt = getHgtBeech(tree);
	// Setup for calculating a H25 value; 070524 p�d
	CTransaction_dcls_tree h25_tree = CTransaction_dcls_tree(tree);
	h25_tree.setDCLS_from(24.0);
	h25_tree.setDCLS_to(26.0);
	// Setup for calculating HGV based on DBH = 25.0 cm and DGV; 070525 p�d
	CTransaction_dcls_tree hgv_tree = CTransaction_dcls_tree(tree);
	hgv_tree.setDCLS_from(24.0);
	hgv_tree.setDCLS_to(26.0);
	hgv_tree.setHgt(getTraktData(tree.getSpcID())->getDGV());
	if (!m_bIsCalculated)
	{
		m_fSoderbergsH25Value = getHgtBeech(h25_tree);
		m_fSoderbergsHGVValue = getHgtBeech(hgv_tree);
		m_bIsCalculated = TRUE;
	}

	return fHgt;;
}

double CHeightSoderbergs::setHeightForOak(int hgt_index,CTransaction_dcls_tree tree)
{
	double fHgt = getHgtOak(tree);
	// Setup for calculating a H25 value; 070524 p�d
	CTransaction_dcls_tree h25_tree = CTransaction_dcls_tree(tree);
	h25_tree.setDCLS_from(24.0);
	h25_tree.setDCLS_to(26.0);
	// Setup for calculating HGV based on DBH = 25.0 cm and DGV; 070525 p�d
	CTransaction_dcls_tree hgv_tree = CTransaction_dcls_tree(tree);
	hgv_tree.setDCLS_from(24.0);
	hgv_tree.setDCLS_to(26.0);
	hgv_tree.setHgt(getTraktData(tree.getSpcID())->getDGV());
	if (!m_bIsCalculated)
	{
		m_fSoderbergsH25Value = getHgtOak(h25_tree);
		m_fSoderbergsHGVValue = getHgtOak(hgv_tree);
		m_bIsCalculated = TRUE;
	}
	return fHgt;
}

//==========================================================================
// PUBLIC
CHeightSoderbergs::CHeightSoderbergs(void)
	: CCalculationBaseClass()
{
		m_bIsCalculated = FALSE;
}

CHeightSoderbergs::CHeightSoderbergs(int spc_index,CTransaction_trakt rec1,
																									 CTransaction_trakt_misc_data rec2,
																									 vecTransactionTraktData &tdata_list,
																									 vecTransactionSampleTree &sample_tree_list,
																									 vecTransactionDCLSTree &dcls_tree_list,
																									 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
		m_bIsCalculated = FALSE;
}

//----------------------------------------------------------------------------------------------
//  COMMENT ADDED 2009-02-27 P�D
//	CHANGED 'calculate' METHOD; We'll not check if number of sampletrees >= MIN_NUMOF_SAMPLETREES_SODERBERGS
//	JUST IF THERE'S SAMPLETREES DO CALIBRATION, OTHERWISE WE'LL DO A "CLEAN NO'N CALIBRATED S�DERBERGS; 090227 p�d
//----------------------------------------------------------------------------------------------

BOOL CHeightSoderbergs::calculate(vecTransactionTraktData& vec1,
																	vecTransactionSampleTree& vec2,
																	vecTransactionDCLSTree& vec4,
																	vecTransactionTraktSetSpc& vec3)
{
	double fHgt = 0.0;
	double fFactor = 0.0;
	int nHgtSpcID = -1;
	int nHgtIndex = -1;
	int nSpcID = -1;
	int nNumOfSampleTrees = 0;
	CString sMsg,S;
	CString sResStr;

	// Not set yet; 070524 p�d
	m_fSoderbergsH25Value = 0.0;
	m_fSoderbergsHGVValue = 0.0;

	// Check that there's any species; 070418 p�d
	if (m_nNumOfSampleSpecies == 0)
		return FALSE;


/*
	// Check that there's any trees; 070418 p�d
	if (m_nNumOfSampleTrees == 0)
		return FALSE;
*/
	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nHgtSpcID = getHeightSpcID();	
	nHgtIndex = getHeightIndex();

/* COMMENTED OUT 2009-02-27 P�D
	nNumOfSampleTrees = getNumOfSampleTrees();
	if (nNumOfSampleTrees > 0 && nNumOfSampleTrees < MIN_NUMOF_SAMPLETREES_SODERBERGS)
	{
		// Setup message, tellin' user about the bad new. Not enough sampletrees; 070625 p�d

		sMsg.Format(_T("<FONT SIZE=\"10\"><B>%s</B></FONT><BR><BR><BR>%s <B>%s</B><BR><BR>%s <B>%d</B><BR><BR>%s<BR><BR>%s"),
			getResStr(IDS_STRING202),
			getResStr(IDS_STRING203),
			getSpecieName(nSpcID),
			getResStr(IDS_STRING204),
			nNumOfSampleTrees,
			getResStr(IDS_STRING205),
			getResStr(IDS_STRING206));

		CMessageDialog *msg = new CMessageDialog(getResStr(IDS_STRING200),
																						 getResStr(IDS_STRING201),
																						 sMsg);
		if (msg != NULL)
		{
			msg->DoModal();
			delete msg;
		}
		return FALSE;	// Not enough sampletrees
	}
*/

	// Loop through the tree list (m_vecSampleTrees) and
	// calculate height for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getHeightID() to get which species
	// method to use and getHeightIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070424 p�d
	for (long i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		// Check for Specie; 070424 p�d
		// OBS! Only do this for trees that is not a sampletree; 070425 p�d
		// Make sure specie is ok and that there's not already a
		// height specified (if height IT'S A SAMPLE TREE); 080122 p�d
		if (tree.getSpcID() == nSpcID && tree.getNumOfRandTrees() != -999)
		{
/*
			S.Format(_T("Spc %d  HgtIndex %d   DCLS %f %f\nT %f  G %f  B %f"),nSpcID,nHgtIndex,tree.getDCLS_from(),tree.getDCLS_to(),getSpcPercent_GY(1),getSpcPercent_GY(2),getSpcPercent_GY(3));
			AfxMessageBox(S);
*/
			// Select function depending on specie and
			// index of function within specie; 070424 p�d
			if (nHgtSpcID == 1)	// Pine
			{
				fHgt = setHeightForPine(nHgtIndex,tree);
/*
				S.Format(_T("Spc %d  HgtIndex %d   DCLS %f %f\nHGT %f"),nSpcID,nHgtIndex,tree.getDCLS_from(),tree.getDCLS_to(),fHgt);
				AfxMessageBox(S);
*/
			}
			else if (nHgtSpcID == 2)	// Spruce
			{
				fHgt = setHeightForSpruce(nHgtIndex,tree);
			}
			else if (nHgtSpcID == 3)	// Birch
			{
				fHgt = setHeightForBirch(nHgtIndex,tree);
			}
			else if (nHgtSpcID == 4)	// Oth. Leaf
			{
				fHgt = setHeightForOtherLeaf(nHgtIndex,tree);
			}
			else if (nHgtSpcID == 5)	// Beech
			{
				fHgt = setHeightForBeech(nHgtIndex,tree);
			}
			else if (nHgtSpcID == 6)	// Oak
			{
				fHgt = setHeightForOak(nHgtIndex,tree);
			}

			//==========================================================================
			// Calculate the calibration factor (according to J. Nygren & Ola Lindgren)
			// Check if there's any sampletrees, if not there's no need to calibrate (J. Nygren); 090227 p�d
			// OBS! Num of trees are by specie; 090227 p�d

			// Check if there's any sampletrees
			if (getNumOfSampleTrees(nSpcID) > 0)
			{
				m_fCalibrationFactor = getCalibrationFactor();
				// Calibration is done, depending on specie
				if (nSpcID == 2) // Spruce
				{
					// Calculated factor to tree; 070425 p�d
					fHgt *= m_fCalibrationFactor;
				}
				else
				{
					if ((fHgt+m_fCalibrationFactor) > 0.0)
					{
						fHgt += m_fCalibrationFactor;
					}	// if ((fHgt+fFactor) > 0.0)
				}
			}	// if (m_nNumOfSampleTrees > 0)
			// Add calculated height to tree; 070425 p�d
			// OBS! Convert from (m) to (mm); 070521 p�d
			m_vecDCLSTrees[i].setHgt(fHgt);
		}	// if (tree.getSpcID() == nSpcID && (tree.getTreeType() == NOT_SAMPLE_TREE || tree.getTreeType() == TREE_WITHIN_NO_SAMPLE))

		if (m_fSoderbergsH25Value > 0.0)
			setTraktData_h25(nSpcID,m_fSoderbergsH25Value/10.0);	// From (mm) to (cm)
		else
			setTraktData_h25(nSpcID,0.0);	// From (mm) to (cm)
		if (m_fSoderbergsHGVValue > 0.0)
			setTraktData_hgv(nSpcID,m_fSoderbergsHGVValue/10.0);	// From (mm) to (cm)
		else
			setTraktData_hgv(nSpcID,0.0);	// From (mm) to (cm)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)
	
	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;
	
	return TRUE;
}

double CHeightSoderbergs::getSoderbergsH25Value(void)
{
	return m_fSoderbergsH25Value;
}
