#include "StdAfx.h"

#include "BarkSoderbergs.h"


//==========================================================================
// PRIVATE

// Constants for "S�derberg" bark function
// OBS! "F�r Gran (Spruce), Bj�rk (Birch) och �vrigt l�f (Other leaf) Norra och Mellersta Sverige slagits samman.

// PINE (Tall)
const double Pine_N[12]	=	{ -402.25, 15037.0, 0.00044577, -0.00015147, -0.013581, -0.0000016395, 0.088075, -0.011552, -0.069739, -0.082879, 5.3324, 0.232 };
const double Pine_M[12] =	{ -394.22, 14040.0, 0.00030388, -0.00092527, -0.064192, -0.00031573, 0.12632, -0.046079, 0.058621, -0.094391, 8.6428, 0.229 };
const double Pine_S[14] = { -383.60, 13442.0, 0.0020965, -0.0000088795, -0.00074698, 0.010185, -0.00017023, -0.024281, -0.049230, 0.057067, 0.024519, 0.19141, 4.7240, 0.238 };

// Gran (Spruce)
const double Spruce_N_M[12] = { -236.33, 7878.40, 0.0013589, 0.0000062227, -0.0015491, 0.028379, 0.35929, 0.057123, 0.020245, -0.071409, 1.6604, 0.237 };
const double Spruce_S[14]	= { -303.55, 13763.0, 0.000023539, -0.0013014, -0.010863, 0.00019027, 0.30230, 0.068055, -0.10406, 0.062182, 0.027539, 0.23053, 3.6138, 0.245};

// Bj�rk (Birch)
const double Birch_N_M[10] = { -371.31, 13012.0, 0.0019655, -0.00071109, 0.0086881, 0.0000062991, 0.17146, 0.18594, 3.1740, 0.293 };
const double Birch_S[12] = { -647.99, 33167.0, 0.0014517, -0.00050779, 0.0054445, -0.000099383, 0.13804, 0.088745, -0.14772, -0.051335, 5.0104, 0.354 };
//�ndrat 2010-02-19 J� Birch_S[10]=5.0104 det var 5.50104 f�rut vilket gav st�rre barkavdrag

// �vtigt L�v (Other leaf)
const double OthLeaf_N_M[8]	= { -175.63, 0.0049609, 0.26968, 0.29703, -0.077013, 0.086920, 2.8446, 0.388 };
const double OthLeaf_S[7]	= { -341.44, 9790.0, 0.0031101, -0.000022562, -0.0021013, 4.5835, 0.298 };

// Bok (Beech)
const double Beech_All[6] = { -173.87, 0.002597, 0.1635, -0.26953, 2.4822, 0.255 };

// Ek (Oak)
const double Oak_All[7] = { -296.05, 8723.5, 0.0022680, -0.24349, 0.044474, 3.9521, 0.217 };

// PRIVATE:

//======================================================
// Tall (Pine)

//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CBarkSoderbergs::getBarkPine_N(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (Pine_N[0]/(getClassMid(tree) + 50.0)) +
									(Pine_N[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Pine_N[2]*getTrakt().getTraktAge()) +
									(Pine_N[3]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Pine_N[4]*getTrakt().getTraktLatitude()) +
									(Pine_N[5]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Pine_N[6]*(getDiamQuata(getClassMid(tree)))) +
									(Pine_N[7]*pow(getDiamQuata(getClassMid(tree)),2)) +
									(Pine_N[8]*getSpcPercent(1)) +	// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Pine_N[9]*getTrakt().getTraktNearCoast()) +
									(Pine_N[10]) +	// A constant
									(pow(Pine_N[11],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}

//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Gran"
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CBarkSoderbergs::getBarkPine_M(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (Pine_M[0]/(getClassMid(tree) + 50.0)) +
									(Pine_M[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Pine_M[2]*getTrakt().getTraktAge()) +
									(Pine_M[3]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Pine_M[4]*getTrakt().getTraktLatitude()) +
									(Pine_M[5]*getTrakt().getTraktHgtOverSea()) +
									(Pine_M[6]*(getDiamQuata(getClassMid(tree)))) +
									(Pine_M[7]*pow(getDiamQuata(getClassMid(tree)),2)) +
									(Pine_M[8]*getSpcPercent(2)) +	// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Pine_M[9]*getTrakt().getTraktNearCoast()) +
									(Pine_M[10]) +	// A constant
									(pow(Pine_M[11],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}


//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
// - "Delad yta" Y/N; Y = 1, N = 0
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CBarkSoderbergs::getBarkPine_S(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (Pine_S[0]/(getClassMid(tree) + 50.0)) +
									(Pine_S[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Pine_S[2]*getTrakt().getTraktAge()) +
									(Pine_S[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(Pine_S[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Pine_S[5]*getTrakt().getTraktHgtOverSea()) +
									(Pine_S[6]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Pine_S[7]*getSpcPercent(1)) +	// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Pine_S[8]*getSpcPercent(3)) +	// Get percentage for specie id = 3 i.e. Birch (Bj�rk)
									(Pine_S[9]*getTrakt().getTraktSoutheast()) +
									(Pine_S[10]*getTrakt().getTraktPartOfPlot()) +
									(Pine_S[11]*getTrakt().getTraktNearCoast()) +
									(Pine_S[12]) +	// A constant
									(pow(Pine_S[13],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}


//======================================================
// Spruce (Gran)

//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Delad yta" Y/N; Y = 1, N = 0
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CBarkSoderbergs::getBarkSpruce_NandM(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (Spruce_N_M[0]/(getClassMid(tree) + 50.0)) +
									(Spruce_N_M[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Spruce_N_M[2]*getTrakt().getTraktAge()) +
									(Spruce_N_M[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(Spruce_N_M[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Spruce_N_M[5]*getTrakt().getTraktLatitude()) +
									(Spruce_N_M[6]*(getDiamQuata(getClassMid(tree)))) +
									(Spruce_N_M[7]*getSpcPercent(1)) +	// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Spruce_N_M[8]*getTrakt().getTraktPartOfPlot()) +
									(Spruce_N_M[9]*getTrakt().getTraktNearCoast()) +
									(Spruce_N_M[10]) +	// A constant
									(pow(Spruce_N_M[11],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}


//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Gran"
// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
// - "Delad yta" Y/N; Y = 1, N = 0
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CBarkSoderbergs::getBarkSpruce_S(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (Spruce_S[0]/(getClassMid(tree) + 50.0)) +
									(Spruce_S[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Spruce_S[2]*pow((double)getTrakt().getTraktAge(),2)) +
									(Spruce_S[3]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Spruce_S[4]*getTrakt().getTraktHgtOverSea()) +
									(Spruce_S[5]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Spruce_S[6]*(getDiamQuata(getClassMid(tree)))) +
									(Spruce_S[7]*getSpcPercent(1)) +	// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Spruce_S[8]*getSpcPercent(2)) +	// Get percentage for specie id = 2 i.e. Spruce (Gran)
									(Spruce_S[9]*getSpcPercent(3)) +	// Get percentage for specie id = 3 i.e. Birch (Bj�rk)
									(Spruce_S[10]*getTrakt().getTraktPartOfPlot()) +
									(Spruce_S[11]*getTrakt().getTraktNearCoast()) +
									(Spruce_S[12]) +	// A constant
									(pow(Spruce_S[13],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}

//======================================================
// Birch (Bj�rk)

//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
double CBarkSoderbergs::getBarkBirch_NandM(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (Birch_N_M[0]/(getClassMid(tree) + 50.0)) +
									(Birch_N_M[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Birch_N_M[2]*getTrakt().getTraktAge()) +
									(Birch_N_M[3]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Birch_N_M[4]*getTrakt().getTraktLatitude()) +
									(Birch_N_M[5]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Birch_N_M[6]*(getDiamQuata(getClassMid(tree)))) +
									(Birch_N_M[7]*getSpcPercent(1)) +	// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Birch_N_M[8]) +	// A constant
									(pow(Birch_N_M[9],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}



//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
// - Height over the sea
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Tall"
// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
double CBarkSoderbergs::getBarkBirch_S(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (Birch_S[0]/(getClassMid(tree) + 50.0)) +
									(Birch_S[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Birch_S[2]*getTrakt().getTraktAge()) +
									(Birch_S[3]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(Birch_S[4]*getTrakt().getTraktHgtOverSea()) +
									(Birch_S[5]*getTrakt().getTraktHgtOverSea()*getTrakt().getTraktLatitude()) +
									(Birch_S[6]*(getDiamQuata(getClassMid(tree)))) +
									(Birch_S[7]*getSpcPercent(1)) +	// Get percentage for specie id = 1 i.e. Pine (Tall)
									(Birch_S[8]*getSpcPercent(3)) +	// Get percentage for specie id = 3 i.e. Birch (Bj�rk)
									(Birch_S[9]*getTrakt().getTraktSoutheast()) +
									(Birch_S[10]) +	// A constant
									(pow(Birch_S[11],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}


//======================================================
// Other Leaf (�vrigt L�v)

//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - Quota for DBH/Max diameter
// - "Tr�dslagsf�rdelning Bj�rk"
// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
double CBarkSoderbergs::getBarkOthLeaf_NandM(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (OthLeaf_N_M[0]/(getClassMid(tree) + 50.0)) +
									(OthLeaf_N_M[1]*getTrakt().getTraktAge()) +
									(OthLeaf_N_M[2]*(getDiamQuata(getClassMid(tree)))) +
									(OthLeaf_N_M[3]*getSpcPercent(3)) +	// Get percentage for specie id = 3 i.e. Birch (Bj�rk)
									(OthLeaf_N_M[4]*getTrakt().getTraktPartOfPlot()) +
									(OthLeaf_N_M[5]*getTrakt().getTraktNearCoast()) +
									(OthLeaf_N_M[6]) +	// A constant
									(pow(OthLeaf_N_M[7],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}


//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - SI value of H100; ex. G30 = 300
// - "Breddgrad"
double CBarkSoderbergs::getBarkOthLeaf_S(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (OthLeaf_S[0]/(getClassMid(tree) + 50.0)) +
									(OthLeaf_S[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(OthLeaf_S[2]*getTrakt().getTraktAge()) +
									(OthLeaf_S[3]*pow((double)getTrakt().getTraktAge(),2)) +
									(OthLeaf_S[4]*getSIfromH100(getTrakt().getTraktSIH100_Pine())) +
									(OthLeaf_S[5]) +	// A constant
									(pow(OthLeaf_S[6],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}


//======================================================
// Beech (Bok) All species

//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - Quota for DBH/Max diameter
// - "Region 5" Y/N; Y = 1, N = 0
double CBarkSoderbergs::getBarkBeech(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (Beech_All[0]/(getClassMid(tree) + 50.0)) +
									(Beech_All[1]*getTrakt().getTraktAge()) +
									(Beech_All[2]*(getDiamQuata(getClassMid(tree)))) +
									(Beech_All[3]*getTrakt().getTraktRegion5()) +
									(Beech_All[4]) +	// A constant
									(pow(Beech_All[5],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}


//======================================================
// Oak (Ek) All species

//---------------------------------------------------
// Arguments used in this function; 0700425 p�d
// - Diameter at breast height
// - "Best�nds-�lder"
// - Quota for DBH/Max diameter
// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
// - "Delad yta" Y/N; Y = 1, N = 0
double CBarkSoderbergs::getBarkOak(CTransaction_dcls_tree tree)
{
	// Calculate bark reduction ("dubbla")
	double fValue = (Oak_All[0]/(getClassMid(tree) + 50.0)) +
									(Oak_All[1]/(pow(getClassMid(tree) + 50.0,2))) +
									(Oak_All[2]*getTrakt().getTraktAge()) +
									(Oak_All[3]*getTrakt().getTraktSoutheast()) +
									(Oak_All[4]*getTrakt().getTraktPartOfPlot()) +
									(Oak_All[5]) +	// A constant
									(pow(Oak_All[6],2)/2.0);
	return exp(fValue)/2.0;	// "Enkel barktjocklek"
}


//==========================================================================
// PROTECTED

double CBarkSoderbergs::setBarkForPine(int bark_index,CTransaction_dcls_tree tree)
{
	// Check index of function to use
	switch (bark_index)
	{
		case 0 :	// Tall norra Sverige
		{
			return getBarkPine_N(tree);
		}
		case 1 :	// Tall mellersta Sverige
		{
			return getBarkPine_M(tree);
		}
		case 2 :	// Tall s�dra Sverige
		{
			return getBarkPine_S(tree);
		}
	};
	return 0.0;
}

double CBarkSoderbergs::setBarkForSpruce(int bark_index,CTransaction_dcls_tree tree)
{
	// Check index of function to use
	switch (bark_index)
	{
		case 0 :	// Gran norra och mellersta Sverige
		{
			return getBarkSpruce_NandM(tree);
		}
		case 1 :	// Gran s�dra Sverige
		{
			return getBarkSpruce_S(tree);
		}
	};
	return 0.0;
}

double CBarkSoderbergs::setBarkForBirch(int bark_index,CTransaction_dcls_tree tree)
{
	// Check index of function to use
	switch (bark_index)
	{
		case 0 :	// Bj�rk norra och mellersta Sverige
		{
			return getBarkBirch_NandM(tree);
		}
		case 1 :	// Bj�rk s�dra Sverige
		{
			return getBarkBirch_S(tree);
		}
	};
	return 0.0;
}

double CBarkSoderbergs::setBarkForOtherLeaf(int bark_index,CTransaction_dcls_tree tree)
{
	// Check index of function to use
	switch (bark_index)
	{
		case 0 :	// �vrigt l�v norra och mellersta Sverige
		{
			return getBarkOthLeaf_NandM(tree);
		}
		case 1 :	//  �vrig l�v s�dra Sverige
		{
			return getBarkOthLeaf_S(tree);
		}
	};
	return 0.0;
}

double CBarkSoderbergs::setBarkForBeech(int bark_index,CTransaction_dcls_tree tree)
{
	return getBarkBeech(tree);
}

double CBarkSoderbergs::setBarkForOak(int bark_index,CTransaction_dcls_tree tree)
{
	return getBarkOak(tree);
}

//==========================================================================
// PUBLIC:

CBarkSoderbergs::CBarkSoderbergs(void)
	: CCalculationBaseClass()
{
}

CBarkSoderbergs::CBarkSoderbergs(int spc_index,CTransaction_trakt rec1,
																							 CTransaction_trakt_misc_data rec2,
																							 vecTransactionTraktData &tdata_list,
																							 vecTransactionSampleTree &sample_tree_list,
																							 vecTransactionDCLSTree &dcls_tree_list,
																							 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}

BOOL CBarkSoderbergs::calculate(vecTransactionTraktData& vec1,
																vecTransactionSampleTree& vec2,
																vecTransactionDCLSTree& vec4,	// Adeed 070820 p�d
																vecTransactionTraktSetSpc& vec3)
{
	int nBarkSpcID = -1;
	int nBarkIndex = -1;
	int nSpcID = -1;

	double fBark = 0.0;
	// Check that there's any species; 070418 p�d
	if (m_nNumOfDCLSSpecies == 0)
		return FALSE;

	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nBarkSpcID = getBarkSpcID();	
	nBarkIndex = getBarkIndex();
	// Loop through the tree list (m_vecSampleTrees) and
	// calculate bark reduxtion for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getBarkID() to get which species
	// method to use and getBarkIndex() to get
	// the specific function within the method,
	// declared in UCCalculationBaseClass; 070424 p�d
	for (long i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		// Check for Specie; 070424 p�d
		if (tree.getSpcID() == nSpcID)
		{
			// Select function depending on specie and
			// index of function within specie; 070424 p�d
			if (nBarkSpcID == 1)	// Pine
			{
				fBark = setBarkForPine(nBarkIndex,tree);
			}
			else if (nBarkSpcID == 2)	// Spruce
			{
				fBark = setBarkForSpruce(nBarkIndex,tree);
			}
			else if (nBarkSpcID == 3)	// Birch
			{
				fBark = setBarkForBirch(nBarkIndex,tree);
			}
			else if (nBarkSpcID == 4)	// Oth. Leaf
			{
				fBark = setBarkForOtherLeaf(nBarkIndex,tree);
			}
			else if (nBarkSpcID == 5)	// Beech
			{
				fBark = setBarkForBeech(nBarkIndex,tree);
			}
			else if (nBarkSpcID == 6)	// Oak
			{
				fBark = setBarkForOak(nBarkIndex,tree);
			}

		// Add calculated bark-thickness to tree; 070425 p�d
			m_vecDCLSTrees[i].setBarkThick(fBark); // *2.0);	// "Dubbla barktjockleken"; 071101 p�d
		};
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}
