/* 2007-04-25 Per-�ke Danielsson */
#if !defined(AFX_BARKSODERBERGS_H)
#define AFX_BARKSODERBERGS_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

//#include "pad_calculation_classes.h"

////////////////////////////////////////////////////////////////////////////////////////
// CBarkSoderbergs

class CBarkSoderbergs : public CCalculationBaseClass
{
//private:
	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Bark over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getBarkPine_N(CTransaction_dcls_tree tree);

	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Bark over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Gran"
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getBarkPine_M(CTransaction_dcls_tree tree);

	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Bark over the sea
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
	// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
	// - "Delad yta" Y/N; Y = 1, N = 0
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getBarkPine_S(CTransaction_dcls_tree tree);

	//======================================================
	// Spruce (Gran)

	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Bark over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Delad yta" Y/N; Y = 1, N = 0
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getBarkSpruce_NandM(CTransaction_dcls_tree tree);

	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Bark over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Gran"
	// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
	// - "Delad yta" Y/N; Y = 1, N = 0
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getBarkSpruce_S(CTransaction_dcls_tree tree);

	//======================================================
	// Birch (Bj�rk)

	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Bark over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	double getBarkBirch_NandM(CTransaction_dcls_tree tree);


	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Bark over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
	// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
	double getBarkBirch_S(CTransaction_dcls_tree tree);

	//======================================================
	// Other Leaf (�vrigt L�v)

	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Bj�rk"
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getBarkOthLeaf_NandM(CTransaction_dcls_tree tree);

	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	double getBarkOthLeaf_S(CTransaction_dcls_tree tree);

	//======================================================
	// Beech (Bok) All species

	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - Quota for DBH/Max diameter
	// - "Region 5" Y/N; Y = 1, N = 0
	double getBarkBeech(CTransaction_dcls_tree tree);

	//======================================================
	// Oak (Ek) All species

	//---------------------------------------------------
	// Arguments used in this function; 0700425 p�d
	// - Diameter at breast Bark
	// - "Best�nds-�lder"
	// - Quota for DBH/Max diameter
	// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
	// - "Delad yta" Y/N; Y = 1, N = 0
	double getBarkOak(CTransaction_dcls_tree tree);

protected:
	// All species
	double setBarkForPine(int bark_index,CTransaction_dcls_tree tree);	
	double setBarkForSpruce(int bark_index,CTransaction_dcls_tree tree);	
	double setBarkForBirch(int bark_index,CTransaction_dcls_tree tree);	
	double setBarkForOtherLeaf(int bark_index,CTransaction_dcls_tree tree);	
	double setBarkForBeech(int bark_index,CTransaction_dcls_tree tree);	
	double setBarkForOak(int bark_index,CTransaction_dcls_tree tree);	
public:
	CBarkSoderbergs(void);
	CBarkSoderbergs(int spc_index,CTransaction_trakt rec1,
																CTransaction_trakt_misc_data rec2,
																vecTransactionTraktData &tdata_list,
																vecTransactionSampleTree &sample_tree_list,
																vecTransactionDCLSTree &dcls_tree_list,
																vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);

};


#endif