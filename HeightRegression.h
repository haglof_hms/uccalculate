/* 2007-04-13 Per-�ke Danielsson */
#if !defined(AFX_HEIGHTREGRESSION_H)
#define AFX_HEIGHTREGRESSION_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

#include "MessageDlg.h"

////////////////////////////////////////////////////////////////////////////////////////
// CHeightRegression

class CHeightRegression : public CCalculationBaseClass
{
	//private
protected:
	// All species except Spruce
	BOOL setHeightForTrees(void);	
	BOOL setH25ForSpecie(void);
	BOOL setHGVForSpecie(void);
	void getKMFactors(int cnt_spc,double log_dbh,double hgt,double dbh2,double log_dbh_h,double *k_factor,double *m_factor);

public:
	CHeightRegression(void);
	CHeightRegression(int spc_index,CTransaction_trakt rec1,
																	CTransaction_trakt_misc_data rec2,
																	vecTransactionTraktData &tdata_list,
																	vecTransactionSampleTree &sample_tree_list,
																	vecTransactionDCLSTree &dcls_tree_list,
																	vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);

};



#endif