#include "StdAfx.h"

#include "BarkSF.h"


// PUBLIC:
CBarkSF::CBarkSF(void)
	: CCalculationBaseClass()
{
}

CBarkSF::CBarkSF(int spc_index,CTransaction_trakt rec1,
															 CTransaction_trakt_misc_data rec2,
															 vecTransactionTraktData &tdata_list,
															 vecTransactionSampleTree &sample_tree_list,
															 vecTransactionDCLSTree &dcls_tree_list,
															 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}

BOOL CBarkSF::calculate(vecTransactionTraktData& vec1,
												vecTransactionSampleTree& vec2,
												vecTransactionDCLSTree& vec4,		// Added 070820 p�d
												vecTransactionTraktSetSpc& vec3)
{
	double fBarkReduction = 0.0;
	int nBarkSpcID = -1;	// Function for specie is used; 070417 p�d
	int nBarkIndex = -1;  // Which function for specie is used; 070417 p�d
	int nSpcID = -1;
	// First check that there's any trees to begin with.
	// If not, just return FALSE; 070417 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nBarkSpcID = getBarkSpcID();	
	nBarkIndex = getBarkIndex();
	// Loop through the tree list (m_vecSampleTrees) and
	// calculate bark-reduction for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getBarkID() to get which species
	// method to use and getBarkIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070417 p�d
	for (long i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		if (tree.getSpcID() == nSpcID)
		{
			// Depending on specie, run function accoring to
			// nBarkIndex value; 070417 p�d
			if (nBarkSpcID == 1)	// Tall
			{
				fBarkReduction = getBarkPine(getClassMid(tree),	// mm
																			130.0,	// "Br�sth�jd"; 090305 p�d
																		 //tree.getHgt(),	// dm
																		 getTrakt().getTraktLatitude());
			}
			if (nBarkSpcID == 2) // Gran
			{
				fBarkReduction = getBarkSpruce(getClassMid(tree)); // mm
			}
			m_vecDCLSTrees[i].setBarkThick(fBarkReduction);
		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}

// PROTECTED
double CBarkSF::getBarkPine(double dbh,double hgt,int latitude)
{
	double fDBH = dbh;
	double fHgt = hgt;
	int nLat = latitude;
	double fDBHb = 0.0;
	double fHgt_break;
	double fBark2;	// "Dubbla barktjockleken"

	// If fDBH > MAX_DBH_IN_SF, this'll set the fDBHb = MAX_DBH_IN_SF mm
	fDBHb = min(fDBH,MAX_DBH_IN_SF);

	// Caluclate a "breakpoint" for the function in cm
	fHgt_break = -log(0.12/(72.1814+0.0789*fDBHb-0.9868*nLat))/(0.0078557-0.0000132*fDBHb);
	
	// Caluclate the bark-thickness ("dubbla") below breakpoint in mm
	fBark2	= 3.5808+0.0109*fDBHb+(72.1814+0.0789*fDBHb-0.9868*nLat)*exp(-(0.0078557-0.0000132*fDBHb)*fHgt);
	
	// Check if fHgt > fHgt_break, if so, calculate bark-thickness above breakpoint in mm
	if (fHgt > fHgt_break)
	{
		fBark2 = (3.5808+0.0109*fDBHb+0.12-0.005*(fHgt-fHgt_break));
	}
	
	// If bark-thickness is less than MIN_BARK_IN_SF mm the thickness is set to MIN_BARK_IN_SF mm
	if (fBark2 < MIN_BARK_IN_SF)
	{
		fBark2 = MIN_BARK_IN_SF;
	}
	
	return fBark2/2.0;	// "Enkel barktjocklek"
}

double CBarkSF::getBarkSpruce(double dbh)
{
	double fDBH = dbh;
	double fRelDia;	
	double fBark2; 

	// This value is calculated as dia/dbh (dia = active diameter)
	// In this case the dia = dbh, ergo fRelDia = 1.0; 070418 p�d
	fRelDia = 1.0;

	// Caluclate the bark-thickness ("dubbla") in mm
	fBark2 = 0.46146+0.01386*fDBH+0.03571*fDBH*fRelDia;

	// If bark-thickness is less than MIN_BARK_IN_SF mm the thickness is set to MIN_BARK_IN_SF mm
	fBark2 = max(fBark2,MIN_BARK_IN_SF);

	return fBark2/2.0;	// "Enkel barktjocklek"
}
