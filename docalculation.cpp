#include "StdAfx.h"

#include "DoCalculation.h"

UCDoCalculation::UCDoCalculation(void)
{
	m_vecDCLSTrees.clear();
	m_vecSampleTrees.clear();
	m_vecSpecies.clear();
}

UCDoCalculation::UCDoCalculation(CTransaction_trakt rec1,
																 CTransaction_trakt_misc_data rec2,
																 vecTransactionTraktData &tdata_list,
																 vecTransactionSampleTree &sample_tree_list,
																 vecTransactionDCLSTree &dcls_tree_list,
																 vecTransactionTraktSetSpc &spc_list)
{
	m_recTrakt = rec1;
	m_recMiscData = rec2;
	m_vecTraktData = tdata_list;
	m_vecSampleTrees	= sample_tree_list;
	m_vecDCLSTrees	= dcls_tree_list;
	m_vecSpecies = spc_list;

	setTreeData();
}

// PRIVATE

void UCDoCalculation::setTreeData(void)
{
	long i;
	int nSpc;
	int nNumOfSampleTreesForSpc = 0;
	int nNumOfDCLSTreesForSpc = 0;
	int nNumOfTreesForSpc = 0;
	int nNumOfTreesInDCLS = 0;	// Added 080307 p�d
	double fDBH = 0.0;
	double fSumDiam = 0.0;
	double fAvgDiam = 0.0;
	double fSumHgtPerSpc = 0.0;
	double fAvgHgt = 0.0;
	double fMaxDia = 0.0;
	double fSpcPerc = 0.0;
	double fSumBaselAreaPerSpc = 0.0;
	double fSumBaselArea = 0.0;

	if (m_vecSampleTrees.size() > 0 && m_vecSpecies.size() > 0)
	{
		m_vecTreeData.clear();
		for (UINT ii = 0;ii < m_vecSpecies.size();ii++)
		{
			nSpc = m_vecSpecies[ii].getSpcID();

			nNumOfSampleTreesForSpc = 0;
			nNumOfDCLSTreesForSpc = 0;
			nNumOfTreesForSpc = 0;
			fSumDiam = 0.0;
			fAvgDiam = 0.0;
			fAvgHgt = 0.0;
			fMaxDia = 0.0;
			fSpcPerc = 0.0;
			fSumBaselAreaPerSpc = 0.0;
			fSumBaselArea = 0.0;
			for (i = 0;i < m_vecSampleTrees.size();i++)
			{
				//---------------------------------------------------------
				// Information on SampleTrees
				if ((m_vecSampleTrees[i].getTreeType() == SAMPLE_TREE || 
						 m_vecSampleTrees[i].getTreeType() == TREE_SAMPLE_REMAINIG || 
						 m_vecSampleTrees[i].getTreeType() == TREE_SAMPLE_EXTRA || 
						 m_vecSampleTrees[i].getTreeType() == TREE_OUTSIDE) && 
						 m_vecSampleTrees[i].getSpcID() == nSpc &&
						 m_vecSampleTrees[i].getHgt() > 0.0)
				{
					// Sum. number of sampletrees for specie
					nNumOfSampleTreesForSpc++;
				}	// if (m_vecSampleTrees[i].getTypeOfTree() == 1 && m_vecSampleTrees[i].getSpcID() == nSpc)
				// Max diamter is not depending on Specie; 080626 p�d
				// .. also get max diameter from Sampletrees; 080626 p�d
				fMaxDia = max(fMaxDia,m_vecSampleTrees[i].getDBH());
			} // for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

			// Diameterclasses
			fSumHgtPerSpc = 0.0;
			for (i = 0;i < m_vecDCLSTrees.size();i++)
			{
				//---------------------------------------------------------
				// Information on DCLSTrees
				if (m_vecDCLSTrees[i].getSpcID() == nSpc)
				{
					// Adaption for "Intr�ngsv�rdering":
					// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
					// OBS! This only allies to "Intr�ng"; 080307 p�d
					nNumOfTreesInDCLS = (m_vecDCLSTrees[i].getNumOf()+m_vecDCLSTrees[i].getNumOfRandTrees());
					fSumBaselAreaPerSpc += ((pow(getClassMid(m_vecDCLSTrees[i]),2)*M_PI)/4.0)*nNumOfTreesInDCLS;

					nNumOfTreesForSpc += nNumOfTreesInDCLS;
				
					// Get sum. of diametreclass; 070417 p�d
					if (getClassMid(m_vecDCLSTrees[i]) > 0.0)
					{
						fSumDiam += getClassMid(m_vecDCLSTrees[i]);
					} // if (data[i].m_vecDCLSTrees() > 0.0)
					if (m_vecDCLSTrees[i].getHgt() > 0.0)
					{
						fSumHgtPerSpc += (m_vecDCLSTrees[i].getHgt()*nNumOfTreesInDCLS)/10.0;
					} // if (data[i].getDBH() > 0.0)
				}	// if (m_vecSampleTrees[i].getTypeOfTree() == 1 && m_vecSampleTrees[i].getSpcID() == nSpc)

				// Max diamter is not depending on Specie; 080626 p�d
				fMaxDia = max(fMaxDia,getClassMid(m_vecDCLSTrees[i]));
				// Calculate Total Sum baselarea; 070523 p�d
				fSumBaselArea += ((pow(getClassMid(m_vecDCLSTrees[i]),2)*M_PI)/4.0)*nNumOfTreesInDCLS;
			}
			//---------------------------------------------------------
			// Try to find out the Diamterclass for each tree, and
			// add to Tree; 070524 p�d		
			for (i = 0;i < m_vecSampleTrees.size();i++)
			{
				// Check if we have a diamterclass to work with.
				// If not set diamgerclass = 0.0; 070615 p�d
				if (m_recMiscData.getDiamClass() > 0.0)
				{
					if (m_vecSampleTrees[i].getSpcID() == nSpc)
					{
						fDBH = m_vecSampleTrees[i].getDBH()/10.0;	// From (mm) to (cm)
						for (double fDCLS = m_recMiscData.getDiamClass(); 
								 fDCLS < (fMaxDia/10.0 + m_recMiscData.getDiamClass());	// Add one diameterclass to make sure the largset diameter
																																				// also is included in a diamterclass; 070528 p�d
								 fDCLS += m_recMiscData.getDiamClass())
						{
							if (fDBH >= fDCLS && fDBH < fDCLS + m_recMiscData.getDiamClass())
							{
								m_vecSampleTrees[i].setDCLS_from(fDCLS);	// From (cm) to (mm)
								m_vecSampleTrees[i].setDCLS_to(fDCLS+m_recMiscData.getDiamClass());	//To (cm) to (mm)
/*
						CString S;
						S.Format("UCDoCalculation::setTreeData\nm_vecTrees[i].getSpcID() %d\nfDBH %f\nm_vecTrees[i].getDCLS() %f",m_vecSampleTrees[i].getSpcID(),fDBH,m_vecSampleTrees[i].getDCLS());
						AfxMessageBox(S);
*/
							}	// if (fDBH >= fDCLS && fDBH < fDCLS + m_recMiscData.getDiamClass())
						}	// for (double fDCLS = m_recMiscData.getDiamClass(); fDCLS < fMaxDia;fDCLS += m_recMiscData.getDiamClass())
					}	// if (m_vecSampleTrees[i].getSpcID() == nSpc)
				}	// if (m_recMiscData.getDiamClass() > 0.0)
				else
				{
					m_vecSampleTrees[i].setDCLS_from(0.0);	// No diamterclass to work with; 070615 p�d
					m_vecSampleTrees[i].setDCLS_to(0.0);	// No diamterclass to work with; 070615 p�d
				}
			}	// for (i = 0;i < m_nNumOfSampleTrees;i++)
			//-----------------------------------------------------------
			// Sampletree specifics; 070424 p�d
			if (fSumDiam > 0.0 && nNumOfSampleTreesForSpc > 0)
			{
				// Calculate avg. diameter; 070417 p�d
				fAvgDiam = fSumDiam/nNumOfSampleTreesForSpc;
			}
			if (fSumHgtPerSpc > 0.0 && nNumOfTreesForSpc > 0)
			{
				// Calculate avg. height; 070417 p�d
				fAvgHgt = fSumHgtPerSpc/nNumOfTreesForSpc;
			}
			//-----------------------------------------------------------
/*
			if (m_nNumOfSampleTrees > 0 && nNumOfTreesForSpc > 0)
			{
				// Calculate percantage of Specie (Tr�dslagsblandning); 070424 p�d
				fSpcPerc = (double)nNumOfTreesForSpc/m_nNumOfSampleTrees;
			}

			// Calculate percentage of three based on baselarea; 070523 p�d
			if (fSumBaselArea > 0 && fSumBaselAreaPerSpc > 0)
			{
				// Calculate percantage of Specie (Tr�dslagsblandning); 070424 p�d
				fSpcPerc = fSumBaselAreaPerSpc/fSumBaselArea;
			}
*/
			m_vecTreeData.push_back(_tree_data(nSpc,
																				 nNumOfSampleTreesForSpc,
																				 nNumOfDCLSTreesForSpc,
																				 fSumDiam,
																				 fAvgDiam,
																				 fAvgHgt,
																				 nNumOfTreesForSpc,
																				 fMaxDia,
																				 0.0));
		}
	}	// if (m_nNumOfSampleTrees > 0 && m_nNumOfSampleSpecies > 0)


}

// Extended information on trees. 
// Calculate DA,DG,DGV,HGV,GY, SumM3Sk,SumM3Fub
void UCDoCalculation::setProTraktData(void)
{
	CString S;
	double fDCLS = 0.0;
	double fSumGY = 0.0;
	double fSumGYPerSpc = 0.0;
	double fSumDPerSpc = 0.0;
	double fSumD2PerSpc = 0.0;
	double fSumD3PerSpc = 0.0;
	double fGY = 0.0;
	double fDA = 0.0;
	double fDG = 0.0;
	double fDGV = 0.0;
	double fM3SkPerSpc = 0.0;
	double fSumM3SkPerSpc = 0.0;
	double fSumM3Sk = 0.0;
	double fSumHgtPerSpc = 0.0;
	double fHgtPerSpc;
	long nNumOf = 0;
	long nNumOfTreesPerSpc = 0;
	int nSpc = 0;
	if (m_vecDCLSTrees.size() > 0 && m_vecTraktData.size() > 0)
	{
		for (UINT ii = 0;ii < m_vecTraktData.size();ii++)
		{
			nSpc = m_vecTraktData[ii].getSpecieID();

			nNumOfTreesPerSpc = 0;
			fSumGY = 0.0;
			fSumGYPerSpc = 0.0;
			fSumDPerSpc	 = 0.0;
			fSumD2PerSpc = 0.0;
			fSumD3PerSpc = 0.0;
			fGY = 0.0;
			fDA = 0.0;
			fDG = 0.0;
			fDGV = 0.0;
			fM3SkPerSpc = 0.0;
			fSumM3SkPerSpc = 0.0;
			fSumM3Sk = 0.0;
			fSumHgtPerSpc = 0.0;
			for (UINT i = 0;i < m_vecDCLSTrees.size();i++)
			{
				fDCLS = getClassMid(m_vecDCLSTrees[i]);
				// Adaption for "Intr�ngsv�rdering":
				// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
				// OBS! This only allies to "Intr�ng"; 080307 p�d
				nNumOf = m_vecDCLSTrees[i].getNumOf()+m_vecDCLSTrees[i].getNumOfRandTrees();
				fM3SkPerSpc = m_vecDCLSTrees[i].getM3sk();
				fHgtPerSpc = m_vecDCLSTrees[i].getHgt();
				// Calculate Sum baselarea ("Grundyta") for specie; 070524 p�d
				if (m_vecDCLSTrees[i].getSpcID() == nSpc)
				{

					fSumGYPerSpc	+= ((pow(fDCLS,2.0)*M_PI)/4000.0)*nNumOf; 
					fSumDPerSpc		+= fDCLS*nNumOf;
					fSumD2PerSpc	+= pow(fDCLS,2.0)*nNumOf;
					fSumD3PerSpc	+= pow(fDCLS,3.0)*nNumOf;
					fSumM3SkPerSpc += fM3SkPerSpc;
					fSumHgtPerSpc += (fHgtPerSpc*nNumOf)/10.0;	// (dm) => (m)

					nNumOfTreesPerSpc += nNumOf;
				}	// if (m_vecSampleTrees[i].getSpcID() == nSpc)

				fSumGY += ((pow(fDCLS,2.0)*M_PI)/4000.0)*nNumOf; 
				fSumM3Sk += fM3SkPerSpc;

			} // for (UINT i = 0;i < m_nNumOfSampleTrees;i++)


			if (fSumDPerSpc > 0.0 && fSumD2PerSpc > 0.0 && fSumD3PerSpc > 0.0 && nNumOfTreesPerSpc > 0)
			{
				// Calculate DA,DG,DGV etc.; 070524 p�d
				fDA = fSumDPerSpc/nNumOfTreesPerSpc;
				fDG = sqrt(fSumD2PerSpc/nNumOfTreesPerSpc);
				fDGV = fSumD3PerSpc/fSumD2PerSpc;
			}
			if ( fSumGYPerSpc > 0.0)
			{

				// Setup the GY value calculated without areal. Do the calaculation to ha
				// in main program; 070619 p�d
				fGY = fSumGYPerSpc; 
			}
/*
			CString S;
			S.Format(_T("UCDoCalculation::setProTraktData\nfSumGYPerSpc %f\nSumD %f\nSumD2 %f\nSumD3 %f\nNumOf %d\nDA %f\nDG %f\nDGV %f\nGY %f"),
				fSumGYPerSpc,fSumDPerSpc,fSumD2PerSpc,fSumD3PerSpc,nNumOfTreesPerSpc,fDA,fDG,fDGV,fGY);
			AfxMessageBox(S);
*/
			// Add information to m_vecTraktData; 070524 p�d
			m_vecTraktData[ii].setNumOf(nNumOfTreesPerSpc);
			// Calculate percent of tree using Baselarea; 071002 p�d
/*
			S.Format(_T("UCDoCalculation::setProTraktData Spc %d\nfSumM3SkPerSpc %f\nfSumM3Sk %f"),
					nSpc,fSumM3SkPerSpc,fSumM3Sk);
			AfxMessageBox(S);
*/
/*
			if (fSumM3SkPerSpc > 0.0 && fSumM3Sk > 0.0)
			{
				m_vecTraktData[ii].setPercent((fSumM3SkPerSpc/fSumM3Sk)*100.0);
			}
			else
			{
				m_vecTraktData[ii].setPercent(0.0);
			}
*/
			// Grundytev�gd tr�dslagsblandning; 100506 p�d
			if (fSumGYPerSpc > 0.0 && fSumGY > 0.0)
			{
				m_vecTraktData[ii].setPercent((fSumGYPerSpc/fSumGY)*100.0);
			}
			else
			{
				m_vecTraktData[ii].setPercent(0.0);
			}
/*
				S.Format(_T("UCDoCalculation::setProTraktData\nSpc %d\nfGY %f\nfSumGY %f"),
					nSpc,fGY,fSumGY);
				AfxMessageBox(S);
*/
			m_vecTraktData[ii].setGY(fGY/1000.0);
			m_vecTraktData[ii].setDA(fDA/10.0);	
			m_vecTraktData[ii].setDG(fDG/10.0);	
			m_vecTraktData[ii].setDGV(fDGV/10.0); 
			// Avoid division by zero; 071213 p�d
			if (fSumHgtPerSpc > 0.0 && nNumOfTreesPerSpc > 0)
			{
				m_vecTraktData[ii].setAvgHgt(fSumHgtPerSpc/nNumOfTreesPerSpc);
			}
			else
			{
				m_vecTraktData[ii].setAvgHgt(0.0);
			}
		}	// for (UINT ii = 0;ii < m_nNumOfSampleSpecies;ii++)
	}	// if (m_nNumOfSampleTrees > 0 && m_nNumOfSampleSpecies > 0)
}


void UCDoCalculation::setPostTraktData(void)
{
	CString S;
	double fSumM3Sk = 0.0;
	double fSumM3Fub = 0.0;
	double fSumM3Ub = 0.0;
	double fTotSumM3Sk = 0.0;
	double fAvgM3Sk = 0.0;
	double fAvgM3Fub = 0.0;
	double fAvgM3Ub = 0.0;
	double fAvgHgt = 0.0;
	double fSpcPerc = 0.0;
	double fSumHgtPerSpc = 0.0;
	long nNumOfPerSpc = 0;
	int nSpc = 0;
	int nNumOfTreesInDCLS = 0;
	if (m_vecDCLSTrees.size() > 0 && m_vecTraktData.size() > 0)
	{
		fTotSumM3Sk = 0.0;
		for (UINT ii = 0;ii < m_vecTraktData.size();ii++)
		{
			nNumOfPerSpc = 0;
			fSumM3Sk = 0.0;
			fSumM3Fub = 0.0;
			fSumM3Ub = 0.0;
			fAvgM3Sk = 0.0;
			fAvgM3Fub = 0.0;
			fAvgM3Ub = 0.0;
			fAvgHgt = 0.0;
			fSumHgtPerSpc = 0.0;
			fTotSumM3Sk = 0.0;
			fSpcPerc = 0.0;
			nSpc = m_vecTraktData[ii].getSpecieID();

			for (UINT i = 0;i < m_vecDCLSTrees.size();i++)
			{
				// Calculate Sum baselarea ("Grundyta") for specie; 070524 p�d
				if (m_vecDCLSTrees[i].getSpcID() == nSpc)
				{
					fSumM3Sk += m_vecDCLSTrees[i].getM3sk();
					fSumM3Fub += m_vecDCLSTrees[i].getM3fub();
					fSumM3Ub += m_vecDCLSTrees[i].getM3ub();
					// Adaption for "Intr�ngsv�rdering":
					// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
					// OBS! This only allies to "Intr�ng"; 080307 p�d
					nNumOfTreesInDCLS = m_vecDCLSTrees[i].getNumOf() + m_vecDCLSTrees[i].getNumOfRandTrees();
					fSumHgtPerSpc += (m_vecDCLSTrees[i].getHgt()*nNumOfTreesInDCLS)/10.0;	// (dm) => (m)

					nNumOfPerSpc += nNumOfTreesInDCLS;
				}	// if (m_vecDCLSTrees[i].getSpcID() == nSpc)
				fTotSumM3Sk += m_vecDCLSTrees[i].getM3sk();

			} // for (UINT i = 0;i < m_vecDCLSTrees.size();i++)

			// Calaculate avg. height for specie; 070525 p�d
			if (nNumOfPerSpc > 0)
			{
				if (fSumM3Sk > 0.0) fAvgM3Sk = fSumM3Sk/nNumOfPerSpc;
				if (fSumM3Fub > 0.0) fAvgM3Fub = fSumM3Fub/nNumOfPerSpc;
				if (fSumM3Ub > 0.0) fAvgM3Ub = fSumM3Ub/nNumOfPerSpc;
				if (fSumHgtPerSpc > 0.0) fAvgHgt = fSumHgtPerSpc/nNumOfPerSpc;
				fSpcPerc = ((double)nNumOfPerSpc/(double)m_vecDCLSTrees.size())*100.0;
			}
			// Add information to m_vecTraktData; 070525 p�d
			m_vecTraktData[ii].setAvgM3SK(fAvgM3Sk);
			m_vecTraktData[ii].setAvgM3FUB(fAvgM3Fub);
			m_vecTraktData[ii].setAvgM3UB(fAvgM3Ub);
			m_vecTraktData[ii].setAvgHgt(fAvgHgt);

			if (fTotSumM3Sk > 0.0 && fSumM3Sk > 0.0)
				m_vecTraktData[ii].setPercent((fSumM3Sk/fTotSumM3Sk)*100.0);
			m_vecTraktData[ii].setM3SK(fSumM3Sk);
			m_vecTraktData[ii].setM3FUB(fSumM3Fub);
			m_vecTraktData[ii].setM3UB(fSumM3Ub);
		}	// for (UINT ii = 0;ii < m_nNumOfSampleSpecies;ii++)
	}	// if (m_nNumOfSampleTrees > 0 && m_nNumOfSampleSpecies > 0)
}

//--------------------------------------------------------------
// Determin which typeof height calculation method
// id to be used; 070417 p�d
BOOL UCDoCalculation::useForHeight(int id)
{
	// Find out which typeof height calculation
	// method's selected and go to that method; 070417 p�d
	CString S;
	switch(id)
	{
		case ID_H25 :
		{
			CHeightH25 *pH25 = new CHeightH25(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pH25 != NULL)
			{
				pH25->setTreeData(m_vecTreeData);
				pH25->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pH25;
				break;
			}	// if (pH25 != NULL)
		}
		case ID_REGRESSION :
		{
			CHeightRegression *pRegression = new CHeightRegression(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pRegression != NULL)
			{
				pRegression->setTreeData(m_vecTreeData);
				pRegression->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pRegression;
				break;
			}	// if (pRegression != NULL)
		}
		case ID_SODERBERGS :
		{
			CHeightSoderbergs *pSoderbergs = new CHeightSoderbergs(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pSoderbergs != NULL)
			{
				pSoderbergs->setTreeData(m_vecTreeData);
				pSoderbergs->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pSoderbergs;
				break;
			}	// if (pSoderbergs != NULL)
		}
	};	// switch(m_classFuncList.getID)
	return TRUE;
}

//--------------------------------------------------------------
// Determin which typeof volume calculation method
// id to be used; 070417 p�d
BOOL UCDoCalculation::useForVolume(int id)
{
	// Find out which typeof volume calculation
	// method's selected and go to that method; 070417 p�d

	switch(id)
	{
		case ID_BRANDELS :
		{
			CVolumeBrandel *pVolumeBrandel = new CVolumeBrandel(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pVolumeBrandel != NULL)
			{			
				pVolumeBrandel->setTreeData(m_vecTreeData);
				pVolumeBrandel->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pVolumeBrandel;
				return TRUE;
			}	// if (pVolumeBrandel != NULL)
		}
		case ID_NASLUNDS :
		{
			CVolumeNaslund *pVolumeNaslund = new CVolumeNaslund(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pVolumeNaslund != NULL)
			{
				pVolumeNaslund->setTreeData(m_vecTreeData);
				pVolumeNaslund->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pVolumeNaslund;
				return TRUE;
			}	// if (pVolumeNaslund != NULL)
		}
		case ID_HAGBERG_MATERN :
		{
			CVolumeHagbergMarten *pVolumeHagbergMarten = new CVolumeHagbergMarten(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pVolumeHagbergMarten != NULL)
			{
				pVolumeHagbergMarten->setTreeData(m_vecTreeData);
				pVolumeHagbergMarten->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pVolumeHagbergMarten;
				return TRUE;
			}	// if (pVolumeNaslund != NULL)
		}
		case ID_CYLINDER :
		{
			CVolumeCylinder *pVolumeCylinder = new CVolumeCylinder(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pVolumeCylinder != NULL)
			{
				pVolumeCylinder->setTreeData(m_vecTreeData);
				pVolumeCylinder->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pVolumeCylinder;
				return TRUE;
			}	// if (pVolumeNaslund != NULL)
		}

		// Czech volume methods, added 2009-06-04 P�D
		case ID_CZ_VOLUME :
		{
			CVolumeCZ *pVolumeCZ = new CVolumeCZ(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pVolumeCZ != NULL)
			{
				pVolumeCZ->setTreeData(m_vecTreeData);
				pVolumeCZ->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pVolumeCZ;
				return TRUE;
			}	// if (pVolumeNaslund != NULL)
		}
	};	// switch(m_classFuncList.getID)
	
	return TRUE;
}


//--------------------------------------------------------------
// Determin which typeof volume calculation method
// id to be used; 070417 p�d
BOOL UCDoCalculation::useForVolume_ub(int id)
{
	// Find out which typeof volume calculation
	// method's selected and go to that method; 070417 p�d

	switch(id)
	{
		case ID_BRANDELS_UB :
		{
			CVolumeBrandel_ub *pVolumeBrandel_ub = new CVolumeBrandel_ub(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pVolumeBrandel_ub != NULL)
			{			
				CString S;
				pVolumeBrandel_ub->setTreeData(m_vecTreeData);
				pVolumeBrandel_ub->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
/*
				S.Format("useForVolume_ub Brandels\nm_vecTreeData.size %d\nm_vecTraktData.size %d\nm_vecSampleTrees.size %d\nm_vecDCLSTrees.size %d\nm_vecSpecies.size %d",
					m_vecTreeData.size(),m_vecTraktData.size(),m_vecSampleTrees.size(),m_vecDCLSTrees.size(),m_vecSpecies.size());
				AfxMessageBox(S);
*/
				delete pVolumeBrandel_ub;
				return TRUE;
			}	// if (pVolumeBrandel != NULL)
		}
		case ID_NASLUNDS_UB :
		{
			CVolumeNaslund_ub *pVolumeNaslund_ub = new CVolumeNaslund_ub(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pVolumeNaslund_ub != NULL)
			{
				pVolumeNaslund_ub->setTreeData(m_vecTreeData);
				pVolumeNaslund_ub->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pVolumeNaslund_ub;
				return TRUE;
			}	// if (pVolumeNaslund != NULL)
		}
		case ID_HAGBERG_MATERN_UB :
		{
			CVolumeHagbergMarten *pVolumeHagbergMarten = new CVolumeHagbergMarten(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pVolumeHagbergMarten != NULL)
			{
				pVolumeHagbergMarten->setTreeData(m_vecTreeData);
				pVolumeHagbergMarten->calculate_ub(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pVolumeHagbergMarten;
				return TRUE;
			}	// if (pVolumeNaslund != NULL)
		}
		case ID_OMF_FACTOR_UB :
		{
			CVolume_m3sk_m3ub *pVolume_m3sk_m3ub = new CVolume_m3sk_m3ub(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pVolume_m3sk_m3ub != NULL)
			{
				pVolume_m3sk_m3ub->setTreeData(m_vecTreeData);
				pVolume_m3sk_m3ub->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pVolume_m3sk_m3ub;
				return TRUE;
			}	// if (pVolumeNaslund != NULL)
		}

	};	// switch(m_classFuncList.getID)
	
	return TRUE;
}

//--------------------------------------------------------------
// Determin which typeof bark calculation method
// id to be used; 070417 p�d
BOOL UCDoCalculation::useForBark(int id)
{
	// Find out which typeof bark calculation
	// method's selected and go to that method; 070417 p�d

	switch(id)
	{
		case ID_BARK_JO :	// Jonsson och Ostlin
		{
			CBarkJO *pBarkJO = new CBarkJO(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pBarkJO != NULL)
			{
				pBarkJO->setTreeData(m_vecTreeData);
				pBarkJO->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pBarkJO;
				return TRUE;
			}	// if (pBarkJO != NULL)
		}
		case ID_BARK_SF :	// Skogforsk (Sk�rdare)
		{
			CBarkSF *pBarkSF = new CBarkSF(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pBarkSF != NULL)
			{
				pBarkSF->setTreeData(m_vecTreeData);
				pBarkSF->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pBarkSF;
				return TRUE;
			}	// if (pBarkSF != NULL)
		}
		case ID_BARK_SO :	// S�derbergs
		{
			CBarkSoderbergs *pBarkSoderbergs = new CBarkSoderbergs(m_nVecSpecieIndex,m_recTrakt,m_recMiscData,m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
			if (pBarkSoderbergs != NULL)
			{
				pBarkSoderbergs->setTreeData(m_vecTreeData);
				pBarkSoderbergs->calculate(m_vecTraktData,m_vecSampleTrees,m_vecDCLSTrees,m_vecSpecies);
				delete pBarkSoderbergs;
				return TRUE;
			}	// if (pBarkSoderbergs != NULL)
		}
	};	// switch(m_classFuncList.getID)

	return TRUE;
}

// PROTECTED

// PUBLIC

//-----------------------------------------------------------
// Do the calculation, depending on information
// in the m_vecSpecies; 070417 p�d
BOOL UCDoCalculation::doCalculation(vecTransactionTraktData& vec1,
																		vecTransactionSampleTree& vec2,
																		vecTransactionDCLSTree& vec4,	// Added 070820 p�d
																		vecTransactionTraktSetSpc& vec3)
{
	BOOL bReturn = FALSE;
	CString S;

	if (m_vecSpecies.size() > 0)
	{
		// After we done calculations, do some sum. of data into m_vecTraktData
		// Like DA,DG,DGV,HGV,GY etc; 070524 p�d
		setProTraktData();

		for (m_nVecSpecieIndex = 0;m_nVecSpecieIndex < m_vecSpecies.size();m_nVecSpecieIndex++)
		{
			CTransaction_trakt_set_spc spc = m_vecSpecies[m_nVecSpecieIndex];

			// Height calculations
			useForHeight(spc.getHgtFuncID());

			// Bark calculations
			useForBark(spc.getBarkFuncID());

			// Volume calculations
			useForVolume(spc.getVolFuncID());

			// Volume calculations under bark
			useForVolume_ub(spc.getVolUBFuncID());

		}
		setPostTraktData();

		vec1 = m_vecTraktData;
		vec2 = m_vecSampleTrees;
		vec4 = m_vecDCLSTrees;
		vec3 = m_vecSpecies;

		bReturn = TRUE;
	}
	return bReturn;
}
