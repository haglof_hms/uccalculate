// CEnterValueDialog.cpp : implementation file
//

#include "stdafx.h"
#include "UCCalculate.h"
#include "EnterValueDialog.h"


// CNumericEdit

IMPLEMENT_DYNAMIC(CNumericEdit, CEdit)

///
/// <summary></summary>
///
CNumericEdit::CNumericEdit()
{
	// determine the decimal delimiter buffer size
	const int nBuffLen = ::GetLocaleInfo( LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, NULL, 0 );
	_ASSERT( nBuffLen > 0 );

	// get the decimal number delimiter
	const int nResult = ::GetLocaleInfo( LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, 
		m_strDelim.GetBuffer(nBuffLen), nBuffLen );
	_ASSERT(nResult != 0);
	m_strDelim.ReleaseBuffer();
}

///
/// <summary></summary>
///
CNumericEdit::~CNumericEdit()
{
}


BEGIN_MESSAGE_MAP(CNumericEdit, CEdit)	
	ON_WM_CHAR()	
END_MESSAGE_MAP()



// CNumericEdit message handlers

///
/// <summary></summary>
///
void CNumericEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	if( nChar == m_strDelim )
	{
		CString strText;
		GetWindowText( strText );

		if( strText.IsEmpty() )
		{
			SetWindowText( (_T("0") + m_strDelim) );
			SetSel( 2, 2, FALSE );
			return;
		}
		else
		{
			// if the decimal point already entered, not allow enter more points
			if( strText.Find( m_strDelim ) >= 0 )
				return;
		}
	}

	// 8 - back space
	// 46 - .
	// 48 - 1
	// 57 - 0
	if( (nChar == m_strDelim) || (nChar >= '0' && nChar <= '9') || (nChar == 8) )
	{
		CEdit::OnChar(nChar, nRepCnt, nFlags);
	}
}

// CEnterValueDialog dialog

IMPLEMENT_DYNAMIC(CEnterValueDialog, CDialog)

BEGIN_MESSAGE_MAP(CEnterValueDialog, CDialog)
	ON_BN_CLICKED(IDOK, &CEnterValueDialog::OnBnClickedOk)
END_MESSAGE_MAP()

CEnterValueDialog::CEnterValueDialog(LPCTSTR spc_name,CWnd* pParent /*=NULL*/)
	: CDialog(CEnterValueDialog::IDD, pParent)
{
	m_fValue = 0.0;
	m_sSpcName = spc_name;

}

CEnterValueDialog::~CEnterValueDialog()
{
}

void CEnterValueDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_EDIT1, m_wndEdit);
	DDX_Control(pDX, IDC_LABEL1, m_wndLabel);
	//}}AFX_DATA_MAP
}

INT_PTR CEnterValueDialog::DoModal()
{
	if( DisplayMsg() )
	{
		return CDialog::DoModal();
	}
	else
	{
		return IDCANCEL;
	}
}

BOOL CEnterValueDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString sResStr,sMsg;
	sResStr.LoadString(IDS_STRING100);
	sMsg.Format(_T("%s : %s"),sResStr,m_sSpcName);
	SetWindowText(sMsg);
	
	sResStr.LoadString(IDS_STRING101);
	sMsg.Format(_T("%s :"),sResStr);
	m_wndLabel.SetWindowText(sMsg);

	return TRUE;
}

// CEnterValueDialog message handlers

void CEnterValueDialog::OnBnClickedOk()
{
	CString sValue;
	m_wndEdit.GetWindowText(sValue);
	m_fValue = _tstof(sValue);

	OnOK();
}

double CEnterValueDialog::getValue(void)
{
	return m_fValue;
}

