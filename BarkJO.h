/* 2007-04-13 Per-�ke Danielsson */
#if !defined(AFX_BARKJO_H)
#define AFX_BARKJO_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

////////////////////////////////////////////////////////////////////////////////////////
// CBarkJO (Jonsson och Ostlin)

class CBarkJO : public CCalculationBaseClass
{
//private:
protected:
	double getBarkPine(double dbh,int idx);
	double getBarkSpruce(double dbh,int idx);
	double getBarkBirch(double dbh,int idx);
public:
	CBarkJO(void);
	CBarkJO(int spc_index,CTransaction_trakt rec1,
												CTransaction_trakt_misc_data rec2,
												vecTransactionTraktData &tdata_list,
												vecTransactionSampleTree &sample_tree_list,
												vecTransactionDCLSTree &dcls_tree_list,
												vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);

};


#endif