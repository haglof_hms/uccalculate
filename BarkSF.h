/* 2007-04-13 Per-�ke Danielsson */
#if !defined(AFX_BARKSF_H)
#define AFX_BARKSF_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

//#include "pad_calculation_classes.h"

////////////////////////////////////////////////////////////////////////////////////////
// CBarkSF (According to Skogforsk)

class CBarkSF : public CCalculationBaseClass
{
//private:
protected:
	double getBarkPine(double dbh,double hgt,int latitude);
	double getBarkSpruce(double dbh);
public:
	CBarkSF(void);
	CBarkSF(int spc_index,CTransaction_trakt rec1,
												CTransaction_trakt_misc_data rec2,
												vecTransactionTraktData &tdata_list,
												vecTransactionSampleTree &sample_tree_list,
												vecTransactionDCLSTree &dcls_tree_list,
												vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);

};


#endif