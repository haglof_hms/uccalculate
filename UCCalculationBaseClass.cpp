
#include "StdAfx.h"
#include "UCCalculationBaseClass.h"

// PUBLIC
CCalculationBaseClass::CCalculationBaseClass(void)
{
	m_nNumOfSampleTrees = 0;
	m_nSpcIndex = -1;
	m_vecSampleTrees.clear();
	m_vecTraktData.clear();
	m_vecSetSpc.clear();
}

CCalculationBaseClass::CCalculationBaseClass(int spc_index,
																						 CTransaction_trakt rec1,
																						 CTransaction_trakt_misc_data rec2,
																						 vecTransactionTraktData &vec1,
																						 vecTransactionSampleTree &vec2,
																						 vecTransactionDCLSTree &vec4,
																						 vecTransactionTraktSetSpc &vec3)
{
	m_recTrakt = rec1;
	m_recMiscData = rec2;
	m_vecTraktData = vec1;
	m_vecSampleTrees = vec2;
	m_vecDCLSTrees = vec4;
	m_vecSetSpc = vec3;
	m_nSpcIndex = spc_index;
	m_nNumOfSampleTrees = m_vecSampleTrees.size();
	m_nNumOfDCLSTrees = m_vecDCLSTrees.size();
	m_nNumOfSampleSpecies = (UINT)m_vecSetSpc.size();
	
	m_nNumOfDCLSSpecies = vec3.size();

}

// PROTECTED
//================================================================
//	Method to calculate the GreeCrown percentage from testtrees
//	to be used in the rest of the trees. If there's no testtrees
//	and/or no greencrowns spec. ask the user to enter a percantage
//	for the greencrown; 070416 p�d
//================================================================
double CCalculationBaseClass::getGreenCrownPerc(void)
{
	int nNumOfGCrownTrees = 0;
	double	fSumGCrownPerc = 0.0,fGCPerc = 0.0;
	if (m_nNumOfSampleTrees > 0 && m_nNumOfSampleSpecies > 0)
	{
		for (long i = 0;i < m_nNumOfSampleTrees;i++)
		{
//			if (m_vecSampleTrees[i].getTreeType() < 2 && m_vecSampleTrees[i].getSpcID() == m_vecSetSpc[m_nSpcIndex].getSpcID())
			// Changed 101026 p�d
			// Don't look at Type of sampletree; 101026 p�d
//			if (m_vecSampleTrees[i].getTreeType() < 2 && m_vecSampleTrees[i].getSpcID() == m_vecSetSpc[m_nSpcIndex].getSpcID())
			if (m_vecSampleTrees[i].getSpcID() == m_vecSetSpc[m_nSpcIndex].getSpcID())
			{
				if (m_vecSampleTrees[i].getHgt() > 0.0 && m_vecSampleTrees[i].getGCrownPerc() > 0.0)
				{
					fSumGCrownPerc += m_vecSampleTrees[i].getGCrownPerc();
					nNumOfGCrownTrees++;
				} // if (data[i].getHgt() > 0.0 && data[i].getGCrown() > 0.0)
			}	// if (data[i].getIsASampleTree() == 1)
		} // for (long i = 0;i < numof;i++)
	}	// if (m_nNumOfSampleTrees > 0 && m_nNumOfSampleSpecies > 0)

	//--------------------------------------------------------------
	//	Calculate the avg. GCrown percenatage; 030410 p�d
	//--------------------------------------------------------------
	if (nNumOfGCrownTrees > 0 && fSumGCrownPerc > 0.0)
	{
		fGCPerc = fSumGCrownPerc/nNumOfGCrownTrees;
	}
	else	// #4556: H�mta ut standard gr�nkroneandel
	{
		for(int nSet=0; nSet<m_vecSetSpc.size(); nSet++)
		{
			if(m_vecSetSpc[nSet].getSpcID() == m_nSpcIndex)
			{
				fGCPerc = m_vecSetSpc[nSet].getDefaultGreenCrown();
				break;
			}
		}
	}

	return (fGCPerc);
}


double CCalculationBaseClass::getGreenCrownPercForSpc(int spc_id)
{
	int nNumOfGCrownTrees = 0;
	double	fSumGCrownPerc = 0.0,fGCPerc = 0.0;
	if (m_nNumOfSampleTrees > 0)
	{
		for (long i = 0;i < m_nNumOfSampleTrees;i++)
		{
//			if (m_vecSampleTrees[i].getTreeType() < 2 && m_vecSampleTrees[i].getSpcID() == spc_id)
			// Changed 101026 p�d
			// Don't look at Type of sampletree; 101026 p�d
			if (m_vecSampleTrees[i].getSpcID() == spc_id)
			{
				if (m_vecSampleTrees[i].getHgt() > 0.0 && m_vecSampleTrees[i].getGCrownPerc() > 0.0)
				{
					fSumGCrownPerc += m_vecSampleTrees[i].getGCrownPerc();
					nNumOfGCrownTrees++;
				} // if (data[i].getHgt() > 0.0 && data[i].getGCrown() > 0.0)
			}	// if (data[i].getIsASampleTree() == 1)
		} // for (long i = 0;i < numof;i++)
	}	// if (m_nNumOfSampleTrees > 0 && m_nNumOfSampleSpecies > 0)

	//--------------------------------------------------------------
	//	Calculate the avg. GCrown percenatage; 030410 p�d
	//--------------------------------------------------------------
	if (nNumOfGCrownTrees > 0 && fSumGCrownPerc > 0.0)
	{
		return fSumGCrownPerc/nNumOfGCrownTrees;
	}	// if (nNumOfGCrownTrees > 0 && fSumGCrownPerc > 0.0)


	// #4556: H�mta ut standardv�rdet f�r gr�nkronegr�ns
	for(int nSet=0; nSet<m_vecSetSpc.size(); nSet++)
	{
		if(m_vecSetSpc[nSet].getSpcID() == spc_id)
		{
			fGCPerc = m_vecSetSpc[nSet].getDefaultGreenCrown();
			break;
		}
	}

/*
	// If "Gr�nkroneprocenten = 0", try to collect "Gr�nkrona" from TraktData; 080625 p�d
	if (m_vecTraktData.size() > 0)
	{
		for (UINT ii = 0;ii < m_vecTraktData.size();ii++)
		{
			if (m_vecTraktData[ii].getSpecieID() == spc_id)
			{
				return m_vecTraktData[ii].getGreenCrown();
			}	// if (m_vecTraktData[ii].getSpecieID() == spc_id)
		}	// for (UINT ii = 0;ii < m_vecTraktData.size();ii++)
	}	// if (vecTraktData.size() > 0)
*/
	return fGCPerc;
}

//================================================================
// Get the largest diamterclass in m_vecDCLSTrees; 070820 p�d
//----------------------------------------------------------------
// Get the largest diamter in m_vecSampleTrees, doesn't matter if it's
// a sampletree or not; 070420 p�d
//================================================================
double CCalculationBaseClass::getMaxDiameter(void)
{
	double fDiam = 0.0;
	double fDCLS = 0.0;
	// No traa data at all, for any specie; 070417 p�d
	if (m_vecDCLSTrees.size() == 0) return 0.0;

	for (UINT i = 0;i < m_vecDCLSTrees.size();i++)
	{
		fDCLS = getClassMid(m_vecDCLSTrees[i]);
		fDiam = max(fDCLS,fDiam);
	}
	
	return fDiam;
}

double CCalculationBaseClass::getMinDiameter(void)
{
	CString S;
	double fDiam = 999999.0;
	double fDCLS = 0.0;
	// No traa data at all, for any species; 070417 p�d
	if (m_vecDCLSTrees.size() == 0) return 0.0;

	for (UINT i = 0;i < m_vecDCLSTrees.size();i++)
	{
		fDCLS = getClassMid(m_vecDCLSTrees[i]);
		fDiam = min(fDCLS,fDiam);
	}
	
	return fDiam;
}

int CCalculationBaseClass::getNumOfTreesForSpc(int spc_id)
{
	if (m_vecDCLSTrees.size() == 0) return 0;

	for (long i = 0;i < m_vecDCLSTrees.size();i++)
	{
		if (m_vecDCLSTrees[i].getSpcID() == spc_id)
			return m_vecDCLSTrees[i].getNumOf() + m_vecDCLSTrees[i].getNumOfRandTrees();	// Also add "Kanttr�d"; 090227 p�d
	}

	return 0.0;
}

double CCalculationBaseClass::getSpcSumSampleTreeHgt(int spc)
{
	double fSumHgt = 0.0;
	if (m_nNumOfSampleTrees == 0) return fSumHgt;
	for (long i = 0;i < m_nNumOfSampleTrees;i++)
	{
		if (m_vecSampleTrees[i].getSpcID() == spc && m_vecSampleTrees[i].getHgt() > 0.0)
		{
			fSumHgt += m_vecSampleTrees[i].getHgt();
		} // if (m_vecSampleTrees[i].getSpcID() == spc)
	} // for (long i = 0;i < m_nNumOfSampleTrees;i++)

	return fSumHgt;
}

/*
double CCalculationBaseClass::getSpcPercent(int spc)
{
	// No traa data at all, for any specie; 070417 p�d
	if (m_vecTreeData.size() == 0)
		return 0.0;

	for (UINT i = 0;i < m_vecTreeData.size();i++)
	{
		if (m_vecTreeData[i].getSpcID() == spc)
		{
			return (m_vecTreeData[i].getSpcPerc());
		}
	}
	return 0.0;
}
*/
double CCalculationBaseClass::getSpcPercent(int spc)
{
	// No traa data at all, for any specie; 070417 p�d
	if (m_vecTraktData.size() == 0)
		return 0.0;

	for (UINT i = 0;i < m_vecTraktData.size();i++)
	{
		if (m_vecTraktData[i].getSpecieID() == spc)
		{
			return (m_vecTraktData[i].getPercent()/100.0);
		}
	}
	return 0.0;
}

double CCalculationBaseClass::getSpcPercent_GY(int spc,BOOL only_tgl)
{
	CString S;
	double fSpcMix = 0.0;
	double fSumGY = getSumGY();
	int nNumOfTreesForSpc = getNumOfTreesForSpc(spc);
	// No traa data at all, for any specie; 070417 p�d
	if (m_vecTraktData.size() == 0)
		return 0.0;

	for (UINT i = 0;i < m_vecTraktData.size();i++)
	{
		// Calculate for only for Tall,Gran or Bj�rk; 090227 p�d
		if (only_tgl)
		{
			if (m_vecTraktData[i].getSpecieID() == spc)
			{
				if (nNumOfTreesForSpc > 0 && m_vecTraktData[i].getGY() > 0.0 && fSumGY > 0.0)
				{
					fSpcMix = ((m_vecTraktData[i].getGY())/(fSumGY));
				}	// if (nNumOfTreesForSpc > 0 && m_vecTraktData[i].getGY() > 0.0 && fSumGY > 0.0 && m_nNumOfDCLSTrees > 0)
			}
		}	// if (only_tgl)
		// Calculate for ALL species; 090227 p�d
		else	
		{
			if (m_vecTraktData[i].getSpecieID() == spc)
			{
				if (nNumOfTreesForSpc > 0 && m_vecTraktData[i].getGY() > 0.0 && fSumGY > 0.0)
				{
					fSpcMix = ((m_vecTraktData[i].getGY())/(fSumGY));
				}	// if (nNumOfTreesForSpc > 0 && m_vecTraktData[i].getGY() > 0.0 && fSumGY > 0.0 && m_nNumOfDCLSTrees > 0)
			}	// if (m_vecTraktData[i].getSpecieID() == spc)
		}
	}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
/*
	S.Format(_T("CCalculationBaseClass::getSpcPercent_GY spc %d\nfSpcMix %f"),spc,fSpcMix);
	AfxMessageBox(S);
*/
	return fSpcMix;
}

double CCalculationBaseClass::getSpcAvgHgt(int spc)
{
	// No traa data at all, for any specie; 070417 p�d
	if (m_vecTreeData.size() == 0)
		return 0.0;

	for (UINT i = 0;i < m_vecTreeData.size();i++)
	{
		if (m_vecTreeData[i].getSpcID() == spc)
		{
			return (m_vecTreeData[i].getAvgHgt());
		}
	}
	return 0.0;
}

double CCalculationBaseClass::getDiamQuata(double dbh)
{
	double fMaxDiam = getMaxDiameter();
	// Make sure that the Max diameter is the
	// max diameter; 070425 p�d
	if (dbh > fMaxDiam)
		return 1.0;
	else if (dbh > 0.0 && fMaxDiam > 0.0)
		return dbh/fMaxDiam;
	else
		return 0.0;
}

// Sum. baselarea; 090227 p�d
double CCalculationBaseClass::getSumGY(void)
{
	double fSumGY = 0.0;
	// No data at all, for any specie; 090227 p�d
	if (m_vecTraktData.size() == 0)
		return 0.0;
	
	for (UINT i = 0;i < m_vecTraktData.size();i++)
	{
		fSumGY += m_vecTraktData[i].getGY();
	}

	return fSumGY;
}

void CCalculationBaseClass::setTreeData(vecTreeData &v)
{
	m_vecTreeData = v;
}

long CCalculationBaseClass::getNumOfSampleTrees(int spc_id)
{
	int nNumOf = -1;
	// No sampletrees at all, for any specie; 070417 p�d
	if (m_vecTreeData.size() == 0)
		return 0;

	for (UINT i = 0;i < m_vecTreeData.size();i++)
	{
		if (m_vecTreeData[i].getSpcID() == spc_id) //m_vecSetSpc[m_nSpcIndex].getSpcID())
		{
			nNumOf = (m_vecTreeData[i].getNumOfSampleTrees());
			return nNumOf;
		}
	}
	
	return 0;
}

// Check if there's sampletrees ro a specie; 070417 p�d
BOOL CCalculationBaseClass::getHasSpecieSampleTrees(void)
{
	// No sampletrees at all, for any specie; 070417 p�d
	if (m_vecTreeData.size() == 0)
		return FALSE;

	for (UINT i = 0;i < m_vecTreeData.size();i++)
	{
		if (m_vecTreeData[i].getSpcID() == m_vecSetSpc[m_nSpcIndex].getSpcID())
			return (m_vecTreeData[i].getNumOfSampleTrees() > 0);
	}
	
	return FALSE;
}

//------------------------------------------------------------------------
// This method's used to retrive the active specie set by m_nSpcIndex; 070418 p�d
int CCalculationBaseClass::getSpecieID(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getSpcID();
}

//------------------------------------------------------------------------
// Retrive the "omf�ringstal", from m3sk to m3fub for specie; 070524 p�d
double CCalculationBaseClass::getM3SkToM3Fub(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getM3SkToM3Fub();
}

//------------------------------------------------------------------------
// This method's used to retrive the ID of the calculationmethod
// for bark-reduction to use (ex. Jonsson-Ostlin); 070417 p�d
int CCalculationBaseClass::getBarkID(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getBarkFuncID();
}

// This method's used to retrive the specie id of the calculationmethod
// for bark-reduction, to use; 070417 p�d
int CCalculationBaseClass::getBarkSpcID(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getBarkFuncSpcID();
}

// This method's used to retrive the index within the calculationmethod
// for bark-reduction, to use; 070417 p�d
int CCalculationBaseClass::getBarkIndex(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getBarkFuncIndex();
}

//------------------------------------------------------------------------
// This method's used to retrive the ID of the calculationmethod
// for volume to use (ex. Brandels); 070417 p�d
int CCalculationBaseClass::getVolumeID(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getVolFuncID();
}

// This method's used to retrive the specie id of the calculationmethod
// for volume, to use; 070417 p�d
int CCalculationBaseClass::getVolumeSpcID(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getVolFuncSpcID();
}

// This method's used to retrive the index within the calculationmethod
// for volume, to use; 070417 p�d
int CCalculationBaseClass::getVolumeIndex(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getVolFuncIndex();
}


//------------------------------------------------------------------------
// This method's used to retrive the ID of the calculationmethod
// for volume to uses for m3ub (ex. Brandels); 071022 p�d
int CCalculationBaseClass::getVolumeID_ub(void)
{
	// No species at all; 071022 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getVolUBFuncID();
}

// This method's used to retrive the specie id of the calculationmethod
// for volume, to use for m3ub; 071022 p�d
int CCalculationBaseClass::getVolumeSpcID_ub(void)
{
	// No species at all; 071022 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getVolUBFuncSpcID();
}

// This method's used to retrive the index within the calculationmethod
// for volume, to use for m3ub; 071022 p�d
int CCalculationBaseClass::getVolumeIndex_ub(void)
{
	// No species at all; 071022 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getVolUBFuncIndex();
}

//------------------------------------------------------------------------
// This method's used to retrive the ID of the calculationmethod
// for height to use (ex. H25); 070417 p�d
int CCalculationBaseClass::getHeightID(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getHgtFuncID();
}

// This method's used to retrive the specie id of the calculationmethod
// for height, to use; 070417 p�d
int CCalculationBaseClass::getHeightSpcID(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getHgtFuncSpcID();
}

// This method's used to retrive the index within the calculationmethod
// for height, to use; 070417 p�d
int CCalculationBaseClass::getHeightIndex(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getHgtFuncIndex();
}

int CCalculationBaseClass::getNearCoast(void)
{
	CStringArray args;
	CString sExtraInfo = m_vecSetSpc[m_nSpcIndex].getExtraInfo();
	if (!sExtraInfo.IsEmpty())
	{
		if (SplitString(sExtraInfo,_T(";"),args) > 0)
		{
			return _tstoi(args.GetAt(0));
		}	// if (SplitString(sExtraInfo,",",args) > 0)
	}	// if (!sExtraInfo.IsEmpty())
	else
	{
		return getTrakt().getTraktNearCoast();
	}

	return 0;
}

int CCalculationBaseClass::getSouthEast(void)
{
	CStringArray args;
	CString sExtraInfo = m_vecSetSpc[m_nSpcIndex].getExtraInfo();
	if (!sExtraInfo.IsEmpty())
	{
		if (SplitString(sExtraInfo,_T(";"),args) > 0)
		{
			return _tstoi(args.GetAt(1));
		}	// if (SplitString(sExtraInfo,",",args) > 0)
	}	// if (!sExtraInfo.IsEmpty())
	else
	{
		return getTrakt().getTraktSoutheast();
	}
	return 0;
}

int CCalculationBaseClass::getRegion5(void)
{
	CStringArray args;
	CString sExtraInfo = m_vecSetSpc[m_nSpcIndex].getExtraInfo();
	if (!sExtraInfo.IsEmpty())
	{
		if (SplitString(sExtraInfo,_T(";"),args) > 0)
		{
			return _tstoi(args.GetAt(2));
		}	// if (SplitString(sExtraInfo,",",args) > 0)
	}	// if (!sExtraInfo.IsEmpty())
	else
	{
		return getTrakt().getTraktRegion5();
	}
	return 0;
}

int CCalculationBaseClass::getPartOfPlot(void)
{
	CStringArray args;
	CString sExtraInfo = m_vecSetSpc[m_nSpcIndex].getExtraInfo();
	if (!sExtraInfo.IsEmpty())
	{
		if (SplitString(sExtraInfo,_T(";"),args) > 0)
		{
			return _tstoi(args.GetAt(3));
		}	// if (SplitString(sExtraInfo,",",args) > 0)
	}	// if (!sExtraInfo.IsEmpty())
	else
	{
		return getTrakt().getTraktPartOfPlot();
	}
	return 0;
}

CTransaction_trakt& CCalculationBaseClass::getTrakt(void)
{
	return m_recTrakt;
}

CTransaction_trakt_data *CCalculationBaseClass::getTraktData(int spc)
{
	CTransaction_trakt_data *data;
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			data = &m_vecTraktData[i];
			if (data->getSpecieID() == spc)
				return data;
		}
	}
	return NULL;
}

void CCalculationBaseClass::setTraktData_h25(int spc,double h25)
{
	CTransaction_trakt_data data;
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			data = m_vecTraktData[i];
			if (data.getSpecieID() == spc && data.getH25() != h25)
			{
				m_vecTraktData[i].setH25(h25);
				return;
			}	// if (data.getSpecieID() == spc)
		}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
	}	// if (m_vecTraktData.size() > 0)
}

void CCalculationBaseClass::setTraktData_hgv(int spc,double hgv)
{
	CTransaction_trakt_data data;
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			data = m_vecTraktData[i];
			if (data.getSpecieID() == spc)
			{
				m_vecTraktData[i].setHGV(hgv);
				return;
			}	// if (data.getSpecieID() == spc)
		}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
	}	// if (m_vecTraktData.size() > 0)
}

void CCalculationBaseClass::setTraktData_gcrown(int spc,double gcrown)
{
	CTransaction_trakt_data data;
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			data = m_vecTraktData[i];
			if (data.getSpecieID() == spc)
			{
				m_vecTraktData[i].setGreenCrown(gcrown);
				return;
			}	// if (data.getSpecieID() == spc)
		}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
	}	// if (m_vecTraktData.size() > 0)
}

//----------------------------------------------------------------------------------
int CCalculationBaseClass::getSIfromH100(LPCTSTR value)
{
	int nBuffLen = 0;
	int nCalcFactor = 1;
	CString sBuffer;
	sBuffer = value;

	nBuffLen = sBuffer.GetLength();
	// Check that the buffer is 4 characters in length (e.g. T200,G120 etc); 070418 p�d
	// I.e. H100 is set in (dm); 071101 p�d
	if (nBuffLen == 3 || nBuffLen == 4)
	{
		if (nBuffLen == 3)
			nCalcFactor = 10;	// From (m) to (dm)
		// Check that pos 2,3 and 4 are a numeric value e.g. (G200 = 200); 070418 p�d
		if (isNumeric(sBuffer,1,sBuffer.GetLength()-1))
		{
			sBuffer.Delete(0,1);
			return _tstoi(sBuffer)*nCalcFactor;
		}	// if (isNumeric(sBuffer,1,sBuffer.GetLength()-1))
	}	// if (nBufflen == 3 || nBuffLen == 4)
	return 0;

}

BOOL CCalculationBaseClass::isSpecieInTrees(int spc_id)
{
	if (m_vecDCLSTrees.size() > 0)
	{
		for (int i = 0;i < m_vecDCLSTrees.size();i++)
		{
			if (m_vecDCLSTrees[i].getSpcID() == spc_id)
				return TRUE;
		}	// for (UINT i = 0;i < m_vecSampleTrees.size();i++)
	}	// if (m_vecSampleTrees.size() > 0)
	return FALSE;
}

CString CCalculationBaseClass::getSpecieName(int spc_id)				
{
	if (m_vecDCLSTrees.size() > 0)
	{
		for (long i = 0;i < m_vecDCLSTrees.size();i++)
		{
			if (m_vecDCLSTrees[i].getSpcID() == spc_id)
				return m_vecDCLSTrees[i].getSpcName();
		}	// for (UINT i = 0;i < m_vecSampleTrees.size();i++)
	}	// if (m_vecSampleTrees.size() > 0)
	return _T("");
}


CTransaction_dcls_tree* CCalculationBaseClass::getTree(int spc_id)
{
	if (m_vecDCLSTrees.size() > 0)
	{
		for (long i = 0;i < m_vecDCLSTrees.size();i++)
		{
			if (m_vecDCLSTrees[i].getSpcID() == spc_id)
				return &m_vecDCLSTrees[i];
		}	// for (UINT i = 0;i < m_vecSampleTrees.size();i++)
	}	// if (m_vecSampleTrees.size() > 0)
	return NULL;
}

// Helpmethods
BOOL CCalculationBaseClass::isNumeric(LPCTSTR value,int start,int end)
{
	TCHAR szBuffer[64];
	_tcscpy_s(szBuffer,64,value);
	if (end == -1)
		end = (int)_tcslen(szBuffer);

	for (int i = start;i < end;i++)
	{
		if (szBuffer[i] == '0' || 
				szBuffer[i] == '1' || 
				szBuffer[i] == '2' || 
				szBuffer[i] == '3' || 
				szBuffer[i] == '4' || 
				szBuffer[i] == '5' || 
				szBuffer[i] == '6' || 
				szBuffer[i] == '7' || 
				szBuffer[i] == '8' || 
				szBuffer[i] == '9' || 
				szBuffer[i] == '.' || 
				szBuffer[i] == ',')
		{
			return TRUE;
		}
	}
	return FALSE;
}

// This method's used to retrive the index within the calculationmethod
// for volume, to use; 070417 p�d
double CCalculationBaseClass::getOmfValueForM3SkToM3Ub(void)
{
	// No species at all; 070417 p�d
	if (m_vecSetSpc.size() == 0)
		return 0;

	return m_vecSetSpc[m_nSpcIndex].getM3SkToM3Ub();
}
