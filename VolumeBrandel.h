/* 2007-04-13 Per-�ke Danielsson */
#if !defined(AFX_VOLUMEBRANDEL_H)
#define AFX_VOLUMEBRANDEL_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

////////////////////////////////////////////////////////////////////////////////////////
// CVolumeBrandel

class CVolumeBrandel : public CCalculationBaseClass
{
//private:

protected:
//================================================================
// Calculate volume
// Formula on page 26 (01)
//================================================================
double getCalcSimple(double hgt,double dbh,const double *const_val);
//================================================================
// Calculate volume using "Krongr�ns"
// Formula on page 27 (02)
//================================================================
double getCalcKG(double hgt,double dbh,double kg,const double *const_val);
//================================================================
// Calculate volume using "Barkthickness"
// Formula on page 27 (02)
//================================================================
double getCalcBRK(double hgt,double dbh,double brk,const double *const_val);
//================================================================
// Calculate volume using "Krongr�ns" and "Barkthickness".
// Formula on page 27 (04)
//================================================================
double getCalcKG_BRK(double hgt,double dbh,double kg,double brk,const double *const_val);
//================================================================
//  Brandels volume calculation ; 070412 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  hgt_sea = Height over sea
//  lng     = Longitude for tree
//  kg      = GreenCrown (Krongr�ns)
//================================================================
double getVolumeForPine(int n,double hgt,double dbh,int hgt_sea,int lng,double kg,double brk);
double getVolumeForSpruce(int n,double hgt,double dbh,int hgt_sea,int lng,double kg,double brk);
double getVolumeForBirch(int n,double hgt,double dbh,int hgt_sea,int lng,double kg,double brk);

//================================================================
//	Method to calculate the GreeCrown percentage from testtrees
//	to be used in the rest of the trees. If there's no testtrees
//	and/or no greencrowns spec. ask the user to enter a percantage
//	for the greencrown; 070412 p�d
//================================================================
double CVolumeBrandel::getGreenCrownPerc_ub(int spc);
public:
	CVolumeBrandel(void);
	CVolumeBrandel(int spc_index,CTransaction_trakt rec1,
															 CTransaction_trakt_misc_data rec2,
															 vecTransactionTraktData &tdata_list,
															 vecTransactionSampleTree &sample_tree_list,
															 vecTransactionDCLSTree &dcls_tree_list,
															 vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);
};

#endif