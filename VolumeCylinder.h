/* 2007-06-26 Per-�ke Danielsson */
#if !defined(AFX_VOLUMECYLINDER_H)
#define AFX_VOLUMECYLINDER_H


#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

////////////////////////////////////////////////////////////////////////////////////////
// CVolumeCylinder

class CVolumeCylinder : public CCalculationBaseClass
{
//private:

protected:
	double getCylinderVolume(double hgt,double dbh);

public:
	CVolumeCylinder(void);
	CVolumeCylinder(int spc_index,CTransaction_trakt rec1,
																CTransaction_trakt_misc_data rec2,
																vecTransactionTraktData &tdata_list,
																vecTransactionSampleTree &sample_tree_list,
																vecTransactionDCLSTree &dcls_tree_list,
																vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);
};


#endif