
#include "StdAfx.h"

#include "GrotCalculations.h"


CGrotCalc::CGrotCalc(void)
{
	m_recTrakt = CTransaction_trakt();
	m_vecTraktData.clear();
	m_vecSpcList.clear();
	m_vecDCLSTrees.clear();
	m_nSI_PineOrSpruce = 0;
}

CGrotCalc::CGrotCalc(CTransaction_trakt rec,vecTransactionTraktData &trakt_data,vecTransactionTraktSetSpc &spc_list,vecTransactionDCLSTree &dcls_tree_list)
{
	m_recTrakt = rec;
	m_vecTraktData = trakt_data;
	m_vecSpcList = spc_list;
	m_vecDCLSTrees = dcls_tree_list;
	m_nSI_PineOrSpruce = 0;
}

CGrotCalc::~CGrotCalc(void)
{
}

int CGrotCalc::getFunctionID(void)
{
	if (m_vecSpcList.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpcList.size();i++)
			if (m_nActiveSpc == m_vecSpcList[i].getSpcID()) 
				return m_vecSpcList[i].getGrotFuncID();
	}
	return 0;
}

double CGrotCalc::getPercent(void)
{
	if (m_vecSpcList.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpcList.size();i++)
			if (m_nActiveSpc == m_vecSpcList[i].getSpcID()) 
				return m_vecSpcList[i].getGrotPercent();
	}
	return 0;
}


double CGrotCalc::getKR_length(void)
{
	double fDBHpb = m_fDBH*10.0,fRetVal = 0.0;
	if (m_nSI_PineOrSpruce == 1) // Tall
		fRetVal = ((2.396*fDBHpb + 19.2*m_fHgt)/100.0);
	if (m_nSI_PineOrSpruce == 2) // Gran
		fRetVal = ((2.5694*fDBHpb + 37.53*m_fHgt)/100.0);

	return fRetVal;
}

int CGrotCalc::getConversH100Value(short id,short h100)
{
	// Handle Bok; 081124 p�d
	for (short i = 0;i < NUMOF_ROWS_IN_TABLE_CONVERS;i++)
	{
		if (id == 1)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][1];	// Values for Bok; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 1)

		// Handle Ek; 081124 p�d
		if (id == 2)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][2];	// Values for Ek; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 2)

		// Handle Bj�rk; 081124 p�d
		if (id == 3)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][3];	// Values for Ek; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 3)

		// Handle Contorta; 081124 p�d
		if (id == 4)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][4];	// Values for Ek; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 1)
	}	// for (short i = 0;i < NUMOF_ROWS_IN_TABLE_CONVERS;i++)

	return 0;
}

BOOL CGrotCalc::DoGrotCalc()
{
#ifdef CALCULATE_GROT
	BOOL bAllOK = TRUE;
	int nGrotFuncID = 0;
	double fGrot = 0.0;

	if (m_vecTraktData.size() > 0)
	{
		for (UINT j = 0;j < m_vecTraktData.size();j++)
		{
			m_mapSumGrotPerSpc[m_vecTraktData[j].getSpecieID()] = 0.0;
		}
	}

	//--------------------------------------------------------------------
	// Setup data for calculations; 100311 p�d
	//--------------------------------------------------------------------
	CString sStr(m_recTrakt.getTraktSIH100()),S;
	
	m_fBrg = m_recTrakt.getTraktLatitude()*1.0;	// Make float
	m_fHgtOverSea = m_recTrakt.getTraktHgtOverSea()/1000.0;	// (km)
	m_fNCoord = _tstof(m_recTrakt.getTraktXCoord());	// Nord-koordinat i Rikets n�t (RT90)

	if (m_fNCoord == 0.0 || m_fHgtOverSea == 0.0 || m_fBrg == 0.0 || m_vecDCLSTrees.size() == 0) 
	{
		if (m_vecTraktData.size() > 0)
		{
			for (UINT j = 0;j < m_vecTraktData.size();j++)
			{
				m_vecTraktData[j].setGrot(0.0);
			}
		}
		bAllOK = FALSE;
	}

	if (bAllOK)
	{
		for (UINT i = 0;i < m_vecDCLSTrees.size();i++)
		{
			CTransaction_dcls_tree recTree = m_vecDCLSTrees[i];
			fGrot = 0.0;
			if (recTree.getNumOfRandTrees() > 0)
				m_nNumOfTrees = recTree.getNumOf() + recTree.getNumOfRandTrees();
			else
				m_nNumOfTrees = recTree.getNumOf();
			if (m_nNumOfTrees <= 0) return FALSE;
			if (m_nNumOfTrees > 0)
			{
				m_nActiveSpc = recTree.getSpcID();
				nGrotFuncID = getFunctionID();
				m_fDBH = getClassMid(recTree)/10.0;	// (cm)
				m_fHgt = recTree.getHgt()/10.0;	// (m)
				m_fGrotPercent = getPercent()/100.0;
				// Check if SI value is set to something else
				// than T = Pine (Tall), G = Spruce (Gran).
				// If so, convert value to T or G; 100311 p�d
				if (sStr.Find('F') == 0)	// F = "Bok"
				{
					m_nH100 = getConversH100Value(1,_tstoi(sStr.Right(sStr.GetLength()-1)));
					m_nSI_PineOrSpruce = 2;
				}
				else if (sStr.Find('E') == 0)	// E = "Ek"
				{
					m_nH100 = getConversH100Value(2,_tstoi(sStr.Right(sStr.GetLength()-1)));
					m_nSI_PineOrSpruce = 2;
				}
				else if (sStr.Find('B') == 0)	// B = "Bj�rk"
				{
					m_nH100 = getConversH100Value(3,_tstoi(sStr.Right(sStr.GetLength()-1)));
					m_nSI_PineOrSpruce = 2;
				}
				else if (sStr.Find('C') == 0)	// C = "Contorta"
				{
					m_nH100 = getConversH100Value(4,_tstoi(sStr.Right(sStr.GetLength()-1)));
					m_nSI_PineOrSpruce = 1;
				}
				else if (m_nActiveSpc == 1)	// T = "Tall"
				{
					m_nH100 = _tstoi(sStr.Right(sStr.GetLength()-1));
					m_nSI_PineOrSpruce = 1;
				}
				else if (m_nActiveSpc == 2)	// G = "Gran"
				{
					m_nH100 = _tstoi(sStr.Right(sStr.GetLength()-1));
					m_nSI_PineOrSpruce = 2;
				}

				// Calculate "Kronl�ngd"; 100311 p�d
				m_fKR_length = getKR_length();

				//-------------------------------------------------------------------------------
				if (bAllOK)
				{
					// Setup calculation of GROT depending on function selected; 100312 p�d
					switch (nGrotFuncID)
					{
						case ID_GROT_PINE :	
							fGrot = (T_15() - T_19() + T_23())*m_fGrotPercent;	
						break;

						case ID_GROT_SPRUCE :
							// We need to check 'breddgrad' to determin the function to use; 100312 p�d
							if (m_fBrg <59.5)	// Syd
							{
								// Also check DBH; 100312 p�d
								if (m_fDBH < 25.0) 
								{
									fGrot = (G_LivinBranches() + G_DeadBranches())*m_fGrotPercent;
								}
								else if (m_fDBH >= 25.0 && m_fDBH < 35.0) 
								{
									fGrot = (G_13()-G_17()+G_21())*m_fGrotPercent;
								}
								else if (m_fDBH > 35.0) 
								{
									fGrot = (G_13()-G_17()+G_21())*m_fGrotPercent*1.2;
								}
							}
							else if (m_fBrg >=59.5 && m_fBrg < 62.2)	// Mellan
							{
								// Also check DBH; 100312 p�d
								if (m_fDBH < 25.0) fGrot = (G_LivinBranches() + G_DeadBranches())*m_fGrotPercent;
								else if (m_fDBH >= 25.0) fGrot = (G_13()-G_17()+G_21())*m_fGrotPercent;
							}
							else if (m_fBrg > 62.2)	// Norr
							{
								fGrot = (G_LivinBranches() + G_DeadBranches())*m_fGrotPercent;
							}
						break;

						case ID_GROT_BEACH :
								fGrot = (B_LivinBranches() + B_DeadBranches())*m_fGrotPercent;
						break;

						case ID_GROT_LEAF :
							if (m_fBrg <59.5)	// Syd
							{
								fGrot = (B_LivinBranches() + B_DeadBranches())*m_fGrotPercent;
							}
							else if (m_fBrg >=59.5 && m_fBrg < 62.2)	// Mellan
							{
								fGrot = (B_LivinBranches() + B_DeadBranches())*m_fGrotPercent*0.80;	
							}
							else if (m_fBrg > 62.2)	// Norr
							{
								fGrot = (B_LivinBranches() + B_DeadBranches())*m_fGrotPercent*0.80;
							}
						break;

					};
				}	// if (bAllOK)
				else
					fGrot = 0.0;
				//-------------------------------------------------------------------------------
				m_vecDCLSTrees[i].setGROT(fGrot*m_nNumOfTrees);
				m_mapSumGrotPerSpc[m_nActiveSpc] += fGrot*m_nNumOfTrees;
			}	// if (m_nNumOfTrees > 0)
		}
		}

#else
		for (UINT i = 0;i < m_vecDCLSTrees.size();i++)
		{
			m_vecDCLSTrees[i].setGROT(0.0);
			m_mapSumGrotPerSpc[m_nActiveSpc] += 0.0;
		}
#endif

		if (m_vecTraktData.size() > 0 )
		{
			for (UINT j = 0;j < m_vecTraktData.size();j++)
			{
				m_vecTraktData[j].setGrot(m_mapSumGrotPerSpc[m_vecTraktData[j].getSpecieID()]);
			}
		}
		
	return TRUE;

}

//-----------------------------------------
// MARKLUND
//-----------------------------------------
// Calc Pine 

double CGrotCalc::T_02(void)
{
	return 0.0;
}

double CGrotCalc::T_14(void)
{
	return 0.0;
}

double CGrotCalc::T_15(void)
{
	if (m_fDBH == 0.0 || m_fHgt == 0.0 || m_fKR_length == 0.0) return 0.0;
	double fValue = exp(11.4337*(m_fDBH/(m_fDBH+10.0))-1.4815*log(m_fHgt)+0.9825*log(m_fKR_length)-0.0235*m_fNCoord-0.9137);
	return fValue;
}

double CGrotCalc::T_18(void)
{
	return 0.0;
}

double CGrotCalc::T_19(void)
{
	if (m_fDBH == 0.0 || m_fHgt == 0.0 || m_fKR_length == 0.0) return 0.0;
	double fValue = exp(9.8471*(m_fDBH/(m_fDBH+7.0))+0.026*m_fHgt-1.6717*log(m_fHgt)+1.0419*log(m_fKR_length)-0.0123*m_fNCoord-2.6024);
	return fValue;
}

double CGrotCalc::T_22(void)
{
	return 0.0;
}

double CGrotCalc::T_23(void)
{
	if (m_fDBH == 0.0 || m_fHgt == 0.0) return 0.0;
	double fValue = exp(7.1889*(m_fDBH/(m_fDBH+10.0))-0.085*m_fHgt+1.3027*log(m_fHgt)-0.0702*m_fNCoord-1.0568*m_fHgtOverSea-0.9305);
	return fValue;
}

double CGrotCalc::T_28(void)
{
	return 0.0;
}

double CGrotCalc::T_29(void)
{
	return 0.0;
}

double CGrotCalc::T_31(void)
{
	return 0.0;
}

double CGrotCalc::T_32(void)
{
	return 0.0;
}
// Calc Spruce
double CGrotCalc::G_02(void)
{
	return 0.0;
}

double CGrotCalc::G_12(void)
{
	return 0.0;
}

double CGrotCalc::G_13(void)
{
	double fRetVal = 0.0;
	if (m_fDBH == 0.0 || m_fKR_length == 0.0 || m_fHgt == 0.0) return 0.0;

	if (m_nSI_PineOrSpruce == 1) // Tall
		fRetVal = exp(10.4621*(m_fDBH/(m_fDBH+13.0))-1.5211*log(m_fHgt)+1.0179*log(m_fKR_length)+0.0121*m_nH100-1.1209);
	else if (m_nSI_PineOrSpruce == 2) // Gran
		fRetVal = exp(10.4621*(m_fDBH/(m_fDBH+13.0))-1.5211*log(m_fHgt)+1.0179*log(m_fKR_length)+0.011*m_nH100-1.1209);

	return fRetVal;
}

double CGrotCalc::G_16(void)
{
	return 0.0;
}

double CGrotCalc::G_17(void)
{
	if (m_fDBH == 0.0 || m_fKR_length == 0.0 || m_fHgt == 0.0) return 0.0;
	return exp(8.4127*(m_fDBH/(m_fDBH+12.0))-1.5628*log(m_fHgt)+1.4032*log(m_fKR_length)-1.5732);
}

double CGrotCalc::G_20(void)
{
	return 0.0;
}

double CGrotCalc::G_21(void)
{
	if (m_fDBH == 0.0 || m_fKR_length == 0.0 || m_fHgt == 0.0) return 0.0;
	return exp(5.6333*(m_fDBH/(m_fDBH+18.0))+2.7826*log(m_fHgt)-1.7460*log(m_fKR_length)-5.3924);
}

double CGrotCalc::G_26(void)
{
	return 0.0;
}

double CGrotCalc::G_27(void)
{
	return 0.0;
}

double CGrotCalc::G_28(void)
{
	return 0.0;
}

double CGrotCalc::G_29(void)
{
	return 0.0;
}
// Calc Birch
double CGrotCalc::B_02(void)
{
	return 0.0;
}

double CGrotCalc::B_12(void)
{
	return 0.0;
}

double CGrotCalc::B_13(void)
{
	if (m_fDBH == 0.0 || m_fKR_length == 0.0 || m_fHgt == 0.0) return 0.0;
	return exp(10.7485*(m_fDBH/(m_fDBH+10.0))-1.2066*log(m_fHgt)+1.0409*log(m_fKR_length)-0.0415*m_fNCoord+0.0282);
}

double CGrotCalc::B_16(void)
{
	return 0.0;
}

double CGrotCalc::B_17(void)
{
	if (m_fDBH == 0.0 || m_fKR_length == 0.0 || m_fHgt == 0.0) return 0.0;
	return exp(12.0799*(m_fDBH/(m_fDBH+30.0))-0.3448*m_fHgt+2.7062*log(m_fHgt)+1.5634*m_fHgtOverSea-0.0914*m_fNCoord-0.67);
}

//-----------------------------------------
// REPOLA
//-----------------------------------------
// Pine
double CGrotCalc::T_Stemawood(void)
{
	return 0.0;
}

double CGrotCalc::T_Steambark(void)
{
	return 0.0;
}
// Formel uppdaterad enligt revision 2 (Bj�rn Hannrup, Skogforsk); 2010-04-20 P�D
double CGrotCalc::T_LivinBranches(void)
{
	if (m_fDBH == 0.0 || m_fHgt == 0.0 || m_fKR_length == 0.0) return 0.0;
	double fDIAMs = 2.0 + 1.25*m_fDBH;
	return exp(-5.166+13.085*(fDIAMs/(fDIAMs+12.0))-5.189*(m_fHgt/(m_fHgt+8.0))+1.110*log(m_fKR_length)+((0.020+0.063)/2.0));
}

double CGrotCalc::T_Needles(void)
{
	return 0.0;
}

// Formel uppdaterad enligt revision 2 (Bj�rn Hannrup, Skogforsk); 2010-04-20 P�D
double CGrotCalc::T_DeadBranches(void)
{
	if (m_fDBH == 0.0) return 0.0;
	double fDIAMs = 2.0 + 1.25*m_fDBH;
//	return exp(-5.334+10.789*(fDIAMs/(fDIAMs+16.0)))*1.242;
	return exp(-5.318+10.771*(fDIAMs/(fDIAMs+16.0)))*0.913;
}

double CGrotCalc::T_Stump(void)
{
	return 0.0;
}

double CGrotCalc::T_Roots_GT_1CM(void)
{
	return 0.0;
}

// Spruce
double CGrotCalc::G_Steamwood(void)
{
	return 0.0;
}

double CGrotCalc::G_Steambark(void)
{
	return 0.0;
}
// RG-A9
double CGrotCalc::G_LivinBranches(void)
{
	if (m_fDBH == 0.0 || m_fHgt == 0.0 || m_fKR_length == 0.0) return 0.0;
	double fDIAMs = 2.0 + 1.25*m_fDBH;
	double fValue = exp(-3.023+12.017*(fDIAMs/(fDIAMs+14.0))-5.722*(m_fHgt/(m_fHgt+5.0))+1.033*log(m_fKR_length)+((0.017+0.068)/2.0));
	return fValue;
}

double CGrotCalc::G_Needles(void)
{
	return 0.0;
}
// RG-A11
double CGrotCalc::G_DeadBranches(void)
{
	if (m_fDBH == 0.0 || m_fHgt == 0.0) return 0.0;
	double fDIAMs = 2.0 + 1.25*m_fDBH;
	double fValue = exp(-5.317+6.384*(fDIAMs/(fDIAMs+18.0))+0.982*log(m_fHgt))*1.208;
	return fValue;
}

double CGrotCalc::G_Stump(void)
{
	return 0.0;
}

double CGrotCalc::G_Roots_GT_1CM(void)
{
	return 0.0;
}

// Birch
double CGrotCalc::B_Steamwood(void)
{
	return 0.0;
}

double CGrotCalc::B_Steambark(void)
{
	return 0.0;
}
// RB-9
double CGrotCalc::B_LivinBranches(void)
{
	if (m_fDBH == 0.0 || m_fHgt == 0.0) return 0.0;
	double fDIAMs = 2.0 + 1.25*m_fDBH;
	double fValue = exp(-4.152+15.874*(fDIAMs/(fDIAMs+16.0))-4.407*(m_fHgt/(m_fHgt+10.0))+((0.02733+0.07662)/2.0));
	return fValue;
}

double CGrotCalc::B_Leafs(void)
{
	return 0.0;
}
// RB-10
double CGrotCalc::B_DeadBranches(void)
{
	if (m_fHgt == 0.0) return 0.0;
	double fDIAMs = 2.0 + 1.25*m_fDBH;
	double fValue = exp(-8.335+12.402*(fDIAMs/(fDIAMs+16.0)))*2.0737;
	return fValue;
}

double CGrotCalc::B_Stump(void)
{
	return 0.0;
}

double CGrotCalc::B_Roots_GT_1CM(void)
{
	return 0.0;
}
