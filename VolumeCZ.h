#if !defined(AFX_VOLUMECZ_H)
#define AFX_VOLUMECZ_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

////////////////////////////////////////////////////////////////////////////////////////
// CVolumeCZ

class CVolumeCZ : public CCalculationBaseClass
{
//private:

protected:

	void getTableVolume_Spruce(double dbh,double hgt,double *volume);	// Gran
	void getTableVolume_Fir(double dbh,double hgt,int age,double *volume);	// Fir
	void getTableVolume_Pine(double dbh,double hgt,int age,double *volume);	// Tall
	void getTableVolume_Larch(double dbh,double hgt,double *volume);	// L�rktr�d
	void getTableVolume_Oak(double dbh,double hgt,double *volume);	// Ek
	void getTableVolume_Beech(double dbh,double hgt,double *volume);	// Bok
	void getTableVolume_HornBeam(double dbh,double hgt,double *volume);	// Averbok
	void getTableVolume_Ash(double dbh,double hgt,double *volume);	// Ask
	void getTableVolume_Robina(double dbh,double hgt,double *volume);	// Robina
	void getTableVolume_Birch(double dbh,double hgt,double *volume);	// Bj�rk
	void getTableVolume_Alder(double dbh,double hgt,double *volume);	// Al

public:
	CVolumeCZ(void);
	CVolumeCZ(int spc_index,CTransaction_trakt rec1,
												  CTransaction_trakt_misc_data rec2,
													vecTransactionTraktData &tdata_list,
													vecTransactionSampleTree &sample_tree_list,
													vecTransactionDCLSTree &dcls_tree_list,
													vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);
};

#endif
