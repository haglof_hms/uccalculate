#include "StdAfx.h"

#include "ExportedFunctions.h"

#include "DoCalculation.h"

#include "GrotCalculations.h"

// Global variable; 090603 p�d
static CString sLangFN(getCalcLangFile());

////////////////////////////////////////////////////////////////////////////
// exported functions


//=========================================================================
// Returns a list of ALL bark-functions in this module; 070416 p�d
void getBarkFunctions(vecUCFunctions &list)
{
	if (existsFile(sLangFN))
	{
		readLangFile_functions(sLangFN,LANG_READ_FUNC_BARK,list);
	}	// if (existsFile(sLangFN))
	else
	{
		list.push_back(UCFunctions(ID_BARK_JO,_T("Jonsson och Ostlin" )));
		list.push_back(UCFunctions(ID_BARK_SF,_T("Skogforsk (Sk�rdare)" )));
		list.push_back(UCFunctions(ID_BARK_SO,_T("S�derbergs" )));
	}
}

//=========================================================================
// List of bark-functions
void getBarkFunctionList(vecUCFunctionList &list)
{
	if (existsFile(sLangFN))
	{
		readLangFile_function_list(sLangFN,LANG_READ_FUNC_BARK,LANG_READ_FUNC_BARK_TAG,list);
	}	// if (existsFile(sLangFN))
	else
	{

		//	Contstants for PINE (TALL)
		list.push_back(UCFunctionList(ID_BARK_JO,1,0,_T("Tall"),_T("Jonsson och Ostlin"), _T("Serie 1"), _T("2,0;0,075") ));
		list.push_back(UCFunctionList(ID_BARK_JO,1,1,_T("Tall"),_T("Jonsson och Ostlin"), _T("Serie 2"), _T("2,0;0,1") ));
		list.push_back(UCFunctionList(ID_BARK_JO,1,2,_T("Tall"),_T("Jonsson och Ostlin"), _T("Serie 3"), _T("0,0;0,13") ));
		list.push_back(UCFunctionList(ID_BARK_JO,1,3,_T("Tall"),_T("Jonsson och Ostlin"), _T("Serie 4"), _T("0,0;0,145") ));
		list.push_back(UCFunctionList(ID_BARK_JO,1,4,_T("Tall"),_T("Jonsson och Ostlin"), _T("Serie 5"), _T("0,0;0,16") ));
		list.push_back(UCFunctionList(ID_BARK_JO,1,5,_T("Tall"),_T("Jonsson och Ostlin"), _T("Serie 6"), _T("0,0;0,19") ));

		//	Contstants for SPRUCE (GRAN)
		list.push_back(UCFunctionList(ID_BARK_JO,2,0,_T("Gran"),_T("Jonsson och Ostlin"), _T("Serie 1"), _T("4,5;0,035") ));
		list.push_back(UCFunctionList(ID_BARK_JO,2,1,_T("Gran"),_T("Jonsson och Ostlin"), _T("Serie 2"), _T("5,0;0,05") ));
		list.push_back(UCFunctionList(ID_BARK_JO,2,2,_T("Gran"),_T("Jonsson och Ostlin"), _T("Serie 3"), _T("5,5;0,065") ));
		list.push_back(UCFunctionList(ID_BARK_JO,2,3,_T("Gran"),_T("Jonsson och Ostlin"), _T("Serie 4"), _T("6,0;0,08") ));

		//	Contstants for BIRCH (BJ�RK l�v)
		list.push_back(UCFunctionList(ID_BARK_JO,3,0,_T("Bj�rk"),_T("Jonsson och Ostlin"), _T("Serie 1"), _T("0,0;0,1") ));
		list.push_back(UCFunctionList(ID_BARK_JO,3,1,_T("Bj�rk"),_T("Jonsson och Ostlin"), _T("Serie 2"), _T("0,0;0,14") ));
		list.push_back(UCFunctionList(ID_BARK_JO,3,2,_T("Bj�rk"),_T("Jonsson och Ostlin"), _T("Serie 3"), _T("0,0;0,16") ));
	
		// Skogforsk bark-functions
		list.push_back(UCFunctionList(ID_BARK_SF,1,0,_T("Tall"),_T("Skogforsk"), _T("Sf_tall"), _T("Sk�rdar-funktion") ));
		list.push_back(UCFunctionList(ID_BARK_SF,2,1,_T("Gran"),_T("Skogforsk"), _T("Sf_gran"), _T("Sk�rdar-funktion") ));

		//	S�derbergs
		list.push_back(UCFunctionList(ID_BARK_SO,1,0,_T("Tall"), _T("S�derbergs"), _T("Norra"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,1,1,_T("Tall"), _T("S�derbergs"), _T("Mellersta"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,1,2,_T("Tall"), _T("S�derbergs"), _T("S�dra"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,2,0,_T("Gran"), _T("S�derbergs"), _T("Norra/Mellersta"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,2,1,_T("Gran"), _T("S�derbergs"), _T("S�dra"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,3,0,_T("Bj�rk"), _T("S�derbergs"), _T("Norra/Mellersta"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,3,1,_T("Bj�rk"), _T("S�derbergs"), _T("S�dra"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,4,0,_T("�vrigt l�v"), _T("S�derbergs"), _T("Norra/Mellersta"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,4,1,_T("�vrigt l�v"), _T("S�derbergs"), _T("S�dra"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,5,0,_T("Bok"), _T("S�derbergs"), _T("Hela landet"), _T("") ));
		list.push_back(UCFunctionList(ID_BARK_SO,6,0,_T("Ek"), _T("S�derbergs"), _T("Hela landet"), _T("") ));
	}
}


//=========================================================================
// Returns a list of ALL heightfunctions in this module; 070416 p�d
void getHeightFunctions(vecUCFunctions &list)
{
	if (existsFile(sLangFN))
	{
		readLangFile_functions(sLangFN,LANG_READ_FUNC_HEIGHT,list);
	}	// if (existsFile(sLangFN))
	else
	{
		list.push_back(UCFunctions(ID_H25,_T("H25") ));
		list.push_back(UCFunctions(ID_REGRESSION,_T("Regression") ));
		list.push_back(UCFunctions(ID_SODERBERGS,_T("S�derbergs") ));
	}
}

//=========================================================================
// List of height-functions
void getHeightFunctionList(vecUCFunctionList &list)
{
	if (existsFile(sLangFN))
	{
		readLangFile_function_list(sLangFN,LANG_READ_FUNC_HEIGHT,LANG_READ_FUNC_HEIGHT_TAG,list);
	}	// if (existsFile(sLangFN))
	else
	{

		//=====================================================================================
		// H25
		list.push_back(UCFunctionList(ID_H25,-1 /* Alla tr�dslag utom Gran */,0,_T("�vriga tr�dslag"),_T("H25"), _T("Hela landet"), _T("") ));
		list.push_back(UCFunctionList(ID_H25,2,0,_T("Gran"),_T("H25"), _T("Hela landet"), _T("") ));

		//=====================================================================================
		//	Regression
		list.push_back(UCFunctionList(ID_REGRESSION,-1 /* Alla tr�dslag */,0,_T("Alla tr�dslag"),_T("Regression"),_T("Hela landet"),_T("") ));

		//=====================================================================================
		//	S�derbergs
		list.push_back(UCFunctionList(ID_SODERBERGS,1,0,_T("Tall"),_T("S�derbergs"),_T("Norra"),_T("") ));
		list.push_back(UCFunctionList(ID_SODERBERGS,1,1,_T("Tall"),_T("S�derbergs"),_T("Mellersta"),_T("") ));
		list.push_back(UCFunctionList(ID_SODERBERGS,1,2,_T("Tall"),_T("S�derbergs"),_T("S�dra"),_T("") ));
		list.push_back(UCFunctionList(ID_SODERBERGS,2,0,_T("Gran"),_T("S�derbergs"),_T("Norra/Mellersta"),_T("") ));
		list.push_back(UCFunctionList(ID_SODERBERGS,2,1,_T("Gran"),_T("S�derbergs"),_T("S�dra"),_T("") ));
		list.push_back(UCFunctionList(ID_SODERBERGS,3,0,_T("Bj�rk"),_T("S�derbergs"),_T("Norra/Mellersta"),_T("") ));
//	"S�derbergs, Bj�rk, S�dra kommenterad bort, pga. utr�kningen blir felaktig";
// Uncommented, on AG. Test if function works; 091204 p�d
		list.push_back(UCFunctionList(ID_SODERBERGS,3,1,_T("Bj�rk"),_T("S�derbergs"),_T("S�dra"),_T("" ) ));
		list.push_back(UCFunctionList(ID_SODERBERGS,4,0,_T("�vrigt l�v"),_T("S�derbergs"),_T("Norra/Mellersta"),_T("") ));
		list.push_back(UCFunctionList(ID_SODERBERGS,4,1,_T("�vrigt l�v"),_T("S�derbergs"),_T("S�dra"),_T("") ));
		list.push_back(UCFunctionList(ID_SODERBERGS,5,0,_T("Bok"),_T("S�derbergs"),_T("Hela landet"),_T("") ));
		list.push_back(UCFunctionList(ID_SODERBERGS,6,0,_T("Ek"),_T("S�derbergs"),_T("Hela landet"),_T("") ));
	}
}

//=========================================================================
// Returns a list of ALL volumefunctions in this module; 070416 p�d
void getVolumeFunctions(vecUCFunctions &list)
{
	if (existsFile(sLangFN))
	{
		readLangFile_functions(sLangFN,LANG_READ_FUNC_VOLUME,list);
	}	// if (existsFile(sLangFN))
	else
	{
		list.push_back(UCFunctions(ID_BRANDELS,_T("Brandels") ));
		list.push_back(UCFunctions(ID_NASLUNDS,_T("N�slunds") ));
		list.push_back(UCFunctions(ID_HAGBERG_MATERN,_T("Hagberg/Matern") ));
		list.push_back(UCFunctions(ID_CYLINDER,_T("Cylinder") ));
	}
}

//=========================================================================
// List of volume-functions
void getVolumeFunctionList(vecUCFunctionList &list)
{
	if (existsFile(sLangFN))
	{
		readLangFile_function_list(sLangFN,LANG_READ_FUNC_VOLUME,LANG_READ_FUNC_VOLUME_TAG,list);
	}	// if (existsFile(sLangFN))
	else
	{
		//=====================================================================================
		// BRANDLES
		//	Contstants for PINE (Tall)
		list.push_back(UCFunctionList(ID_BRANDELS,1,0,_T("Tall"),_T("Brandels"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS,1,1,_T("Tall"),_T("Brandels"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS,1,2,_T("Tall"),_T("Brandels"),_T("Norra"),_T("BRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,1,3,_T("Tall"),_T("Brandels"),_T("S�dra"),_T("BRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,1,4,_T("Tall"),_T("Brandels"),_T("Norra"),_T("BRG+KRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,1,5,_T("Tall"),_T("Brandels"),_T("S�dra"),_T("BRG+KRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,1,6,_T("Tall"),_T("Brandels"),_T("S�dra"),_T("KRG+Bark") ));

		//	Contstants for SPRUCE (GRAN)
		list.push_back(UCFunctionList(ID_BRANDELS,2,0,_T("Gran"),_T("Brandels"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS,2,1,_T("Gran"),_T("Brandels"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS,2,2,_T("Gran"),_T("Brandels"),_T("Norra"),_T("H�H") ));
		list.push_back(UCFunctionList(ID_BRANDELS,2,3,_T("Gran"),_T("Brandels"),_T("Norra"),_T("H�H+KRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,2,4,_T("Gran"),_T("Brandels"),_T("Norra"),_T("BRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,2,5,_T("Gran"),_T("Brandels"),_T("Norra"),_T("H�H+BRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,2,6,_T("Gran"),_T("Brandels"),_T("S�dra"),_T("KRG") ));
		//	Contstants for BIRCH (BJ�RK l�v)
		list.push_back(UCFunctionList(ID_BRANDELS,3,0,_T("Bj�rk"),_T("Brandels"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS,3,1,_T("Bj�rk"),_T("Brandels"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS,3,2,_T("Bj�rk"),_T("Brandels"),_T("S�dra"),_T("BRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,3,3,_T("Bj�rk"),_T("Brandels"),_T("Norra"),_T("BRG+KRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,3,4,_T("Bj�rk"),_T("Brandels"),_T("S�dra"),_T("KRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS,3,5,_T("Bj�rk"),_T("Brandels"),_T("S�dra"),_T("Bark") ));

		//=====================================================================================
		// N�SLUNDS
		//	Contstants for PINE (Tall)
		list.push_back(UCFunctionList(ID_NASLUNDS,1,0,_T("Tall"),_T("N�slunds"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS,1,1,_T("Tall"),_T("N�slunds"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS,1,2,_T("Tall"),_T("N�slunds"),_T("Norra"),_T("St�rre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS,1,3,_T("Tall"),_T("N�slunds"),_T("S�dra"),_T("St�rre") ));
		//	Contstants for SPRUCE (Gran)
		list.push_back(UCFunctionList(ID_NASLUNDS,2,0,_T("Gran"),_T("N�slunds"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS,2,1,_T("Gran"),_T("N�slunds"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS,2,2,_T("Gran"),_T("N�slunds"),_T("Norra"),_T("St�rre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS,2,3,_T("Gran"),_T("N�slunds"),_T("S�dra"),_T("St�rre") ));
		//	Contstants for BIRCH (Bj�rk)
		list.push_back(UCFunctionList(ID_NASLUNDS,3,0,_T("Bj�rk"),_T("N�slunds"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS,3,1,_T("Bj�rk"),_T("N�slunds"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS,3,2,_T("Bj�rk"),_T("N�slunds"),_T("Norra"),_T("St�rre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS,3,3,_T("Bj�rk"),_T("N�slunds"),_T("S�dra"),_T("St�rre") ));
		//=====================================================================================
		// HAGBERG/MATERN (Volumefunctions for "Ek" and "Bok"); 070625 p�d
		//	Contstants for "Bok, p� bark"
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN,1,0,_T("Bok"),_T("Hagberg/Matern"),_T("Hel stam"),_T("Exkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN,1,1,_T("Bok"),_T("Hagberg/Matern"),_T("Hel Stam"),_T("Inkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN,1,2,_T("Bok"),_T("Hagberg/Matern"),_T("Klykstam"),_T("Exkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN,1,3,_T("Bok"),_T("Hagberg/Matern"),_T("Klykstam"),_T("Inkl. grenar") ));
		//	Contstants for "Ek, p� bark"
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN,2,0,_T("Ek"),_T("Hagberg/Matern"),_T("Hel stam"),_T("Exkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN,2,1,_T("Ek"),_T("Hagberg/Matern"),_T("Hel stam"),_T("Inkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN,2,2,_T("Ek"),_T("Hagberg/Matern"),_T("Klykstam"),_T("Exkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN,2,3,_T("Ek"),_T("Hagberg/Matern"),_T("Klykstam"),_T("Inkl. grenar") ));
		//=====================================================================================
		// Calulate volume for cylinder
		list.push_back(UCFunctionList(ID_CYLINDER,-1,0,_T("Volym"),_T("Cylinder"),_T("Alla tr�dslag"),_T("Inkl. bark") ));
		list.push_back(UCFunctionList(ID_CYLINDER,-1,1,_T("Volym"),_T("Cylinder"),_T("Alla tr�dslag"),_T("Exkl. bark") ));
	}
}


//=========================================================================
// Returns a list of ALL volumefunctions in this module; 070416 p�d
void getVolumeFunctions_ub(vecUCFunctions &list)
{
	if (existsFile(sLangFN))
	{
		readLangFile_functions(sLangFN,LANG_READ_FUNC_VOLUME_UB,list);
	}	// if (existsFile(sLangFN))
	else
	{
		list.push_back(UCFunctions(ID_BRANDELS_UB,_T("Brandels") ));
		list.push_back(UCFunctions(ID_NASLUNDS_UB,_T("N�slunds") ));
		list.push_back(UCFunctions(ID_HAGBERG_MATERN_UB,_T("Hagberg/Matern") ));	// Added 090305 p�d
		list.push_back(UCFunctions(ID_OMF_FACTOR_UB,_T("Konstant omf�ringstal") ));
	}
}

//=========================================================================
// Returns a list of ALL volume functions under bark, in this module; 071016 p�d
void DLL_BUILD getVolumeFunctionList_ub(vecUCFunctionList &list)
{
	if (existsFile(sLangFN))
	{
		readLangFile_function_list(sLangFN,LANG_READ_FUNC_VOLUME_UB,LANG_READ_FUNC_VOLUME_UB_TAG,list);
	}	// if (existsFile(sLangFN))
	else
	{
		//=====================================================================================
		// BRANDLES
		//	Contstants for PINE (Tall)
		list.push_back(UCFunctionList(ID_BRANDELS_UB,1,0,_T("Tall"),_T("Brandels"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,1,1,_T("Tall"),_T("Brandels"),_T("S�dra"),_T("Mindre") ));
//		list.push_back(UCFunctionList(ID_BRANDELS_UB,1,2,_T("Tall"),_T("Volym �ver stubbe"),_T("Norra"),_T("BRG") ));
//		list.push_back(UCFunctionList(ID_BRANDELS_UB,1,3,_T("Tall"),_T("Volym �ver stubbe"),_T("S�dra"),_T("BRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,1,2,_T("Tall"),_T("Brandels"),_T("Norra"),_T("BRG+KRG") ));
		//	Contstants for SPRUCE (GRAN)
		list.push_back(UCFunctionList(ID_BRANDELS_UB,2,0,_T("Gran"),_T("Brandels"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,2,1,_T("Gran"),_T("Brandels"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,2,2,_T("Gran"),_T("Brandels"),_T("Norra"),_T("H�H") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,2,3,_T("Gran"),_T("Brandels"),_T("Norra"),_T("H�H+KRG") ));
//		list.push_back(UCFunctionList(ID_BRANDELS_UB,2,4,_T("Gran"),_T("Volym �ver stubbe"),_T("Norra"),_T("BRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,2,4,_T("Gran"),_T("Brandels"),_T("Norra"),_T("H�H+BRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,2,5,_T("Gran"),_T("Brandels"),_T("S�dra"),_T("KRG") ));
		//	Contstants for BIRCH (BJ�RK l�v)
		list.push_back(UCFunctionList(ID_BRANDELS_UB,3,0,_T("Bj�rk"),_T("Brandels"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,3,1,_T("Bj�rk"),_T("Brandels"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,3,2,_T("Bj�rk"),_T("Brandels"),_T("S�dra"),_T("BRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,3,3,_T("Bj�rk"),_T("Brandels"),_T("Norra"),_T("KRG") ));
		list.push_back(UCFunctionList(ID_BRANDELS_UB,3,4,_T("Bj�rk"),_T("Brandels"),_T("Norra"),_T("Bark") ));

		//=====================================================================================
		// N�SLUNDS
		//	Contstants for PINE (Tall)
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,1,0,_T("Tall"),_T("N�slunds"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,1,1,_T("Tall"),_T("N�slunds"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,1,2,_T("Tall"),_T("N�slunds"),_T("Norra"),_T("St�rre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,1,3,_T("Tall"),_T("N�slunds"),_T("S�dra"),_T("St�rre") ));
		//	Contstants for SPRUCE (Gran)
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,2,0,_T("Gran"),_T("N�slunds"),_T("Norra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,2,1,_T("Gran"),_T("N�slunds"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,2,2,_T("Gran"),_T("N�slunds"),_T("Norra"),_T("St�rre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,2,3,_T("Gran"),_T("N�slunds"),_T("S�dra"),_T("St�rre") ));
		//	Contstants for BIRCH (Bj�rk)
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,3,0,_T("Bj�rk"),_T("N�slunds"),_T("Norra"),_T("Mindre")));
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,3,1,_T("Bj�rk"),_T("N�slunds"),_T("S�dra"),_T("Mindre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,3,2,_T("Bj�rk"),_T("N�slunds"),_T("Norra"),_T("St�rre") ));
		list.push_back(UCFunctionList(ID_NASLUNDS_UB,3,3,_T("Bj�rk"),_T("N�slunds"),_T("S�dra"),_T("St�rre") ));
		//=====================================================================================
		// HAGBERG/MATERN
		//	Contstants for "Bok, under bark"
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN_UB,1,0,_T("Bok"),_T("Hagberg/Matern"),_T("Hel stam"),_T("Exkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN_UB,1,1,_T("Bok"),_T("Hagberg/Matern"),_T("Hel Stam"),_T("Inkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN_UB,1,2,_T("Bok"),_T("Hagberg/Matern"),_T("Klykstam"),_T("Exkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN_UB,1,3,_T("Bok"),_T("Hagberg/Matern"),_T("Klykstam"),_T("Inkl. grenar") ));
		//	Contstants for "Ek,under bark"
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN_UB,2,0,_T("Ek"),_T("Hagberg/Matern"),_T("Hel stam"),_T("Exkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN_UB,2,1,_T("Ek"),_T("Hagberg/Matern"),_T("Hel stam"),_T("Inkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN_UB,2,2,_T("Ek"),_T("Hagberg/Matern"),_T("Klykstam"),_T("Exkl. grenar") ));
		list.push_back(UCFunctionList(ID_HAGBERG_MATERN_UB,2,3,_T("Ek"),_T("Hagberg/Matern"),_T("Klykstam"),_T("Inkl. grenar") ));

		//=====================================================================================
		// "Omf. tal" sk to ub< 071022 p[d
		list.push_back(UCFunctionList(ID_OMF_FACTOR_UB,0,0,_T("Omf�ring"),_T("Konstant omf�ringstal"),_T("m3sk-m3ub"),_T("Hela landet") ));
	}
}


//=========================================================================
// Returns a list of ALL GROT functions; 100311 p�d
void getGROTFunctionsCalc(vecUCFunctions &list)
{
	list.push_back(UCFunctions(ID_GROT_PINE,_T("Tall") ));
	list.push_back(UCFunctions(ID_GROT_SPRUCE,_T("Gran (Contorta)") ));
	list.push_back(UCFunctions(ID_GROT_BEACH,_T("Bj�rk") ));
	list.push_back(UCFunctions(ID_GROT_LEAF,_T("�vrigt l�v") ));
}	

// Perform actual calculation, depending on user selections
// of Height-  and Volume-function, Barkreduction etc; 070416 p�d
BOOL DLL_BUILD doCalculation(CTransaction_trakt rec1,
														 CTransaction_trakt_misc_data rec2,
														 vecTransactionTraktData &tdata_list,
														 vecTransactionSampleTree &sample_tree_list,
														 vecTransactionDCLSTree &dcls_tree_list,
														 vecTransactionTraktSetSpc &spc_list)
{
	// Always add this first in each function; 080131 p�d
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	UCDoCalculation *pCalc = new UCDoCalculation(rec1,									// Info. about common data for TREES
																							 rec2,									// Misc. info. e.g. diamterclass
																							 tdata_list,						// Trakt data info. e.g. Def. H25
																							 sample_tree_list,			// Sample Trees in Trakt, to be calculated on
																							 dcls_tree_list,				// Trees per diameterclass in Trakt, to be calculated on
																							 spc_list								// Specie specific information in this Trakt
																							 );	
	if (pCalc != NULL)
	{
		pCalc->doCalculation(tdata_list,sample_tree_list,dcls_tree_list,spc_list);
	
		delete pCalc;
	}

	return TRUE;
}

//-------------------------------------------------------------------------------------
//	DoGROTCalulate function; 100311 p�d
//	- St�ndortsindex f�r Tall eller Gran (m)
//	- Breddgrad
//	- H�jd �ver havet (km!)
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doGROTCalc(CTransaction_trakt rec,vecTransactionTraktData &trakt_data,vecTransactionTraktSetSpc &spc_list,vecTransactionDCLSTree &dcls_tree_list)
{
	vecTransactionDCLSTree vec;
	CGrotCalc *pCalc = new CGrotCalc(rec,trakt_data,spc_list,dcls_tree_list);
	if (pCalc)
	{
		pCalc->DoGrotCalc();

		trakt_data = pCalc->getTraktData();		
		dcls_tree_list = pCalc->getDCLSTrees();
		
		delete pCalc;
	}

	return TRUE;
}
