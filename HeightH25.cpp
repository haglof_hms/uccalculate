#include "StdAfx.h"

#include "EnterValueDialog.h"

#include "heightH25.h"

// PROTECTED

//==========================================================================
// Calculate the height for trees; 070418 p�d
// This method first calculates Heigth*Diam*Diam and Diam*Diam and 
// uses these values to claculate the H25 value. 
// ALL IS DONE FOR ACTIVE SPECIE; 070418 p�d

BOOL CHeightH25::setHeightForTrees(double H25_entered)
{
	CString S;
	long nTreeCnt;
	double fSumHDD;
	double fSumDD;
	double fH25Hgt;
	double fH25Diam;
	double fH25;
	double fLog;
	double fHeight;
	long nNumOfSampleTreesPerSpecie;


	// Make sure there's any trees; 070417 p�d
	if (m_nNumOfDCLSTrees == 0 || m_nNumOfSampleSpecies == 0)
		return FALSE;

	// Get information on active specie in m_vecSetSpc; 070418 p�d
	CTransaction_trakt_set_spc spc = m_vecSetSpc[m_nSpcIndex];

	// Make sure the specie in CTransaction_trakt_set_spc also
	// is in m_vecSampleTrees. If not, just get the hell out of this method; 070525 p�d
	if (!isSpecieInTrees(spc.getSpcID()))
	{
		return FALSE;
	}


	fSumHDD = 0.0;
	fSumDD = 0.0;
	fH25Hgt	= 0.0;
	fH25Diam = 0.0;
	fH25 = 0.0;
	fLog = 0.0;
	fHeight = 0.0;
	// Get number of sampletrees for Specie. If there are non, no need to
	// do SumHDD and SumDD; 070522 p�d
	nNumOfSampleTreesPerSpecie = getNumOfSampleTrees(spc.getSpcID());
	// Loop m_vecSampleTrees to calculate Height*Diam*Diam and Diam*Diam for active Specie; 070418 p�d
	for (nTreeCnt = 0;nTreeCnt < m_nNumOfSampleTrees;nTreeCnt++)
	{
		CTransaction_sample_tree tree = m_vecSampleTrees[nTreeCnt];
		if ((m_vecSampleTrees[nTreeCnt].getTreeType() == SAMPLE_TREE || 
				 m_vecSampleTrees[nTreeCnt].getTreeType() == TREE_SAMPLE_REMAINIG || 
				 m_vecSampleTrees[nTreeCnt].getTreeType() == TREE_SAMPLE_EXTRA || 
				 m_vecSampleTrees[nTreeCnt].getTreeType() == TREE_OUTSIDE) && 
				 tree.getSpcID() == spc.getSpcID())
		{
			fSumHDD	+= (tree.getHgt()/10.0)*(pow(tree.getDBH()/10.0,2) );
			fSumDD	+= pow(tree.getDBH()/10.0,2);
		}	// if (tree.getTypeOfTree() == SAMPLE_TREE && tree.getSpcID() == spc.getSpcID())
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)
	
	// Check that fSumHDD > 0.0 and fSumDD > 0.0; 070418 p�d
	// If we have sampletrees for specie, use them to calculate
	// the H25 value, otherwise use default H25 value, form
	// TraktData or set by in method askUserForH25Value; 070522 p�d
	if (fSumHDD > 0.0 && fSumDD > 0.0 && nNumOfSampleTreesPerSpecie > 0)
	{
		fH25Hgt	= (fSumHDD / fSumDD);
		fH25Diam = sqrt(fSumDD / nNumOfSampleTreesPerSpecie);
		fLog = (log(fH25Diam)/log(10.0));

		// H25 is calcualted different for Spruce (Gran); 070418 p�d
		if (spc.getHgtFuncSpcID() == 2) // Gran
			fH25 = (fH25Hgt - 9.022 + 6.454*fLog) / (1.613*fLog-1.256);
		else // All other species
			fH25 = (fH25Hgt - 1.518 + 1.086*fLog) / (1.086*fLog-0.518);
		
		if (fH25 < H25_MIN_VALUE) 
		{
			fH25 = H25_MIN_VALUE;
		}
		else if (fH25 > H25_MAX_VALUE)
		{
			fH25 = H25_MAX_VALUE;
		}
	}
	else
	{
		fH25Hgt	= 1.0;
		fH25Diam = 1.0;

		if (H25_entered > 0.0)
			fH25 = H25_entered;
		else
		{
			if (getTraktData(spc.getSpcID()) != NULL)
				fH25 = getTraktData(spc.getSpcID())->getH25();
			else
				fH25 = H25_entered;
		}
	}

	//--------------------------------------------------------------------------
	// Add information on H25 value to m_vecSetSpc; 070418 p�d
	setTraktData_h25(spc.getSpcID(),fH25);

	//--------------------------------------------------------------------------
	// Loop m_vecDCLSTrees and calculate heights for trees in diamterclasses; 070820 p�d
	// Loop m_vecSampleTrees and calculate heights for trees except sampletrees; 070418 p�d
	for (nTreeCnt = 0;nTreeCnt < m_nNumOfDCLSTrees;nTreeCnt++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[nTreeCnt];
		if (tree.getNumOfRandTrees() != -999)
		{
			// Make sure specie is ok and that there's not already a
			// height specified (if height IT'S A SAMPLE TREE); 080122 p�d
			if (tree.getSpcID() == spc.getSpcID()) // && tree.getHgt() == 0.0)
			{
				fLog = log(getClassMid(tree)/10.0)/log(10.0);
				// H25Inv, i.e. the height, is calcualted different for Spruce (Gran); 070418 p�d
				if (spc.getHgtFuncSpcID() == 2) // Gran
					fHeight = (9.022 - 6.454*fLog - 1.256*fH25 + 1.613*fH25*fLog);
				else
					fHeight = (1.518 - 1.086*fLog - 0.518*fH25 + 1.086*fH25*fLog);

				// Check that fHeight > 1.0, if not set fHeight = 1.3; 090505 p�d
				if (fHeight < 1.0) fHeight = 1.3;	// "Br�sth�jd"; 090505 p�d

				// Add height to m_vecSampleTrees
				m_vecDCLSTrees[nTreeCnt].setHgt(fHeight*10.0);	// From (dm) to (mm)
			}
		}	// if ((tree.getTreeType() == NOT_SAMPLE_TREE || tree.getTreeType() == TREE_WITHIN_NO_SAMPLE) && tree.getSpcID() == spc.getSpcID())
	}	// for (nTreeCnt = 0;nTreeCnt < m_nNumOfDCLSTrees;nTreeCnt++)

	return TRUE;
}

BOOL CHeightH25::setHeightForDGVTree(void)
{
	int nSpcID;
	double fH25;
	double fLog;
	double fHeight;
	double fDGV;

	CString S;
	
	// Make sure there's any trees; 070417 p�d
	if (m_nNumOfDCLSTrees == 0 || m_nNumOfSampleSpecies == 0)
		return FALSE;

	nSpcID = getSpecieID();

	// Make sure the specie in CTransaction_trakt_set_spc also
	// is in m_vecSampleTrees. If not, just get the hell out of this method; 070525 p�d
	if (!isSpecieInTrees(nSpcID))
	{
		// Also, reset som values; HGV to 0.0. 
		// Just to confirm that there's no trees for this specie; 070525 p�d
		setTraktData_hgv(nSpcID,0.0);
		return FALSE;
	}

	fH25 = 0.0;
	fLog = 0.0;
	fHeight = 0.0;
	// Get calculated DGV value (SetTraktData()); 070525 p�d
	if (getTraktData(nSpcID) != NULL)
	{
		fDGV = getTraktData(nSpcID)->getDGV();
		fH25 = getTraktData(nSpcID)->getH25();
	}
	else
	{
		fDGV = 0.0;
		fH25 = 0.0;
	}
	//--------------------------------------------------------------------------
	if (fDGV > 0.0)
	{
		fLog = log(fDGV)/log(10.0);
		// H25Inv, i.e. the height, is calcualted different for Spruce (Gran); 070418 p�d
		if (nSpcID == 2) // Gran
			fHeight = (9.022 - 6.454*fLog - 1.256*fH25 + 1.613*fH25*fLog);
		else
			fHeight = (1.518 - 1.086*fLog - 0.518*fH25 + 1.086*fH25*fLog);

		if (fHeight <= -0.0)
			fHeight = 0.0;

		setTraktData_hgv(nSpcID,fHeight);
	}	// if (fDGV > 0.0)

	return TRUE;
}

double CHeightH25::askUserForH25Value(LPCTSTR spc_name)
{
	double fH25Value = 0.0;
	CEnterValueDialog *pDlg = new CEnterValueDialog(spc_name);
	if (pDlg != NULL)
	{
		if (pDlg->DoModal() == IDOK)
		{
			fH25Value = pDlg->getValue();

		}
		delete pDlg;
	}

	return fH25Value;
}

// PUBLIC
CHeightH25::CHeightH25(void)
	: CCalculationBaseClass()
{
}

CHeightH25::CHeightH25(int spc_index,CTransaction_trakt rec1,
																		 CTransaction_trakt_misc_data rec2,
																		 vecTransactionTraktData &tdata_list,
																		 vecTransactionSampleTree &sample_tree_list,
																		 vecTransactionDCLSTree &dcls_tree_list,
																		 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}

BOOL CHeightH25::calculate(vecTransactionTraktData& vec1,
													 vecTransactionSampleTree& vec2,
													 vecTransactionDCLSTree& vec4,		// Added 070820 p�d
													 vecTransactionTraktSetSpc& vec3)
{
	CString S;
	CString sSpcName;
	double fHgt = 0.0;
	double fH25Entered = 0.0;
	BOOL bHasSampleTrees;
	CTransaction_trakt_data *pTraktData;

	// Make sure we have trees; 070417 p�d
	if (m_nNumOfDCLSTrees == 0) 
		return FALSE;

	// Make sure we have species; 070417 p�d
	if (m_nNumOfSampleSpecies == 0) 
		return FALSE;

	//===========================================================================
	// Start by checkin' if there is sampletrees for species; 070417 p�d
	CTransaction_trakt_set_spc spc = m_vecSetSpc[m_nSpcIndex];
	bHasSampleTrees = getHasSpecieSampleTrees();

	// If not, check if there's a default h25 value for specie; 070417 p�d
	fH25Entered = vec3[m_nSpcIndex].getDefaultH25();

	//===========================================================================
	// Calcualte heights for each tree in m_vecSampleTrees (except sampletrees); 070418 p�d
	setHeightForTrees(fH25Entered);
	setHeightForDGVTree();

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}
