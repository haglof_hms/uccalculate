#include "StdAfx.h"

#include "BarkJO.h"


// PUBLIC:
CBarkJO::CBarkJO(void)
	: CCalculationBaseClass()
{
}

CBarkJO::CBarkJO(int spc_index,CTransaction_trakt rec1,
															 CTransaction_trakt_misc_data rec2,
															 vecTransactionTraktData &tdata_list,
															 vecTransactionSampleTree &sample_tree_list,
															 vecTransactionDCLSTree &dcls_tree_list,
															 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}

BOOL CBarkJO::calculate(vecTransactionTraktData& vec1,
												vecTransactionSampleTree& vec2,
												vecTransactionDCLSTree& vec4,
												vecTransactionTraktSetSpc& vec3)
{
	double fDCLS = 0.0;
	double fBarkReduction = 0.0;
	int nBarkSpcID = -1;	// Function for specie is used; 070417 p�d
	int nBarkIndex = -1;  // Which function for specie is used; 070417 p�d
	int nSpcID = -1;
	// First check that there's any trees to begin with.
	// If not, just return FALSE; 070417 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nBarkSpcID = getBarkSpcID();	
	nBarkIndex = getBarkIndex();
	// Loop through the tree list (m_vecSampleTrees) and
	// calculate bark-reduction for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getBarkID() to get which species
	// method to use and getBarkIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070417 p�d
	for (int i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		if (tree.getSpcID() == nSpcID)
		{
			fDCLS = getClassMid(tree); ///10.0;
			// Depending on specie, run function accoring to
			// nBarkIndex value; 070417 p�d
			if (nBarkSpcID == 1)	// Tall
			{
				fBarkReduction = getBarkPine(fDCLS,nBarkIndex);
			}
			if (nBarkSpcID == 2) // Gran
			{
				fBarkReduction = getBarkSpruce(fDCLS,nBarkIndex);
			}
			if (nBarkSpcID == 3)	// Bj�rk
			{
				fBarkReduction = getBarkBirch(fDCLS,nBarkIndex);
			}
			m_vecDCLSTrees[i].setBarkThick(fBarkReduction);
		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}

// PROTECTED
double CBarkJO::getBarkPine(double dbh,int idx)
{
	if (idx > 5) return 0.0;

	double k1[6] = {2.0,2.0,0.0,0.0,0.0,0.0};
	double k2[6] = {0.075,0.1,0.13,0.145,0.16,0.19};

	return (dbh - (dbh - (k1[idx] + (k2[idx] * dbh ))))/2.0;
}

double CBarkJO::getBarkSpruce(double dbh,int idx)
{
	if (idx > 3) return 0.0;

	double k1[4] = {4.5,5.0,5.5,6.0};
	double k2[4] = {0.035,0.05,0.065,0.08};

	return (dbh - (dbh - (k1[idx] + (k2[idx] * dbh ))))/2.0;
}

double CBarkJO::getBarkBirch(double dbh,int idx)
{
	if (idx > 2) return 0.0;

	double k1[3] = {0.0,0.0,0.0};
	double k2[3] = {0.1,0.14,0.16};

	return (dbh - (dbh - (k1[idx] + (k2[idx] * dbh ))))/2.0;
}