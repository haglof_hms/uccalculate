/* 2007-04-13 Per-�ke Danielsson */
#if !defined(AFX_HEIGHTSODERBERGS_H)
#define AFX_HEIGHTSODERBERGS_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

#include "MessageDlg.h"

////////////////////////////////////////////////////////////////////////////////////////
// CHeightSoderbergs

class CHeightSoderbergs : public CCalculationBaseClass
{
	//private

	//======================================================
	// Tall (Pine)

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Height over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Delad yta"  Y/N; Y = 1, N = 0
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getHgtPine_N(CTransaction_dcls_tree tree);

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Height over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Gran"
	// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
	// - "Delad yta" Y/N; Y = 1, N = 0
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getHgtPine_M(CTransaction_dcls_tree tree);

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Height over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Gran"
	// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
	// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
	// - "Delad yta" Y/N; Y = 1, N = 0
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getHgtPine_S(CTransaction_dcls_tree tree);

	//======================================================
	// Spruce (Gran)

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Height over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Gran"
	// - "Delad yta" Y/N; Y = 1, N = 0
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getHgtSpruce_NandM(CTransaction_dcls_tree tree);

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Height over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Gran"
	// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
	// - "Delad yta" Y/N; Y = 1, N = 0
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getHgtSpruce_S(CTransaction_dcls_tree tree);

	//======================================================
	// Birch (Bj�rk)

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Height over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Gran"
	// - "Delad yta" Y/N; Y = 1, N = 0
	double getHgtBirch_NandM(CTransaction_dcls_tree tree);


	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Height over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Gran"
	// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
	// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
	// - "Delad yta" Y/N; Y = 1, N = 0
	double getHgtBirch_S(CTransaction_dcls_tree tree);

	//======================================================
	// Other Leaf (�vrigt L�v)

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - "Tr�dslagsf�rdelning Gran"
	// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
	// - "Kustn�ra < 50 km" Y/N; Y = 1, N = 0
	double getHgtOthLeaf_NandM(CTransaction_dcls_tree tree);

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
	// - "Breddgrad"
	// - Height over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Tall"
	// - "Tr�dslagsf�rdelning Gran"
	// - "Tr�dslagsf�rdelning Bj�rk (l�v)"
	// - "Delad yta" Y/N; Y = 1, N = 0
	double getHgtOthLeaf_S(CTransaction_dcls_tree tree);


	//======================================================
	// Beech (Bok) All species

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
 	// - "Breddgrad"
	// - Height over the sea
	// - "Tr�dslagsf�rdelning Gran"
	// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
	// - "Region 5" Y/N; Y = 1, N = 0
	// - "Delad yta" Y/N; Y = 1, N = 0
	double getHgtBeech(CTransaction_dcls_tree tree);

	//======================================================
	// Oak (Ek) All species

	//---------------------------------------------------
	// Arguments used in this function; 0700424 p�d
	// - Diameter at breast height
	// - "Best�nds-�lder"
	// - SI value of H100; ex. G30 = 300
 	// - "Breddgrad"
	// - Height over the sea
	// - Quota for DBH/Max diameter
	// - "Tr�dslagsf�rdelning Gran"
	// - "Syd�stra Sverige" Y/N; Y = 1, N = 0
	// - "Region 5" Y/N; Y = 1, N = 0
	// - "Delad yta" Y/N; Y = 1, N = 0
	double getHgtOak(CTransaction_dcls_tree tree);

	double getCalibrationFactor(void);

	double m_fSoderbergsH25Value;
	double m_fSoderbergsHGVValue;
	double m_fCalibrationFactor;

	BOOL m_bIsCalculated;
protected:
	// All species
	double setHeightForPine(int hgt_index,CTransaction_dcls_tree tree);	
	double setHeightForSpruce(int hgt_index,CTransaction_dcls_tree tree);	
	double setHeightForBirch(int hgt_index,CTransaction_dcls_tree tree);	
	double setHeightForOtherLeaf(int hgt_index,CTransaction_dcls_tree tree);	
	double setHeightForBeech(int hgt_index,CTransaction_dcls_tree tree);	
	double setHeightForOak(int hgt_index,CTransaction_dcls_tree tree);	
public:
	CHeightSoderbergs(void);
	CHeightSoderbergs(int spc_index,CTransaction_trakt rec1,
																	CTransaction_trakt_misc_data rec2,
																	vecTransactionTraktData &tdata_list,
																	vecTransactionSampleTree &sample_tree_list,
																	vecTransactionDCLSTree &dcls_tree_list,
																	vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);

	double getSoderbergsH25Value(void);

};



#endif