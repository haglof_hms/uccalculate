#if !defined(AFX_UCCALCULATIONBASECLASS_H)
#define AFX_UCCALCULATIONBASECLASS_H

#include "pad_calculation_classes.h"

// Simple class holding information on
// specie.
// For sampletrees
//	- Number of sampletrees, sum. diameter,
//	- Sum. diameter
//	- Avg. diameter
//	-	Avg. height
// For all trees
//	- Totral number of trees for specie
//	- Maximum diameter 
//	- Percentage of specie (Tr�dslagsf�rdelning)
class _tree_data
{
	//private:
	int m_nSpcID;
	int m_nNumOfSampleTrees;
	int m_nNumOfDCLSTrees;
	double m_fSumDiam;	// Sum. of diamters for sampletrees; 070419 p�d
	double m_fAvgDiam;	// Avg. diamter
	double m_fAvgHgt;		// Avg. height

	long m_nNumOfTrees;	// Total number of trees for specie
	double m_fMaxDia;		// OBS! This is the largest diamter, of ALL trees, not only sampletrees; 070420 p�d
	double m_fSpcPerc;	// "Tr�dslagsf�rdelning"
public:
	_tree_data(void)
	{
		m_nSpcID = -1;
		// Tree specifics/Specie
		m_nNumOfSampleTrees = -1;
		m_nNumOfDCLSTrees = -1;
		m_fSumDiam	= 0.0;
		m_fAvgDiam = 0.0;
		m_fAvgHgt = 0.0;

		// Tree specifics/Specie
		m_nNumOfTrees = 0;
		m_fMaxDia = 0.0;
		m_fSpcPerc = 0.0;
	}
	_tree_data(int spc_id,
						 int num_of_sample_trees,
						 int num_of_dcls_trees,
						 double sum_diam,
						 double avg_diam,
						 double avg_hgt,
						 long num_of_trees,
						 double max_dia,
						double spc_perc)
	{
		m_nSpcID = spc_id;
		m_nNumOfSampleTrees = num_of_sample_trees;
		m_nNumOfDCLSTrees = num_of_dcls_trees;
		m_fSumDiam	= sum_diam;
		m_fAvgDiam = avg_diam;
		m_fAvgHgt = avg_hgt;

		m_nNumOfTrees = num_of_trees;
		m_fMaxDia = max_dia;
		m_fSpcPerc = spc_perc;
	}

	int getSpcID(void)							{ return m_nSpcID; }
	int getNumOfSampleTrees(void)		{ return m_nNumOfSampleTrees; }
	int getNumOfDCLSTrees(void)			{ return m_nNumOfDCLSTrees; }
	double getSumDiam(void)					{ return m_fSumDiam; }
	double getAvgDiam(void)					{ return m_fAvgDiam; }
	double getAvgHgt(void)					{ return m_fAvgHgt;	}

	long getNumOfTrees(void)				{ return m_nNumOfTrees; }
	double getMaxDia(void)					{ return m_fMaxDia;	}
	double getSpcPerc(void)					{ return m_fSpcPerc;	}
};

typedef std::vector<_tree_data> vecTreeData;

class CCalculationBaseClass
{
	//privtate
	CTransaction_trakt m_recTrakt;						// Trakt specifics
	CTransaction_trakt_misc_data m_recMiscData;
protected:
	//--------------------------------------------------------------------------------
	// Sample trees
	long m_nNumOfSampleTrees;		// Total number of trees in m_vecSampleTrees; 070424 p�f
	UINT m_nNumOfSampleSpecies;	// Number of species in m_vecSampleTrees; 070417 p�d
	vecTransactionSampleTree m_vecSampleTrees;	// Common to ALL calculations (Trees!); 070416 p�d
															// the m_vecSpecies in BOOL UCDoCalculation::doCalculation(void); 070418 p�d

	//--------------------------------------------------------------------------------
	// Trees per diameterclass; 070820 p�d
	long m_nNumOfDCLSTrees;		// Total number of trees in m_vecDCLSTrees; 070820 p�d
	UINT m_nNumOfDCLSSpecies;	// Number of species in m_vecDCLSTrees; 070820 p�d
	vecTransactionDCLSTree m_vecDCLSTrees;	// Common to ALL calculations (Trees!); 070820 p�d
														// the m_vecSpecies in BOOL UCDoCalculation::doCalculation(void); 070820 p�d
	int getNumOfTreesForSpc(int spc_id);	// Get number of species for a sepecific specie; 090227 p�d
	//--------------------------------------------------------------------------------

	vecTransactionTraktData m_vecTraktData;		// Trakt data specifics

	// Information extracted from m_vecSampleTrees; 070424 p�d
	double getGreenCrownPerc(void);
	double getGreenCrownPercForSpc(int spc_id);

	// Information from Sample trees; 070820 p�d
	// Information extracted from m_vecTreeData; 070424 p�d
	long getNumOfSampleTrees(int spc_id);		// Return number of sampletrees for specie; 070418 p�d
	BOOL getHasSpecieSampleTrees(void);			// Check if there's sampletrees for specie; 070417 p�d
	double getSpcSumSampleTreeHgt(int spc);	// Get sum of heights for sampletrees; 090227 p�d
	double getSpcPercent(int spc);					// Get the percentage of Specie; 070424 p�d
	double getSpcAvgHgt(int spc);						// Get avg. height for specie; 071126 p�d
	// Get the percentage of Specie, based on basel area; 090227 p�d
	// If only_tgl = TRUE, calculate spc mix only for: Tall,Gran or Bj�rk; 090227 p�d
	double getSpcPercent_GY(int spc,BOOL only_tgl = TRUE);		

	// Information from DCLS trees; 070820 p�d
	double getMaxDiameter(void);						// Get the largest diamter in m_vecDCLSTrees, doesn't matter if it's a sampletree or not; 070420 p�d
	double getMinDiameter(void);						// Get the smallest diamter in m_vecDCLSTrees, doesn't matter if it's a sampletree or not; 070420 p�d
	double getDiamQuata(double dbh);

	double getSumGY(void);	// Sum Baselarea; 090227 p�d

	// Information extracted from m_vecSpecies; 070424 p�d
	vecTransactionTraktSetSpc m_vecSetSpc;		// Species in vecTransactionSampleTree; 070417 p�d
	int m_nSpcIndex;				// This index keeps track of which item's selected in
	int getSpecieID(void);									// Get ID for active specie, set in BOOL UCDoCalculation::doCalculation(void); 070418 p�d
	double getM3SkToM3Fub(void);						// Retrive the "omf�ringstal", from m3sk to m3fub for specie; 070524 p�d
	
	int getBarkID(void);										// Get ID for bark-function  to use (ex Jonsson-Ostlin); 070417 p�d
	int getBarkSpcID(void);									// Get ID for specie for bark-function; 070425 p�d
	int getBarkIndex(void);									// Get Index for bark-function on BarkID; 070417 p�d

	int getVolumeID(void);									// Get ID for volume-function to use (ex Brandels); 070418 p�d
	int getVolumeSpcID(void);								// Get ID for specie for volume-function; 070425 p�d
	int getVolumeIndex(void);								// Get Index for volume-function; 070418 p�d
	
	int getVolumeID_ub(void);								// Get ID for volume-function to used for m3ub (ex Brandels); 070418 p�d
	int getVolumeSpcID_ub(void);						// Get ID for specie for volume-function m3ub; 070425 p�d
	int getVolumeIndex_ub(void);						// Get Index for volume-function m3ub; 070418 p�d

	int getHeightID(void);									// Get ID for height-function to use (ex H25); 070418 p�d
	int getHeightSpcID(void);								// Get ID for bark-function for specie; 070417 p�d
	int getHeightIndex(void);								// Get Index for height-function on BarkID; 070418 p�d

	int getNearCoast(void);									// Get value set, used in "S�derbergs"
	int getSouthEast(void);									// Get value set, used in "S�derbergs"
	int getRegion5(void);										// Get value set, used in "S�derbergs"
	int getPartOfPlot(void);								// Get value set, used in "S�derbergs"

	double getOmfValueForM3SkToM3Ub(void);	// Get value, set in set specie table; 071022 p�d
public:
	// CTransaction_trakt specific data
	CTransaction_trakt& getTrakt(void);								// Returns the CTransaction_trakt class
	
	// CTransaction_trakt_data specific data
	CTransaction_trakt_data *getTraktData(int spc);			// Returns the CTransaction_trakt_data class for specie
	void setTraktData_h25(int spc,double h25);					// Add H25 value to m_vecTraktData for specie
	void setTraktData_hgv(int spc,double hgv);					// Add HGV value to m_vecTraktData for specie
	void setTraktData_gcrown(int spc,double gcrown);		// Add "Gr�nkroneandel (%) value to m_vecTraktData for specie

	vecTreeData m_vecTreeData;							// Information on sampletrees/specie in vecTransactionSampleTree; 070417 p�d
	void setTreeData(vecTreeData &v);

	// Get the SI part from H100, ex. G30 => SI = 300; 070425 p�d
	int getSIfromH100(LPCTSTR value);
	// Check if a specie is in the m_vecSampleTrees; 070525 p�d
	BOOL isSpecieInTrees(int spc_id);
	// Get Name for active specie, from m_vecSampleTrees; 070625 p�d
	CString getSpecieName(int spc_id);								

	CTransaction_dcls_tree* getTree(int spc_id);		// Return a CTransaction_sample_tree class, based on specie; 070530 p�d

	// Helpmethods
	BOOL isNumeric(LPCTSTR value,int start,int end = -1);	// Check if the value is a numeric value (just digits); 070418 p�d

	double getClassMid(CTransaction_dcls_tree& v)
	{
		double fDCLS = (v.getDCLS_to() - v.getDCLS_from());
		double fMid = fDCLS / 2.0;
/*
		CString S;
		S.Format("getClassMid\nTo %f\nFrom %f\nDCLS %f\nMid %f",
				v.getDCLS_to(),v.getDCLS_from(),fDCLS,fMid);
		AfxMessageBox(S);
*/
		return (v.getDCLS_from() + fMid)*10.0;	// From (cm) to (mm)
	}
public:
	CCalculationBaseClass(void);
	CCalculationBaseClass(int,CTransaction_trakt,
														CTransaction_trakt_misc_data,
														vecTransactionTraktData &,
														vecTransactionSampleTree &,
														vecTransactionDCLSTree &,
														vecTransactionTraktSetSpc &);

	virtual BOOL calculate(vecTransactionTraktData &,
												 vecTransactionSampleTree &,
												 vecTransactionDCLSTree &,
												 vecTransactionTraktSetSpc &) = 0;	// Must have this function in derived classes; 070416 p�d

};



#endif
