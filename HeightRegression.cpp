#include "StdAfx.h"

#include "EnterValueDialog.h"

#include "HeightRegression.h"

// PROTECTED

//==========================================================================
// Calculate the height for trees; 070418 p�d
// ALL IS DONE FOR ACTIVE SPECIE; 070418 p�d

BOOL CHeightRegression::setHeightForTrees(void)
{
	long nTreeCnt;
	double fDBH;
	double fHgt;
	double fSumLogDBH;
	double fSumHgt;
	double fSumLogDBH2;
	double fSumLogDBH_Hgt;
	double fFactor_k;
	double fFactor_m;
	long nNumOfSampleTreesPerSpecie;

	// Make sure there's any trees; 070417 p�d
	//if (m_nNumOfSampleTrees == 0 || m_nNumOfSampleSpecies == 0)
	if (m_nNumOfSampleSpecies == 0)	// #4530 20151006 J�
		return FALSE;

	// Get information on active specie in m_vecSetSpc; 070418 p�d
	CTransaction_trakt_set_spc spc = m_vecSetSpc[m_nSpcIndex];

	fDBH	= 0.0;
	fHgt = 0.0;
	fSumLogDBH = 0.0;
	fSumHgt = 0.0;
	fSumLogDBH2 = 0.0;
	fSumLogDBH_Hgt = 0.0;
	// Loop m_vecSampleTrees to calculate Height*Diam*Diam and Diam*Diam per Specie; 070418 p�d
	if (m_nNumOfSampleTrees != 0) // #4530 20151006 J�
	{
		for (nTreeCnt = 0;nTreeCnt < m_nNumOfSampleTrees;nTreeCnt++)
		{
			CTransaction_sample_tree tree = m_vecSampleTrees[nTreeCnt];
			if ((m_vecSampleTrees[nTreeCnt].getTreeType() == SAMPLE_TREE || 
				m_vecSampleTrees[nTreeCnt].getTreeType() == TREE_SAMPLE_REMAINIG || 
				m_vecSampleTrees[nTreeCnt].getTreeType() == TREE_SAMPLE_EXTRA || 
				m_vecSampleTrees[nTreeCnt].getTreeType() == TREE_OUTSIDE) && 
				tree.getSpcID() == spc.getSpcID())
			{
				fDBH			= tree.getDBH()/10.0;
				fHgt			= tree.getHgt()/10.0;
				fSumLogDBH	+= log(fDBH);
				fSumHgt			+= fHgt;
				fSumLogDBH2	+= pow(log(fDBH),2);
				fSumLogDBH_Hgt += (log(fDBH)*fHgt);
			}	// if (tree.getTypeOfTree() == SAMPLE_TREE && tree.getSpcID() == spc.getSpcID())
		}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

		// Get number of sampletrees for specie; 070418 p�d
		nNumOfSampleTreesPerSpecie = getNumOfSampleTrees(spc.getSpcID());

		// Get the k- and m-factor; 070418 p�d
		fFactor_k = 0.0;
		fFactor_m = 0.0;
		getKMFactors(nNumOfSampleTreesPerSpecie,fSumLogDBH,fSumHgt,fSumLogDBH2,fSumLogDBH_Hgt,&fFactor_k,&fFactor_m);
	}

	// Calculate height per tree m_vecDCLSTrees i.e. diameterclasses; 070820 p�d
	// Calculate height per tree (excluding sampletrees) in m_vecSampleTrees; 070418 p�d
	for (nTreeCnt = 0;nTreeCnt < m_nNumOfDCLSTrees;nTreeCnt++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[nTreeCnt];
		if (tree.getNumOfRandTrees() != -999)
		{
			// Make sure specie is ok and that there's not already a
			// height specified (if height IT'S A SAMPLE TREE); 080122 p�d
			if (tree.getSpcID() == spc.getSpcID())
			{
				
				// #4530 20151006 J�
				if (m_nNumOfSampleTrees != 0) // #4530 20151006 J�
				{
					fHgt = (fFactor_k * log(getClassMid(tree)/10.0) + fFactor_m);
					// A tree can only be this tall; 070418 p�d
					if (fHgt > MAX_HGT_IN_REGRESSION)
						fHgt = MAX_HGT_IN_REGRESSION;
					if (fHgt < 0.0)
						fHgt = 0.0;
				}
				else
					fHgt = 0.0;
				// Add height to m_vecSampleTrees
				m_vecDCLSTrees[nTreeCnt].setHgt(fHgt*10.0);	// From (dm) to (mm)
			}	// if (tree.getSpcID() == spc.getSpcID() && tree.getHgt() == 0.0)
		}	// if (tree.getNumOfRandTrees() != -999)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	return TRUE;
}

BOOL CHeightRegression::setH25ForSpecie(void)
{
	long nTreeCnt;
	double fDBH;
	double fHgt;
	double fSumLogDBH;
	double fSumHgt;
	double fSumLogDBH2;
	double fSumLogDBH_Hgt;
	double fFactor_k;
	double fFactor_m;

	long nNumOfSampleTreesPerSpecie;

	// Make sure there's any trees; 070417 p�d
	//if (m_nNumOfSampleTrees == 0 || m_nNumOfSampleSpecies == 0)
	if (m_nNumOfSampleSpecies == 0)		// #4530 20151006 J�
		return FALSE;

	// Get information on active specie in m_vecSetSpc; 070418 p�d
	CTransaction_trakt_set_spc spc = m_vecSetSpc[m_nSpcIndex];

	// Make sure the specie in CTransaction_trakt_set_spc also
	// is in m_vecSampleTrees. If not, just get the hell out of this method; 070525 p�d
	if (!isSpecieInTrees(spc.getSpcID()))
	{
		// Also, reset som values; HGV to 0.0. 
		// Just to confirm that there's no trees for this specie; 070525 p�d
		setTraktData_h25(spc.getSpcID(),0.0);
		return FALSE;
	}

	fDBH	= 0.0;
	fHgt = 0.0;
	fSumLogDBH = 0.0;
	fSumHgt = 0.0;
	fSumLogDBH2 = 0.0;
	fSumLogDBH_Hgt = 0.0;
	// Loop m_vecSampleTrees to calculate Height*Diam*Diam and Diam*Diam per Specie; 070418 p�d
	if (m_nNumOfSampleTrees != 0)// #4530 20151006 J�
	{
		for (nTreeCnt = 0;nTreeCnt < m_nNumOfSampleTrees;nTreeCnt++)
		{
			CTransaction_sample_tree tree = m_vecSampleTrees[nTreeCnt];
			if (tree.getSpcID() == spc.getSpcID())
			{
				fDBH			= tree.getDBH()/10.0;
				fHgt			= tree.getHgt()/10.0;
				fSumLogDBH	+= log(fDBH);
				fSumHgt			+= fHgt;
				fSumLogDBH2	+= pow(log(fDBH),2);
				fSumLogDBH_Hgt += (log(fDBH)*fHgt);
			}	// if (tree.getTypeOfTree() == SAMPLE_TREE && tree.getSpcID() == spc.getSpcID())
		}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

		// Get number of sampletrees for specie; 070418 p�d
		nNumOfSampleTreesPerSpecie = getNumOfSampleTrees(spc.getSpcID());

		// Get the k- and m-factor; 070418 p�d
		fFactor_k = 0.0;
		fFactor_m = 0.0;
		getKMFactors(nNumOfSampleTreesPerSpecie,fSumLogDBH,fSumHgt,fSumLogDBH2,fSumLogDBH_Hgt,&fFactor_k,&fFactor_m);

		// Calculate height for H25 tree (DBH = 250.0 mm); 070525 p�d
		fHgt = (fFactor_k * log(250.0/10.0) + fFactor_m);
		// A tree can only be this tall; 070418 p�d
		if (fHgt > MAX_HGT_IN_REGRESSION)
			fHgt = MAX_HGT_IN_REGRESSION;
	}
	else
		fHgt = 0.0;// #4530 20151006 J�
	setTraktData_h25(spc.getSpcID(),fHgt);

	return TRUE;
}

BOOL CHeightRegression::setHGVForSpecie(void)
{
	CString S;
	CTransaction_sample_tree treeHGV;
	long nTreeCnt;
	double fDBH;
	double fHgt;
	double fSumLogDBH;
	double fSumHgt;
	double fSumLogDBH2;
	double fSumLogDBH_Hgt;
	double fFactor_k;
	double fFactor_m;
	double fDGV;
	long nNumOfSampleTreesPerSpecie;

	// Make sure there's any trees; 070417 p�d
	//if (m_nNumOfSampleTrees == 0 || m_nNumOfSampleSpecies == 0)
	if (m_nNumOfSampleSpecies == 0)	// #4530 20151006 J�
		return FALSE;

	// Get information on active specie in m_vecSetSpc; 070418 p�d
	CTransaction_trakt_set_spc spc = m_vecSetSpc[m_nSpcIndex];

	// Make sure the specie in CTransaction_trakt_set_spc also
	// is in m_vecSampleTrees. If not, just get the hell out of this method; 070525 p�d
	if (!isSpecieInTrees(spc.getSpcID()))
	{
		// Also, reset som values; HGV to 0.0. 
		// Just to confirm that there's no trees for this specie; 070525 p�d
		setTraktData_hgv(spc.getSpcID(),0.0);
		return FALSE;
	}

	// Get calculated DGV value (SetTraktData()); 070525 p�d
	if (getTraktData(spc.getSpcID()) != NULL)
		fDGV = getTraktData(spc.getSpcID())->getDGV();
	else
		fDGV = 0.0;
	// Check that there's a fDGV ta calculate from; 081114 p�d
	if (fDGV == 0.0)
		return FALSE;
	// Check that there's data in trees and
	// use this inforamtion to setup a tree to be used
	// in HGV calculation; 070525 p�d
	if (m_vecSampleTrees.size() > 0)
	{
		treeHGV = CTransaction_sample_tree(m_vecSampleTrees[0]);
		treeHGV.setDBH(fDGV);
	}

	fDBH	= 0.0;
	fHgt = 0.0;
	fSumLogDBH = 0.0;
	fSumHgt = 0.0;
	fSumLogDBH2 = 0.0;
	fSumLogDBH_Hgt = 0.0;
	// Loop m_vecSampleTrees to calculate Height*Diam*Diam and Diam*Diam per Specie; 070418 p�d
	if (m_nNumOfSampleTrees != 0)	// #4530 20151006 J�
	{
		for (nTreeCnt = 0;nTreeCnt < m_nNumOfSampleTrees;nTreeCnt++)
		{
			CTransaction_sample_tree tree = m_vecSampleTrees[nTreeCnt];
			if (tree.getSpcID() == spc.getSpcID())
			{
				fDBH			= tree.getDBH()/10.0;
				fHgt			= tree.getHgt()/10.0;
				fSumLogDBH	+= log(fDBH);
				fSumHgt			+= fHgt;
				fSumLogDBH2	+= pow(log(fDBH),2);
				fSumLogDBH_Hgt += (log(fDBH)*fHgt);
			}	// if (tree.getTypeOfTree() == SAMPLE_TREE && tree.getSpcID() == spc.getSpcID())
		}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

		// Get number of sampletrees for specie; 070418 p�d
		nNumOfSampleTreesPerSpecie = getNumOfSampleTrees(spc.getSpcID());

		// Get the k- and m-factor; 070418 p�d
		fFactor_k = 0.0;
		fFactor_m = 0.0;
		getKMFactors(nNumOfSampleTreesPerSpecie,fSumLogDBH,fSumHgt,fSumLogDBH2,fSumLogDBH_Hgt,&fFactor_k,&fFactor_m);

		// Calculate HGV for DGV tree; 070525 p�d
		fHgt = (fFactor_k * log(treeHGV.getDBH()) + fFactor_m);
		// A tree can only be this tall; 070418 p�d
		if (fHgt > MAX_HGT_IN_REGRESSION)
			fHgt = MAX_HGT_IN_REGRESSION;
	}
	else
		fHgt = 0.0;// #4530 20151006 J�
	setTraktData_hgv(spc.getSpcID(),fHgt);

	return TRUE;
}

void CHeightRegression::getKMFactors(int cnt_spc,double log_dbh,double hgt,double dbh2,double log_dbh_h,double *k_factor,double *m_factor)
{
  int nNumOfSpc  = cnt_spc;
  double fLogDBH = log_dbh;
  double fHgt    = hgt;
  double fDBH2   = dbh2;
  double fLogDBH_Hgt = log_dbh_h;
  double fDiv  = 0.0;

  *k_factor = 0.0;
  *m_factor = 0.0;
	if (nNumOfSpc > 1)
  {
		// Calculate the K-factor and M-factor; 070418 p�d
    fDiv = (fLogDBH*fLogDBH - nNumOfSpc*fDBH2);
    if (fDiv != 0.0)
    {
			*k_factor = ((fLogDBH*fHgt) - (nNumOfSpc*fLogDBH_Hgt))/fDiv;
      *m_factor = ((fLogDBH_Hgt*fLogDBH) - (fHgt*fDBH2))/fDiv;
    } // if (div != 0.0)
  } // if (nNumOfSpc > 0.0)
}


// PUBLIC
CHeightRegression::CHeightRegression(void)
	: CCalculationBaseClass()
{
}

CHeightRegression::CHeightRegression(int spc_index,CTransaction_trakt rec1,
																									 CTransaction_trakt_misc_data rec2,
																									 vecTransactionTraktData &tdata_list,
																									 vecTransactionSampleTree &sample_tree_list,
																									 vecTransactionDCLSTree &dcls_tree_list,
																									 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}

BOOL CHeightRegression::calculate(vecTransactionTraktData& vec1,
																	vecTransactionSampleTree& vec2,
																	vecTransactionDCLSTree& vec4,
																	vecTransactionTraktSetSpc& vec3)
{
	CString S;
	int nSpcID = -1;
	int nNumOfSampleTrees = 0;
	CString sMsg;
	CString sResStr;

	// Make sure we have species; 070417 p�d
	if (m_nNumOfSampleSpecies == 0) 
		return FALSE;

	nSpcID = getSpecieID();	
	// Check if specie's used; 081114 p�d
	if (!isSpecieInTrees(nSpcID))
		return FALSE;

	nNumOfSampleTrees = getNumOfSampleTrees(nSpcID);

	if (nNumOfSampleTrees < MIN_NUMOF_SAMPLETREES_REGRESSION)
	{
		// Setup message, tellin' user about the bad new. Not enough sampletrees; 070625 p�d

		sMsg.Format(_T("<FONT SIZE='10'><B>%s</B></FONT><BR><BR><BR>%s <B>%s</B><BR><BR>%s <B>%d</B><BR><BR>%s<BR><BR><b>%s</b><BR><BR>%s"),
			getResStr(IDS_STRING302),
			getResStr(IDS_STRING303),
			getSpecieName(nSpcID),
			getResStr(IDS_STRING304),
			nNumOfSampleTrees,
			getResStr(IDS_STRING305),
			getResStr(IDS_STRING306),
			getResStr(IDS_STRING307));

		CMessageDialog *msg = new CMessageDialog(getResStr(IDS_STRING300),
																						 getResStr(IDS_STRING301),
																						 sMsg);
		if (msg != NULL)
		{
			msg->DoModal();
			delete msg;
		}
		//return FALSE;	// Not enough sampletrees
	}

	//===========================================================================
	// Calcualte heights for each tree in m_vecSampleTrees (except sampletrees); 070418 p�d
	setHeightForTrees();
	setH25ForSpecie();
	setHGVForSpecie();


	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}
