/* 2007-04-13 Per-�ke Danielsson */
#if !defined(AFX_VOLUME_M3SK_M3UB_H)
#define AFX_VOLUME_M3SK_M3UB_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

////////////////////////////////////////////////////////////////////////////////////////
// CVolume_m3sk_m3ub

class CVolume_m3sk_m3ub : public CCalculationBaseClass
{
//private:

protected:

public:
	CVolume_m3sk_m3ub(void);
	CVolume_m3sk_m3ub(int spc_index,CTransaction_trakt rec1,
															 CTransaction_trakt_misc_data rec2,
															 vecTransactionTraktData &tdata_list,
															 vecTransactionSampleTree &sample_tree_list,
															 vecTransactionDCLSTree &dcls_tree_list,
															 vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);
};

#endif