#if !defined(AFX_GROTCLACULATIONS_H)
#define AFX_GROTCLACULATIONS_H

/*
***********************************************************************
*		Ber�kningar f�r GROT �r anpassade enligt rekomendationer
*		framtagna i rapport nr. 677 och 694 Skogforsk 2009
************************************************************************
*/


class CGrotCalc
{
	//private
	CTransaction_trakt m_recTrakt;
	vecTransactionTraktData m_vecTraktData;
	vecTransactionTraktSetSpc m_vecSpcList;	// Functions to use
	vecTransactionDCLSTree m_vecDCLSTrees;

	std::map<int,double> m_mapSumGrotPerSpc;

	// Datamembers for calculation
	double m_fDBH;
	double m_fHgt;
	double m_fHgtOverSea;
	double m_fBrg;
	double m_fNCoord;
	int m_nH100;
	double m_fKR_length;
	double m_fGrotPercent;
	int m_nNumOfTrees;
	int m_nActiveSpc;
	short m_nSI_PineOrSpruce;	// 1 = Pine 2 = Spruce

	int getConversH100Value(short id,short h100);
	double getKR_length(void);

	inline double getClassMid(CTransaction_dcls_tree& v)
	{
		double fDCLS = (v.getDCLS_to() - v.getDCLS_from());
		double fMid = fDCLS / 2.0;
		return (v.getDCLS_from() + fMid)*10.0;	// From (cm) to (mm)
	}

	int getFunctionID(void);				// Get ID for function  to use; 100311 p�d
	double getPercent(void);				// Get 'Uttagsprocent'; 100311 p�d

protected:
	// MARKLUND
	// Calc Pine 
	double T_02(void);
	double T_14(void);
	double T_15(void);
	double T_18(void);
	double T_19(void);
	double T_22(void);
	double T_23(void);
	double T_28(void);
	double T_29(void);
	double T_31(void);
	double T_32(void);
	// Calc Spruce
	double G_02(void);
	double G_12(void);
	double G_13(void);
	double G_16(void);
	double G_17(void);
	double G_20(void);
	double G_21(void);
	double G_26(void);
	double G_27(void);
	double G_28(void);
	double G_29(void);
	// Calc Birch
	double B_02(void);
	double B_12(void);
	double B_13(void);
	double B_16(void);
	double B_17(void);
	// REPOLA
	// Pine
	double T_Stemawood(void);
	double T_Steambark(void);
	double T_LivinBranches(void);
	double T_Needles(void);
	double T_DeadBranches(void);
	double T_Stump(void);
	double T_Roots_GT_1CM(void);
	// Spruce
	double G_Steamwood(void);
	double G_Steambark(void);
	double G_LivinBranches(void);
	double G_Needles(void);
	double G_DeadBranches(void);
	double G_Stump(void);
	double G_Roots_GT_1CM(void);
	// Birch
	double B_Steamwood(void);
	double B_Steambark(void);
	double B_LivinBranches(void);
	double B_Leafs(void);
	double B_DeadBranches(void);
	double B_Stump(void);
	double B_Roots_GT_1CM(void);

public:
	CGrotCalc(void);
	CGrotCalc(CTransaction_trakt rec,vecTransactionTraktData &trakt_data,vecTransactionTraktSetSpc &spc_list,vecTransactionDCLSTree &dcls_tree_list);
	virtual ~CGrotCalc(void);

	BOOL DoGrotCalc();

	inline vecTransactionDCLSTree &getDCLSTrees(void)	{ return m_vecDCLSTrees; }
	inline vecTransactionTraktData &getTraktData(void)	{ return m_vecTraktData; }

};


#endif