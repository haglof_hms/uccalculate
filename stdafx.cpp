// stdafx.cpp : source file that includes just the standard includes
// UCCalculate.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#include <shlwapi.h>

CString getResStr(int id)
{
	CString S;
	S.LoadString(id);

	return S;
}

int SplitString(const CString& input, 
  const CString& delimiter, CStringArray& results)
{
  int iPos = 0;
  int newPos = -1;
  int sizeS2 = delimiter.GetLength();
  int isize = input.GetLength();

  CArray<INT, int> positions;

  newPos = input.Find (delimiter, 0);

  if( newPos < 0 ) { return 0; }

  int numFound = 0;

  while( newPos > iPos )
  {
    numFound++;
    positions.Add(newPos);
    iPos = newPos;
    newPos = input.Find (delimiter, iPos+sizeS2+1);
  }

  for( int i=0; i <= positions.GetSize(); i++ )
  {
    CString s;
    if( i == 0 )
      s = input.Mid( i, positions[i] );
    else
    {
      int offset = positions[i-1] + sizeS2;
      if( offset < isize )
      {
        if( i == positions.GetSize() )
          s = input.Mid(offset);
        else if( i > 0 )
          s = input.Mid( positions[i-1] + sizeS2, 
                 positions[i] - positions[i-1] - sizeS2 );
      }
    }
    if( s.GetLength() > 0 )
      results.Add(s);
  }
  return numFound;
}


BOOL existsFile(LPCTSTR fn)
{
	BOOL bFound = FALSE;
	CFileFind find;
	bFound = find.FindFile(fn);
	find.Close();
	return bFound;
}

CString getLangSetInReg()
{
	HKEY hk;
	TCHAR strValue[127];
	TCHAR szRoot[127];
	DWORD dwLength = 126;	// Size of the value

	strValue[0] = '\0';

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),REG_ROOT,REG_LANG_KEY);
	if (RegOpenKeyEx(HKEY_CURRENT_USER,
				  szRoot,
				  0,
				  KEY_QUERY_VALUE,
				  &hk) == ERROR_SUCCESS)
	{
		// Retrieve the value of the Left key
		RegQueryValueEx(hk,REG_LANG_ITEM,	NULL,NULL,(LPBYTE)strValue,&dwLength);
	}

	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return strValue;
}

CString getLanguageDirInReg(void)
{
	CString sPath;
	CString sModulesPath;
#ifdef UNICODE
    TCHAR szPath[_MAX_PATH]; 
	::GetModuleFileName(AfxGetApp()->m_hInstance, szPath, _MAX_PATH);	// AfxGetApp() fungerar inte n�r man mixar olika versioner av MFC!
	sPath = szPath;
#else
	VERIFY(::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
	sPath.ReleaseBuffer();
#endif

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

#ifdef UNICODE1
	sModulesPath.Format(_T("%S%S\\"),sPath,SUBDIR_LANGUAGE);
#else
	sModulesPath.Format(_T("%s%s\\"),sPath,SUBDIR_LANGUAGE);
#endif

	return sModulesPath;
}

CString getCalcLangFile(void)
{
	CString sLangFN;
	sLangFN.Format(_T("%s%s%s%s"),getLanguageDirInReg(),NAMN_OF_MODULE,getLangSetInReg(),LANGUAGE_FN_EXT);

	return sLangFN;
}

CString getPathToDatafile(CString fn)
{
	CString sPath;
	CString sModulesPath;
	VERIFY(::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s"), sPath, fn);

	return sModulesPath;
}

// Parser to read alnguage files for UCCalculateXXX.xml; 090603 p�d
BOOL readLangFile_functions(LPCTSTR xml_fn,LPCTSTR tag_type,vecUCFunctions &func)
{
	CString S;
	CComPtr<IStream> pFileStream;
	CComPtr<IXmlReader> pReader;
	DWORD id = 0;
	HRESULT hr;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszFuncName;
	const WCHAR* pwszFuncID;
	UINT cwchPrefix;
	XmlNodeType nodeType;

	BOOL bFound1 = FALSE;
	BOOL bFound2 = FALSE;
	BOOL bFound3 = FALSE;
	// Set up xml reader
	if( FAILED(SHCreateStreamOnFile(xml_fn, STGM_READ, &pFileStream)) )
	{
		return FALSE;
	}
	if( FAILED(CreateXmlReader(__uuidof(IXmlReader), (void**)&pReader, NULL)) )
	{
		return FALSE;
	}
	if( FAILED(pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit)) )
	{
		return FALSE;
	}
	if( FAILED(pReader->SetInput(pFileStream)) )
	{
		return FALSE;
	}

	// Read off xml file
	while((hr = pReader->Read(&nodeType)) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{			
			if(pReader->GetLocalName(&pwszLocalName, NULL) == S_OK )
			{
				if (wcscmp(pwszLocalName,tag_type) == 0)
				{
					if (pReader->MoveToAttributeByName(LANG_READ_FUNC_ID,NULL) == S_OK)
					{
						pReader->GetValue(&pwszFuncID, NULL);
					}	// if (pReader->MoveToAttributeByName(LANG_READ_FUNC,NULL) == S_OK)
					if (pReader->MoveToAttributeByName(LANG_READ_FUNC_NAME,NULL) == S_OK)
					{
						pReader->GetValue(&pwszFuncName, NULL);
					}	// if (pReader->MoveToAttributeByName(LANG_READ_FUNC,NULL) == S_OK)

					func.push_back(UCFunctions(_tstoi(pwszFuncID),pwszFuncName));

				}	// if (wcscmp(pwszLocalName,LANG_READ_FUNC_VOLUME) == 0)
			}	// if(pReader->GetLocalName(&pwszLocalName, NULL) == S_OK )
		}
	}

	return TRUE;
}

BOOL readLangFile_function_list(LPCTSTR xml_fn,LPCTSTR main_tag,LPCTSTR tag,vecUCFunctionList &func_list)
{
	CString S;
	CComPtr<IStream> pFileStream;
	CComPtr<IXmlReader> pReader;
	DWORD id = 0;
	HRESULT hr;
	const WCHAR* pwszLocalName1;
	const WCHAR* pwszLocalName2;
	const WCHAR* pwszFuncID;
	const WCHAR* pwszID;
	const WCHAR* pwszSubID;
	const WCHAR* pwszSpcName;
	const WCHAR* pwszFuncType1;
	const WCHAR* pwszFuncType2;
	const WCHAR* pwszDesc;
	XmlNodeType nodeType;
	int nFuncID = 0;

	// Set up xml reader
	if( FAILED(SHCreateStreamOnFile(xml_fn, STGM_READ, &pFileStream)) )
	{
		return FALSE;
	}
	if( FAILED(CreateXmlReader(__uuidof(IXmlReader), (void**)&pReader, NULL)) )
	{
		return FALSE;
	}
	if( FAILED(pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit)) )
	{
		return FALSE;
	}
	if( FAILED(pReader->SetInput(pFileStream)) )
	{
		return FALSE;
	}

	// Read off xml file
	while((hr = pReader->Read(&nodeType)) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{			
			if(pReader->GetLocalName(&pwszLocalName1, NULL) == S_OK )
			{
				if (wcscmp(pwszLocalName1,main_tag) == 0)
				{
					if (pReader->MoveToAttributeByName(LANG_READ_FUNC_ID,NULL) == S_OK)
					{
						pReader->GetValue(&pwszFuncID, NULL);
						nFuncID = _tstoi(pwszFuncID);
					}	// if (pReader->MoveToAttributeByName(LANG_READ_FUNC,NULL) == S_OK)
				}	// if (wcscmp(pwszLocalName,LANG_READ_FUNC_VOLUME) == 0)
			}	// if(pReader->GetLocalName(&pwszLocalName, NULL) == S_OK )

			if(pReader->GetLocalName(&pwszLocalName2, NULL) == S_OK )
			{
				if (wcscmp(pwszLocalName2,tag) == 0)
				{
					if (pReader->MoveToAttributeByName(LANG_READ_FUNC_ATTR_ID,NULL) == S_OK)
					{
						pReader->GetValue(&pwszID, NULL);
					}	// if (pReader->MoveToAttributeByName(LANG_READ_FUNC,NULL) == S_OK)

					if (pReader->MoveToAttributeByName(LANG_READ_FUNC_ATTR_SUB_ID,NULL) == S_OK)
					{
						pReader->GetValue(&pwszSubID, NULL);
					}	// if (pReader->MoveToAttributeByName(LANG_READ_FUNC,NULL) == S_OK)

					if (pReader->MoveToAttributeByName(LANG_READ_FUNC_ATTR_SPC_NAME,NULL) == S_OK)
					{
						pReader->GetValue(&pwszSpcName, NULL);
					}	// if (pReader->MoveToAttributeByName(LANG_READ_FUNC,NULL) == S_OK)

					if (pReader->MoveToAttributeByName(LANG_READ_FUNC_ATTR_FUNC_TYPE1,NULL) == S_OK)
					{
						pReader->GetValue(&pwszFuncType1, NULL);
					}	// if (pReader->MoveToAttributeByName(LANG_READ_FUNC,NULL) == S_OK)


					if (pReader->MoveToAttributeByName(LANG_READ_FUNC_ATTR_FUNC_TYPE2,NULL) == S_OK)
					{
						pReader->GetValue(&pwszFuncType2, NULL);
					}	// if (pReader->MoveToAttributeByName(LANG_READ_FUNC,NULL) == S_OK)


					if (pReader->MoveToAttributeByName(LANG_READ_FUNC_ATTR_FUNC_DESC,NULL) == S_OK)
					{
						pReader->GetValue(&pwszDesc, NULL);
					}	// if (pReader->MoveToAttributeByName(LANG_READ_FUNC,NULL) == S_OK)

/*
					S.Format(_T("FuncID %d\nID %s\nSubID %s\nSpcName %s\nFT1 %s\nFT2 %s\nDesc %s"),
							nFuncID,pwszID,pwszSubID,pwszSpcName,pwszFuncType1,pwszFuncType2,pwszDesc);
					AfxMessageBox(S);
*/	
					func_list.push_back(UCFunctionList(nFuncID,_tstoi(pwszID),_tstoi(pwszSubID),pwszSpcName,pwszFuncType1,pwszFuncType2,pwszDesc));
				
				}	// if (wcscmp(pwszLocalName2,LANG_READ_FUNC_VOLUME) == 0)
			}	// if(pReader->GetLocalName(&pwszLocalName, NULL) == S_OK )

		}	// if( nodeType == XmlNodeType_Element )
	}	// while((hr = pReader->Read(&nodeType)) == S_OK )

	return TRUE;
}
