/*	2009-06-03 P�D 
		Calculate volume for CZ based on Tables 
*/

#include "StdAfx.h"

#include "VolumeCZ.h"

// Defines for columns per specie; 090604 p�d
#define COLUMNS_TBL1		35		// "Gran (Picea)"
#define COLUMNS_TBL2		31		// "Gran (Abies) 41-80 �r"
#define COLUMNS_TBL3		33		// "Gran (Picea) 81-120 �r"
#define COLUMNS_TBL4		36		// "Gran (Picea) 120+ �r"
#define COLUMNS_TBL5		31		// "Tall 41-80 �r"
#define COLUMNS_TBL6		33		// "Tall 81+ �r"
#define COLUMNS_TBL7		36		// "L�rktr�d"
#define COLUMNS_TBL8		36		// "Ek"
#define COLUMNS_TBL9		34		// "Bok"
#define COLUMNS_TBL10		23		// "Averbok"
#define COLUMNS_TBL11		31		// "Ask"
#define COLUMNS_TBL12		26		// "Robina"
#define COLUMNS_TBL13		24		// "Bj�rk"
#define COLUMNS_TBL14		25		// "Al"

// Name of czech volume-tables; 090604 p�d
static LPCTSTR CZ_VOLUME_SPRUCE1		= _T("CZ_Volume_Spruce1");
static LPCTSTR CZ_VOLUME_FIR1				= _T("CZ_Volume_Fir41_80");
static LPCTSTR CZ_VOLUME_FIR2				= _T("CZ_Volume_Fir81_120");
static LPCTSTR CZ_VOLUME_FIR3				= _T("CZ_Volume_Fir120+");
static LPCTSTR CZ_VOLUME_PINE1			= _T("CZ_Volume_Pine41_80");
static LPCTSTR CZ_VOLUME_PINE2			= _T("CZ_Volume_Pine81+");
static LPCTSTR CZ_VOLUME_LARCH			= _T("CZ_Volume_Larch");
static LPCTSTR CZ_VOLUME_OAK				= _T("CZ_Volume_Oak");
static LPCTSTR CZ_VOLUME_BEECH			= _T("CZ_Volume_Beech");
static LPCTSTR CZ_VOLUME_HORNBEAM		= _T("CZ_Volume_HornBeam");
static LPCTSTR CZ_VOLUME_ASH				= _T("CZ_Volume_Ash");
static LPCTSTR CZ_VOLUME_ROBINA			= _T("CZ_Volume_Robina");
static LPCTSTR CZ_VOLUME_BIRCH			= _T("CZ_Volume_Birch");
static LPCTSTR CZ_VOLUME_ALDER			= _T("CZ_Volume_Alder");

struct _header
{
	char worksheet[30];
	int columns;
	int rows;
};

// PUBLIC:
CVolumeCZ::CVolumeCZ(void)
	: CCalculationBaseClass()
{
}

CVolumeCZ::CVolumeCZ(int spc_index,CTransaction_trakt rec1,
																						 CTransaction_trakt_misc_data rec2,
																						 vecTransactionTraktData &tdata_list,
																						 vecTransactionSampleTree &sample_tree_list,
																						 vecTransactionDCLSTree &dcls_tree_list,
																						 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}

// PROTECTED

void CVolumeCZ::getTableVolume_Spruce(double dbh,double hgt,double *volume)
{
	_header head;
	double fRowData[COLUMNS_TBL1+1],fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	TCHAR szFN[MAX_PATH];

	_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_SPRUCE1));

	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		UMMessageBox(_T("SPRUCE\nCould not open file"));
		return;
	}

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < COLUMNS_TBL1+1;i++) fRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(&fRowData,sizeof(fRowData),1,f);

	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < fRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= fRowData[col-1] && hgt <= fRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}

	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(&fRowData,sizeof(fRowData),1,f);
	fLastDBH = fRowData[0];
	fLastVolume = fRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}

	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < COLUMNS_TBL1+1;i++) fRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(&fRowData,sizeof(fRowData),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (fRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= fRowData[0]) 
			{
				fVolume = fRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = fRowData[0];
			fLastVolume = fRowData[nColumn];
		}
	}

	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	*volume = fVolume;
}


void CVolumeCZ::getTableVolume_Fir(double dbh,double hgt,int age,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = 0;
	TCHAR szFN[MAX_PATH];


	//**************************************************************************
	// Check age to see which table to open; 090603 p�d
	if (age >= 41 && age <= 80)
	{
		_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_FIR1));
		if ((f = _tfopen(szFN,_T("rb"))) == NULL)
		{
			UMMessageBox(_T("FIR\nCould not open file"));
			return;
		}
		nTableSize = COLUMNS_TBL2;
	}
	else if (age >= 81 && age <= 120)
	{
		_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_FIR2));
		if ((f = _tfopen(szFN,_T("rb"))) == NULL)
		{
			UMMessageBox(_T("FIR\nCould not open file"));
			return;
		}
		nTableSize = COLUMNS_TBL3;
	}
	else if (age > 120)
	{
		_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_FIR3));
		if ((f = _tfopen(szFN,_T("rb"))) == NULL)
		{
			UMMessageBox(_T("FIR\nCould not open file"));
			return;
		}
		nTableSize = COLUMNS_TBL4;
	}
	else
	{
		*volume = 0.0;	
		return;
	}
	//**************************************************************************

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);
	
	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;

}

void CVolumeCZ::getTableVolume_Pine(double dbh,double hgt,int age,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = 0;
	TCHAR szFN[MAX_PATH];

	//**************************************************************************
	// Check age to see which table to open; 090603 p�d
	if (age >= 41 && age <= 80)
	{
		_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_PINE1));
		if ((f = _tfopen(szFN,_T("rb"))) == NULL)
		{
			UMMessageBox(_T("PINE\nCould not open file"));
			return;
		}
		nTableSize = COLUMNS_TBL5;
	}
	else if (age > 80)
	{
		_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_PINE2));
		if ((f = _tfopen(szFN,_T("rb"))) == NULL)
		{
			UMMessageBox(_T("PINE\nCould not open file"));
			return;
		}
		nTableSize = COLUMNS_TBL6;
	}
	else
	{
		*volume = 0.0;	
		return;
	}
	//**************************************************************************

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;

}

void CVolumeCZ::getTableVolume_Larch(double dbh,double hgt,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = COLUMNS_TBL7;
	TCHAR szFN[MAX_PATH];

	_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_LARCH));
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		UMMessageBox(_T("LARCH\nCould not open file"));
		return;
	}

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;
}

void CVolumeCZ::getTableVolume_Oak(double dbh,double hgt,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = COLUMNS_TBL8;
	TCHAR szFN[MAX_PATH];

	_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_OAK));
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		UMMessageBox(_T("OAK\nCould not open file"));
		return;
	}

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;
}

void CVolumeCZ::getTableVolume_Beech(double dbh,double hgt,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = COLUMNS_TBL9;
	TCHAR szFN[MAX_PATH];

	_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_BEECH));
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		UMMessageBox(_T("BEECH\nCould not open file"));
		return;
	}

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;
}

void CVolumeCZ::getTableVolume_HornBeam(double dbh,double hgt,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = COLUMNS_TBL10;
	TCHAR szFN[MAX_PATH];

	_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_HORNBEAM));
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		UMMessageBox(_T("HORNBEAM\nCould not open file"));
		return;
	}

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;
}


void CVolumeCZ::getTableVolume_Ash(double dbh,double hgt,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = COLUMNS_TBL11;
	TCHAR szFN[MAX_PATH];

	_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_ASH));
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		UMMessageBox(_T("ASH\nCould not open file"));
		return;
	}

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;
}


void CVolumeCZ::getTableVolume_Robina(double dbh,double hgt,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = COLUMNS_TBL12;
	TCHAR szFN[MAX_PATH];

	_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_ROBINA));
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		UMMessageBox(_T("ROBINA\nCould not open file"));
		return;
	}

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;
}

void CVolumeCZ::getTableVolume_Birch(double dbh,double hgt,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = COLUMNS_TBL13;
	TCHAR szFN[MAX_PATH];

	_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_BIRCH));
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		UMMessageBox(_T("BIRCH\nCould not open file"));
		return;
	}

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;
}

void CVolumeCZ::getTableVolume_Alder(double dbh,double hgt,double *volume)
{
	_header head;
	double *pRowData,fVolume = 0.0,fLastDBH = 0.0,fLastVolume = 0.0;
	short nColumn;
	BOOL bFoundColumn = FALSE;
	BOOL bFoundDBH = FALSE;
	FILE *f;
	int nTableSize = COLUMNS_TBL14;
	TCHAR szFN[MAX_PATH];

	_stprintf(szFN,_T("%s"),getPathToDatafile(CZ_VOLUME_ALDER));
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		UMMessageBox(_T("ALDER\nCould not open file"));
		return;
	}

	pRowData = new double[nTableSize+1];

	// Read header; 090430 p�d
	fread(&head,sizeof(_header),1,f);

	// Clear
	for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;
	//**************************************************************************
	// Read RowData to first row = Heights; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	// We'll try to match hgt to column; 090430 p�d
	for (int col = 1;col < head.columns;col++)
	{
		if (hgt < pRowData[col-1]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
		else if (hgt >= pRowData[col-1] && hgt <= pRowData[col]) 
		{ 
			nColumn = col; 
			bFoundColumn = TRUE;
			break; 
		}
	}
	// Height more than max height in table, set to last height; 090430 p�d
	if (!bFoundColumn) nColumn = head.columns; 
	//**************************************************************************

	//**************************************************************************
	// We'll try to match dbh to row; 090430 p�d
	// Read row to get first DBH; 090430 p�d
	fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
	fLastDBH = pRowData[0];
	fLastVolume = pRowData[nColumn];

	// Check if DBH is less than smallest DBH in table; 090430 p�d
	if (dbh < fLastDBH) 
	{
		fVolume = fLastVolume;
		bFoundDBH = TRUE;
	}
	// Try to match DBH in table; 090430 p�d
	if (!bFoundDBH)
	{
		for (int row = 1;row < head.rows;row++)
		{
			// Clear
			for (int i = 0;i < nTableSize+1;i++) pRowData[i] = 0.0;

			// Read RowData; 090430 p�d
			fread(pRowData,sizeof(double)*(nTableSize+1),1,f);
			// Check that the next DBH in table is > than last DBH; 090430 p�d
			if (pRowData[0] < fLastDBH) 
				break;

			// 
			if (dbh >= fLastDBH && dbh <= pRowData[0]) 
			{
				fVolume = pRowData[nColumn];
				bFoundDBH = TRUE;
				break;
			}

			fLastDBH = pRowData[0];
			fLastVolume = pRowData[nColumn];
		}
	}
	// Still not found a match, we'll set volume to last volume.
	// I.g. DBH > Largset diamter in Table; 090430 p�d
	if (!bFoundDBH) fVolume = fLastVolume;
	//**************************************************************************
	fclose(f);

	delete[] pRowData;

	*volume = fVolume;
}


// PUBLIC

BOOL CVolumeCZ::calculate(vecTransactionTraktData &vec1,
															 vecTransactionSampleTree &vec2,
															 vecTransactionDCLSTree &vec4,		// Added 070820 p�d
															 vecTransactionTraktSetSpc& vec3)
{
	CString S;
	double fM3Sk = 0.0;
	double fM3Fub = 0.0;
	double fGreenCrown = 0.0;
	int nNumOfTrees = 0;	// Number of trees in Duameterclass
	int nVolSpcID = -1;
	int nVolIndex = -1;
	int nSpcID = -1;
	CString sHgt;
	double fHgtRounded;

	// Check that there's any species; 070418 p�d
	if (m_nNumOfDCLSSpecies == 0)
		return FALSE;

	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nVolSpcID = getVolumeSpcID();	
	nVolIndex = getVolumeIndex();
	// Loop through the tree list (m_vecSampleTrees) and
	// calculate volume for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getVolumeID() to get which species
	// method to use and getVolumeIndex() to get
	// the specific function withint the method,
	// declared in UCVolumeCZ; 070417 p�d
	for (long i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		if (tree.getSpcID() == nSpcID)
		{

			// Adaption for "Intr�ngsv�rdering":
			// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
			// OBS! This only allies to "Intr�ng"; 090604 p�d
			nNumOfTrees = tree.getNumOf() + tree.getNumOfRandTrees();
			// Get "Gr�nkroneprocent f�r tr�dslag"; 090604 p�d
			fGreenCrown = getGreenCrownPercForSpc(tree.getSpcID());

			// Round height; 0900604 p�d
			sHgt.Format(_T("%.0f"),tree.getHgt());
			fHgtRounded = _tstof(sHgt);

			switch (nVolSpcID)
			{
				case 1	: getTableVolume_Pine(getClassMid(tree)/10.0,fHgtRounded/10.0,getTrakt().getTraktAge(),&fM3Sk); break;
				case 2	: getTableVolume_Spruce(getClassMid(tree)/10.0,fHgtRounded/10.0,&fM3Sk); break;
				case 3	: getTableVolume_Birch(getClassMid(tree)/10.0,fHgtRounded/10.0,&fM3Sk); break;
				case 4	: getTableVolume_Oak(getClassMid(tree)/10.0,fHgtRounded/10.0,&fM3Sk); break;
				case 5	: getTableVolume_Ash(getClassMid(tree)/10.0,fHgtRounded/10.0,&fM3Sk); break;
				case 6	: getTableVolume_Beech(getClassMid(tree)/10.0,fHgtRounded/10.0,&fM3Sk); break;
				case 7	: getTableVolume_Alder(getClassMid(tree)/10.0,fHgtRounded/10.0,&fM3Sk); break;
				case 8	: getTableVolume_Larch(getClassMid(tree)/10.0,fHgtRounded/10.0,&fM3Sk); break;
				case 9	: getTableVolume_HornBeam(getClassMid(tree)/10.0,fHgtRounded/10.0,&fM3Sk); break;
				case 10	: getTableVolume_Fir(getClassMid(tree)/10.0,fHgtRounded/10.0,getTrakt().getTraktAge(),&fM3Sk); break;
				case 11	: getTableVolume_Robina(getClassMid(tree)/10.0,fHgtRounded/10.0,&fM3Sk); break;
			};

			if (nNumOfTrees > 0)
			{
				fM3Sk	*= nNumOfTrees;
			}
			
			m_vecDCLSTrees[i].setM3sk(fM3Sk);

			// Also calculate m3fub using "�verf�ringstal" for m3sk to m3fub
			// in esti_trakt_set_spc_table; 070524 p�d
			m_vecDCLSTrees[i].setM3fub(0.0);
			m_vecDCLSTrees[i].setGCrownPerc(fGreenCrown);

			// Also set Calculated "Gr�nkrona", from SampleTrees/Specie; 080625 p�d
			setTraktData_gcrown(nSpcID,fGreenCrown);

		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}