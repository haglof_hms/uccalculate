#include "StdAfx.h"

#include "VolumeBrandel.h"
//----------------------------------------------------------------------------
//	Volumefunctions from the book : "Volymfunktioner f�renskilda tr�d 
//	av G�ran Brandel"
//----------------------------------------------------------------------------
/*
*     TALL
*/

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels mindre"; 021202 p�d
//	Page: 110; func: 100-01
//  Pine_Simple_N[5]   = Pine (Tall) Norra	
//	Page: 104; func: 100-01
//  Pine_Simple_S[5]   = Pine (Tall) S�dra	
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
const double Pine_Simple_N[5]   = { -1.20914, 1.94740,-0.05947, 1.40958,-0.45810 };
const double Pine_Simple_S[5]   = { -1.38903, 1.84493, 0.06563, 2.02122,-1.01095 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 142 func: 100-01
//  Pine_BG_63_LT_N    =  Pine (Tall) + BG <63, Norra
//  Pine_BG_63_65_N    =  Pine (Tall) + BG >=63 ... <65, Norra
//  Pine_BG_65_67_N    =  Pine (Tall) + BG >=65 ... <67, Norra
//  Pine_BG_67_GT_N    =  Pine (Tall) + BG >=67, Norra
//	Page: 138 func: 100-01
//  Pine_BG_57_LT_S    =  Pine (Tall) + BG <57, S�dra						
//  Pine_BG_57_59_S    =  Pine (Tall) + BG >=57 ... <59, S�dra
//  Pine_BG_59_GT_S    =  Pine (Tall) + BG >=59, S�dra
//----------------------------------------------------------------------------
//                                      a        b        c        d         e
const double Pine_BG_63_LT_N[5]    = {-1.30052, 1.93867,-0.04966, 1.81528, -0.80910 };
const double Pine_BG_63_65_N[5]    = {-1.29068, 1.93867,-0.04966, 1.81528, -0.80910 };
const double Pine_BG_65_67_N[5]    = {-1.28297, 1.93867,-0.04966, 1.81528, -0.80910 };
const double Pine_BG_67_GT_N[5]    = {-1.28213, 1.93867,-0.04966, 1.81528, -0.80910 };

const double Pine_BG_57_LT_S[5]    = {-1.40718, 1.83182, 0.07275, 2.12777, -1.09439 };
const double Pine_BG_57_59_S[5]    = {-1.41955, 1.83182, 0.07275, 2.12777, -1.09439 };
const double Pine_BG_59_GT_S[5]    = {-1.41472, 1.83182, 0.07275, 2.12777, -1.09439 };


//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG+KG)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 142 func: 100-02
//  Pine_BG_KG_63_LT_N    =  Pine (Tall) + BG <63, Norra
//  Pine_BG_KG_63_65_N    =  Pine (Tall) + BG >=63 ... <65, Norra
//  Pine_BG_KG_65_67_N    =  Pine (Tall) + BG >=65 ... <67, Norra
//  Pine_BG_KG_67_GT_N    =  Pine (Tall) + BG >=67, Norra
//	Page: 138 func: 100-02
//  Pine_BG_KG_57_LT_S    =  Pine (Tall) + BG <57, S�dra
//  Pine_BG_KG_57_59_S    =  Pine (Tall) + BG >=57 ... <59, S�dra
//  Pine_BG_KG_59_GT_S    =  Pine (Tall) + BG >=59, S�dra
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
const double Pine_BG_KG_63_LT_N[6]  = {-1.22976, 1.99808,-0.11785, 1.92669, -0.99978, 0.05282 };
const double Pine_BG_KG_63_65_N[6]  = {-1.21943, 1.99808,-0.11785, 1.92669, -0.99978, 0.05282 };
const double Pine_BG_KG_65_67_N[6]  = {-1.21217, 1.99808,-0.11785, 1.92669, -0.99978, 0.05282 };
const double Pine_BG_KG_67_GT_N[6]  = {-1.20912, 1.99808,-0.11785, 1.92669, -0.99978, 0.05282 };
const double Pine_BG_KG_57_LT_S[6]  = {-1.37368, 1.85256, 0.05972, 2.04072, -1.06474, 0.03177 };
const double Pine_BG_KG_57_59_S[6]  = {-1.38584, 1.85256, 0.05972, 2.04072, -1.06474, 0.03177 };
const double Pine_BG_KG_59_GT_S[6]  = {-1.38066, 1.85256, 0.05972, 2.04072, -1.06474, 0.03177 };


//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (KG+BRK)"; 030829 p�d
//----------------------------------------------------------------------------
//	Page: 104 func: 100-04
//  Pine_KG_BRK_S    =  Pine (Tall) + BG + Barkred.
//----------------------------------------------------------------------------
//                                    a        b        c        d        e      f         g
const double Pine_KG_BRK_S[7]  = {-1.20042, 2.10263,-0.07366, 1.99751, -1.11357, 0.06420, -0.14963};

/*
*     GRAN
*/

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels mindre"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 112 func: 100-01
//  Spruce_Simple_N[5] = Spruce (Gran) Norra
//	Page: 106 func: 100-01
//  Spruce_Simple_S[5] = Spruce (Gran) S�dra
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
const double Spruce_Simple_N[5] = { -0.79783, 2.07157,-0.73882, 3.16332,-1.82622 };
const double Spruce_Simple_S[5] = { -1.02039, 2.00128,-0.47473, 2.87138,-1.61803 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (HH)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 137 func: 100-01
//  Spruce_HH_200_LT_N[5]  = Height over sea <200
//  Spruce_HH_200_500_N[5] = Height over sea >=200 ... <500
//  Spruce_HH_500_GT_N[5]  = Height over sea >=500
//----------------------------------------------------------------------------
//                                        a        b         c        d         e
const double Spruce_HH_200_LT_N[5]  = { -0.77559, 2.08947, -0.75125, 3.13586, -1.81658 };
const double Spruce_HH_200_500_N[5] = { -0.77897, 2.08947, -0.75125, 3.13586, -1.81658 };
const double Spruce_HH_500_GT_N[5]  = { -0.78963, 2.08947, -0.75125, 3.13586, -1.81658 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (HH+KG)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 137 func: 100-02
//  Spruce_HH_KG_200_LT_N[5]  = Height over sea <200
//  Spruce_HH_KG_200_500_N[5] = Height over sea >=200 ... <500
//  Spruce_HH_KG_500_GT_N[5]  = Height over sea >=500
//----------------------------------------------------------------------------
//                                           a        b         c        d         e        f
const double Spruce_HH_KG_200_LT_N[6]  = { -0.65100, 2.17217, -0.82444, 2.91428, -1.71019, 0.04344};
const double Spruce_HH_KG_200_500_N[6] = { -0.65243, 2.17217, -0.82444, 2.91428, -1.71019, 0.04344};
const double Spruce_HH_KG_500_GT_N[6]  = { -0.65965, 2.17217, -0.82444, 2.91428, -1.71019, 0.04344};

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG)"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 144 func: 100-01
//  Spruce_BG_63_LT_N  =  Spruce (Gran) + BG <63, Norra
//  Spruce_BG_63_65_N  =  Spruce (Gran) + BG >=63 ... <65, Norra
//  Spruce_BG_65_67_N  =  Spruce (Gran) + BG >=65 ... <67, Norra
//  Spruce_BG_67_GT_N  =  Spruce (Gran) + BG >=67, Norra
//----------------------------------------------------------------------------
//                                     a        b        c        d         e
const double Spruce_BG_63_LT_N[5] = {-0.78473, 2.08374,-0.74090, 3.11476, -1.79579 };
const double Spruce_BG_63_65_N[5] = {-0.78892, 2.08374,-0.74090, 3.11476, -1.79579 };
const double Spruce_BG_65_67_N[5] = {-0.78905, 2.08374,-0.74090, 3.11476, -1.79579 };
const double Spruce_BG_67_GT_N[5] = {-0.79936, 2.08374,-0.74090, 3.11476, -1.79579 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG+HH)"; 021203 p�d
//----------------------------------------------------------------------------
//	Page: 154 func: 100-01
//  Contants for Spruce (Gran) North : Height over sea = <200
//  Spruce_BG_HH_200_LT_63_LT_N[6] = <63
//  Spruce_BG_HH_200_LT_63_65_N[6] = >=63 ... <65
//  Spruce_BG_HH_200_LT_65_67_N[6] = >=65 ... <67
//  Spruce_BG_HH_200_LT_67_GT_N[6] = >=67
//----------------------------------------------------------------------------
//                                                a        b        c        d         e
const double Spruce_BG_HH_200_LT_63_LT_N[5] = { -0.74910, 2.11123,-0.76342, 3.07608, -1.78237};
const double Spruce_BG_HH_200_LT_63_65_N[5] = { -0.75384, 2.11123,-0.76342, 3.07608, -1.78237};
const double Spruce_BG_HH_200_LT_65_67_N[5] = { -0.75549, 2.11123,-0.76342, 3.07608, -1.78237};
const double Spruce_BG_HH_200_LT_67_GT_N[5] = { -0.76640, 2.11123,-0.76342, 3.07608, -1.78237};

//----------------------------------------------------------------------------
//  Contants for Spruce (Gran) North : Height over sea = >=200 ... <500
//----------------------------------------------------------------------------
//	Page: 154 func: 100-01
//  Spruce_BG_HH_500_200_63_LT_N[5] = <63
//  Spruce_BG_HH_500_200_63_65_N[5] = >=63 ... <65
//  Spruce_BG_HH_500_200_65_67_N[5] = >=65 ... <67
//  Spruce_BG_HH_500_200_67_GT_N[5] = >=67
//----------------------------------------------------------------------------
//                                                 a        b        c        d         e
const double Spruce_BG_HH_500_200_63_LT_N[5] = { -0.75208, 2.11123,-0.76342, 3.07608, -1.78237};
const double Spruce_BG_HH_500_200_63_65_N[5] = { -0.75682, 2.11123,-0.76342, 3.07608, -1.78237};
const double Spruce_BG_HH_500_200_65_67_N[5] = { -0.75847, 2.11123,-0.76342, 3.07608, -1.78237};
const double Spruce_BG_HH_500_200_67_GT_N[5] = { -0.76938, 2.11123,-0.76342, 3.07608, -1.78237};

//----------------------------------------------------------------------------
//  Contants for Spruce (Gran) North : Height over sea = >500
//----------------------------------------------------------------------------
//	Page: 154 func: 100-01
//  Spruce_BG_HH_500_GT_63_LT_N[5] = <63
//  Spruce_BG_HH_500_GT_63_65_N[5] = >=63 ... <65
//  Spruce_BG_HH_500_GT_65_67_N[5] = >=65 ... <67
//  Spruce_BG_HH_500_GT_67_GT_N[5] = >=67
//----------------------------------------------------------------------------
//                                                a        b        c        d         e
const double Spruce_BG_HH_500_GT_63_LT_N[5] = { -0.76488, 2.11123,-0.76342, 3.07608, -1.78237};
const double Spruce_BG_HH_500_GT_63_65_N[5] = { -0.76962, 2.11123,-0.76342, 3.07608, -1.78237};
const double Spruce_BG_HH_500_GT_65_67_N[5] = { -0.77127, 2.11123,-0.76342, 3.07608, -1.78237};
const double Spruce_BG_HH_500_GT_67_GT_N[5] = { -0.78218, 2.11123,-0.76342, 3.07608, -1.78237};

//----------------------------------------------------------------------------
//  Contants for Spruce (Gran) South "Brandels (KG)"
//----------------------------------------------------------------------------
//	Page: 106 func: 100-02
//  Spruce_KG_S[6]
//----------------------------------------------------------------------------
//                               a        b        c        d         e				 f
const double Spruce_KG_S[6] = { -0.93173, 2.06103,-0.51644, 2.66914, -1.51878, 0.04291};


/*
*     BJ�RK
*/

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels mindre"; 021202 p�d
//----------------------------------------------------------------------------
//	Page: 114 func: 100-01
//  Birch_Simple_N[5]  = Birch (Bj�rk) Norra
//	Page: 108 func: 100-01
//  Birch_Simple_S[5]  = Birch (Bj�rk) S�dra
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
const double Birch_Simple_N[5]  = { -0.44224, 2.47580,-1.40854, 5.16863,-3.77147 };
const double Birch_Simple_S[5]  = { -0.89359, 2.27954,-1.18672, 7.07362,-5.45175 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG)"; 021203 p�d
//----------------------------------------------------------------------------
//	Page: 140 func: 100-01
//  Birch_BG_57_LT_S[5] = <57
//  Birch_BG_57_59_S[5] = >=57 ... <59
//  Birch_BG_59_GT_S[5] = >=59
//----------------------------------------------------------------------------
//                                    a        b        c        d        e
const double Birch_BG_57_LT_S[5] = {-0.89363, 2.23818,-1.06930, 6.02015,-4.51472 };
const double Birch_BG_57_59_S[5] = {-0.85480, 2.23818,-1.06930, 6.02015,-4.51472 };
const double Birch_BG_59_GT_S[5] = {-0.84627, 2.23818,-1.06930, 6.02015,-4.51472 };

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BG+KG)"; 021203 p�d
//----------------------------------------------------------------------------
//	Page: 140 func: 100-02
//  Birch_BG_KG_57_LT_S[5] = <57
//  Birch_BG_KG_57_59_S[5] = >=57 ... <59
//  Birch_BG_KG_59_GT_S[5] = >=59
//----------------------------------------------------------------------------
//                                       a        b        c        d        e        f
const double Birch_BG_KG_57_LT_S[6] = {-0.92270, 2.21984,-1.05485, 6.04823,-4.50461,-0.01826};
const double Birch_BG_KG_57_59_S[6] = {-0.88394, 2.21984,-1.05485, 6.04823,-4.50461,-0.01826};
const double Birch_BG_KG_59_GT_S[6] = {-0.87744, 2.21984,-1.05485, 6.04823,-4.50461,-0.01826};

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (KG)"; 030903 p�d
//----------------------------------------------------------------------------
//	Page: 108 func: 100-02
//  Birch_KG_S[5]
//----------------------------------------------------------------------------
//                              a        b        c        d        e        f
const double Birch_KG_S[6] = {-0.97217, 2.21755,-1.11538, 6.91587,-5.21757,-0.04895};

//----------------------------------------------------------------------------
//  Constants for volumne function "Brandels (BRK)"; 030829 p�d
//----------------------------------------------------------------------------
//	Page: 108 func: 100-03
//  Birch_BRK_S[5]
//----------------------------------------------------------------------------
//                              a        b        c        d        e        f
const double Birch_BRK_S[6] = {-1.11287, 2.37620,-0.81819, 5.81127,-4.40713,-0.15036};


////////////////////////////////////////////////////////////////////////////////////////
// CVolumeBrandel


/********************************************************************/
//  HANDLE CALCULATEIONS; 021202 p�d
/********************************************************************/
// Calculate volume
// Formula on page 26 (01)
double CVolumeBrandel::getCalcSimple(double hgt,double dbh,const double *const_val)
{
	if (dbh > 0.0 && hgt > 0.0 && (hgt-1.3) > 0.0)
	{
      double f_a  = pow(10.0,(double)(const_val[0]));
      double f_b  = pow((double)dbh,(double)(const_val[1]));
      double f_c  = pow((double)(dbh + 20.0),(double)(const_val[2]));
      double f_d  = pow((double)hgt,(double)(const_val[3]));
      double f_e  = pow((double)(hgt - 1.3),(double)(const_val[4]));
		
      return (f_a * f_b * f_c * f_d * f_e)/1000.0;  // From dm3 to m3; 020417 p�d
	}
	else
      return 0.0;
}

// Calculate volume using "Krongr�ns"
// Formula on page 27 (02)
double CVolumeBrandel::getCalcKG(double hgt,double dbh,double kg,const double *const_val)
{
	if (dbh > 0.0 && hgt > 0.0 && (hgt-1.3) > 0.0 && kg > 0.0)
	{
		double fCalcKG = (hgt - (hgt*(kg/100.0)));
		double f_a  = pow(10.0									,(double)(const_val[0]));
    double f_b  = pow((double)dbh						,(double)(const_val[1]));
    double f_c  = pow((double)(dbh + 20.0)	,(double)(const_val[2]));
    double f_d  = pow((double)hgt						,(double)(const_val[3]));
    double f_e  = pow((double)(hgt - 1.3)		,(double)(const_val[4]));
    double f_f  = pow((double)fCalcKG				,(double)(const_val[5]));	// kg = percent of treeheight; 080625 p�d
		
    return (f_a * f_b * f_c * f_d * f_e * f_f)/1000.0;  // From dm3 to m3; 021129 p�d
	}
	else
    return 0.0;
}

// Calculate volume using "Barkthickness"
// Formula on page 27 (02)
double CVolumeBrandel::getCalcBRK(double hgt,double dbh,double brk,const double *const_val)
{
	if (dbh > 0.0 && hgt > 0.0 && (hgt-1.3) > 0.0 && brk > 0.0)
	{
      double f_a  = pow(10.0                ,(double)(const_val[0]));
      double f_b  = pow((double)dbh         ,(double)(const_val[1]));
      double f_c  = pow((double)(dbh + 20.0),(double)(const_val[2]));
      double f_d  = pow((double)hgt         ,(double)(const_val[3]));
      double f_e  = pow((double)(hgt - 1.3) ,(double)(const_val[4]));
      double f_f  = pow((double)(brk*2.0)		,(double)(const_val[5]));
		
      return (f_a * f_b * f_c * f_d * f_e * f_f)/1000.0;  // From dm3 to m3; 021129 p�d
	}
	else
      return 0.0;
}

// Calculate volume using "Krongr�ns" and "Barkthickness".
// Formula on page 27 (04)
double CVolumeBrandel::getCalcKG_BRK(double hgt,double dbh,double kg,double brk,const double *const_val)
{
	if (dbh > 0.0 && hgt > 0.0 && (hgt-1.3) > 0.0 && brk > 0.0 && kg > 0.0)
	{
		double fCalcKG = (hgt - (hgt*(kg/100.0)));
    double f_a  = pow(10.0								,(double)(const_val[0]));
    double f_b  = pow((double)dbh					,(double)(const_val[1]));
    double f_c  = pow((double)(dbh + 20.0),(double)(const_val[2]));
    double f_d  = pow((double)hgt					,(double)(const_val[3]));
    double f_e  = pow((double)(hgt - 1.3)	,(double)(const_val[4]));
    double f_f  = pow((double)fCalcKG			,(double)(const_val[5]));	// kg = percent of treeheight; 080625 p�d
    double f_g  = pow((double)(brk*2.0)		,(double)(const_val[6]));
		
    return (f_a * f_b * f_c * f_d * f_e * f_f * f_g)/1000.0;  // From dm3 to m3; 030829 p�d
	}
	else
      return 0.0;
}


//================================================================
//  Brandels volume calculation; 070412 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  hgt_sea = Height over sea
//  lng     = Longitude for tree
//  kg      = GreenCrown (Krongr�ns)
//	bark		= Barkavdrag
//================================================================
double CVolumeBrandel::getVolumeForPine(int n,double dbh,double hgt,int hgt_sea,int lng,double kg,double brk)
{
	switch (n)
	{
		//**************************************************
		// "Brandels, mindre, Tall , Norra"
		//**************************************************
		case 0 :
		{
			return getCalcSimple(hgt,dbh,Pine_Simple_N);
		} /* case 0 */
		//**************************************************
		// "Brandels, mindre, Tall , S�dra"
		//**************************************************
		case 1 :
		{
			return getCalcSimple(hgt,dbh,Pine_Simple_S);
		} /* case 1 */
		//**************************************************
		// "Brandels, (BG), Tall , Norra"
		//**************************************************
		case 2 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 63)
				return getCalcSimple(hgt,dbh,Pine_BG_63_LT_N);
			else if (lng >= 63 && lng < 65)
				return getCalcSimple(hgt,dbh,Pine_BG_63_65_N);
			else if (lng >= 65 && lng < 67)
				return getCalcSimple(hgt,dbh,Pine_BG_65_67_N);
			else if (lng >= 67)
				return getCalcSimple(hgt,dbh,Pine_BG_67_GT_N);
		} /* case 2 */
		//**************************************************
		// "Brandels, (BG), Tall , S�dra"
		//**************************************************
		case 3 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 57)
				return getCalcSimple(hgt,dbh,Pine_BG_57_LT_S);
			else if (lng >= 57 && lng < 59)
				return getCalcSimple(hgt,dbh,Pine_BG_57_59_S);
			else if (lng >= 59)
				return getCalcSimple(hgt,dbh,Pine_BG_59_GT_S);
		} /* case 3 */
		
		//**************************************************
		// "Brandels, (BG+KG), Tall , Norra"
		//**************************************************
		case 4 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG ("krongr�ns"), same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 63)
				return getCalcKG(hgt,dbh,kg,Pine_BG_KG_63_LT_N);
			else if (lng >= 63 && lng < 65)
				return getCalcKG(hgt,dbh,kg,Pine_BG_KG_63_65_N);
			else if (lng >= 65 && lng < 67)
				return getCalcKG(hgt,dbh,kg,Pine_BG_KG_65_67_N);
			else if (lng >= 67)
				return getCalcKG(hgt,dbh,kg,Pine_BG_KG_67_GT_N);
		} /* case 4 */
		//**************************************************
		// "Brandels, (BG+KG), Tall , S�dra"
		//**************************************************
		case 5 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG ("krongr�ns"), same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 57)
				return getCalcKG(hgt,dbh,kg,Pine_BG_KG_57_LT_S);
			else if (lng >= 57 && lng < 59)
				return getCalcKG(hgt,dbh,kg,Pine_BG_KG_57_59_S);
			else if (lng >= 59)
				return getCalcKG(hgt,dbh,kg,Pine_BG_KG_59_GT_S);
		} /* case 5 */
		
		//**************************************************
		// "Brandels, (KG+BRK), Tall , S�dra"
		//**************************************************
		case 6 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG ("krongr�ns"); 030829 p�d
			//---------------------------------------------------------
			return getCalcKG_BRK(hgt,dbh,kg,brk,Pine_KG_BRK_S);
		} /* case 6 */
	}	// switch
    return 0.0;
}

//================================================================
//  Brandels volume calculation; 070412 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  hgt_sea = Height over sea
//  lng     = Longitude for tree
//  kg      = GreenCrown (Krongr�ns)
//	bark		= Barkavdrag
//================================================================
double CVolumeBrandel::getVolumeForSpruce(int n,double dbh,double hgt,int hgt_sea,int lng,double kg,double brk)
{
	switch (n)
	{
		//**************************************************
		// "Brandels, mindre, Gran , Norra"
		//**************************************************
		case 0 :
		{
			return getCalcSimple(hgt,dbh,Spruce_Simple_N);
		} /* case 0 */
		//**************************************************
		// "Brandels, mindre, Gran , S�dra"
		//**************************************************
		case 1 :
		{
			return getCalcSimple(hgt,dbh,Spruce_Simple_S);
		} /* case 1 */
		
		//**************************************************
		// "Brandels, (HH),  Gran , Norra"
		//**************************************************
		case 2 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "H�jd �ver havet"; 021202 p�d
			//---------------------------------------------------------
			if (hgt_sea < 200)
				return getCalcSimple(hgt,dbh,Spruce_HH_200_LT_N);
			else if (hgt_sea >= 200 && hgt_sea < 500)
				return getCalcSimple(hgt,dbh,Spruce_HH_200_500_N);
			else if (hgt_sea >= 500)
				return getCalcSimple(hgt,dbh,Spruce_HH_500_GT_N);
		} /* case 2 */
		
		//**************************************************
		// "Brandels, (HH+KG),  Gran , Norra"
		//**************************************************
		case 3 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG, based on "H�jd �ver havet"
			//  and "Krongr�ns"; 021202 p�d
			//---------------------------------------------------------
			if (hgt_sea < 200)
				return getCalcKG(hgt,dbh,kg,Spruce_HH_KG_200_LT_N);
			else if (hgt_sea >= 200 && hgt_sea < 500)
				return getCalcKG(hgt,dbh,kg,Spruce_HH_KG_200_500_N);
			else if (hgt_sea >= 500)
				return getCalcKG(hgt,dbh,kg,Spruce_HH_KG_500_GT_N);
		} /* case 3 */
		
		//**************************************************
		// "Brandels, (BG),  Gran , Norra"
		//**************************************************
		case 4 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 63)
				return getCalcSimple(hgt,dbh,Spruce_BG_63_LT_N);
			else if (lng >= 63 && lng < 65)
				return getCalcSimple(hgt,dbh,Spruce_BG_63_65_N);
			else if (lng >= 65 && lng < 67)
				return getCalcSimple(hgt,dbh,Spruce_BG_65_67_N);
			else if (lng >= 67)
				return getCalcSimple(hgt,dbh,Spruce_BG_67_GT_N);
		} /* case 4 */
		
		//**************************************************
		// "Brandels, (BG+HH),  Gran , Norra"
		//**************************************************
		case 5 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "H�jd �ver havet"
			//  and "Breddgrad"; 021203 p�d
			//---------------------------------------------------------
			if (hgt_sea < 200)
			{
				if (lng < 63)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_200_LT_63_LT_N);
				else if (lng >= 63 && lng < 65)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_200_LT_63_65_N);
				else if (lng >= 65 && lng < 67)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_200_LT_65_67_N);
				else if (lng >= 67)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_200_LT_67_GT_N);
			}
			else if (hgt_sea >= 200 && hgt_sea < 500)
			{
				if (lng < 63)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_500_200_63_LT_N);
				else if (lng >= 63 && lng < 65)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_500_200_63_65_N);
				else if (lng >= 65 && lng < 67)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_500_200_65_67_N);
				else if (lng >= 67)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_500_200_67_GT_N);
			}
			else if (hgt_sea >= 500)
			{
				if (lng < 63)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_500_GT_63_LT_N);
				else if (lng >= 63 && lng < 65)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_500_GT_63_65_N);
				else if (lng >= 65 && lng < 67)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_500_GT_65_67_N);
				else if (lng >= 67)
					return getCalcSimple(hgt,dbh,Spruce_BG_HH_500_GT_67_GT_N);
			}
		} /* case 5 */
		//**************************************************
		// "Brandels, (KG),  Gran , S�dra"
		//**************************************************
		case 6 :
		{
			return getCalcKG(hgt,dbh,kg,Spruce_KG_S);
		}	/* Case 6 */		
  } // switch ((*n))
	
    return 0.0;
}

//================================================================
//  Brandels volume calculation; 070412 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  hgt_sea = Height over sea
//  lng     = Longitude for tree
//  kg      = GreenCrown (Krongr�ns)
//	brk			= Barkavdrag
//================================================================
double CVolumeBrandel::getVolumeForBirch(int n,double dbh,double hgt,int hgt_sea,int lng,double kg,double brk)
{
	CString S;
	switch (n)
	{
		//**************************************************
		// "Brandels, mindre, Bj�rk, Norra"
		//**************************************************
		case 0 :
		{
			return getCalcSimple(hgt,dbh,Birch_Simple_N);
		} /* case 0 */
		//**************************************************
		// "Brandels, mindre, Bj�rk, S�dra"
		//**************************************************
		case 1 :
		{
			return getCalcSimple(hgt,dbh,Birch_Simple_S);
		} /* case 1 */
		
		//**************************************************
		// "Brandels, (BG), Bj�rk, S�dra"
		//**************************************************
		case 2 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcSimple, same as for
			//  "Brandels mindre", but based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 57)
				return getCalcSimple(hgt,dbh,Birch_BG_57_LT_S);
			else if (lng >= 57 && lng < 59)
				return getCalcSimple(hgt,dbh,Birch_BG_57_59_S);
			else if (lng >= 59)
				return getCalcSimple(hgt,dbh,Birch_BG_59_GT_S);
		} /* case 2 */
		
		//**************************************************
		// "Brandels, (BG+KG), Bj�rk, S�dra"
		//**************************************************
		case 3 :
		{
			//---------------------------------------------------------
			//  Calculate volume using CalcKG, based on "Breddgrad"; 021202 p�d
			//---------------------------------------------------------
			if (lng < 57)
			{
				return getCalcKG(hgt,dbh,kg,Birch_BG_KG_57_LT_S);
			}
			else if (lng >= 57 && lng < 59)
			{
				return getCalcKG(hgt,dbh,kg,Birch_BG_KG_57_59_S);
			}
			else if (lng >= 59)
			{
				return getCalcKG(hgt,dbh,kg,Birch_BG_KG_59_GT_S);
			}
		} /* case 3 */
		//**************************************************
		// "Brandels, (KG),  Bj�rk , S�dra"
		//**************************************************
		case 4 :
		{
			return getCalcKG(hgt,dbh,kg,Birch_KG_S);
		}	/* Case 4 */		
		
		//**************************************************
		// "Brandels, (BRK),  Bj�rk , S�dra"
		//**************************************************
		case 5 :
		{
			return getCalcBRK(hgt,dbh,brk,Birch_BRK_S);
		}	/* Case 5 */		
   } // switch ((*n))
	
   return 0.0;
}

// PUBLIC:
CVolumeBrandel::CVolumeBrandel(void)
	: CCalculationBaseClass()
{
}

CVolumeBrandel::CVolumeBrandel(int spc_index,CTransaction_trakt rec1,
																						 CTransaction_trakt_misc_data rec2,
																						 vecTransactionTraktData &tdata_list,
																						 vecTransactionSampleTree &sample_tree_list,
																						 vecTransactionDCLSTree &dcls_tree_list,
																						 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}


BOOL CVolumeBrandel::calculate(vecTransactionTraktData &vec1,
															 vecTransactionSampleTree &vec2,
															 vecTransactionDCLSTree &vec4,		// Added 070820 p�d
															 vecTransactionTraktSetSpc& vec3)
{
	CString S;
	double fM3Sk = 0.0;
	double fM3Fub = 0.0;
	double fGreenCrown = 0.0;
	int nNumOfTrees = 0;	// Number of trees in Duameterclass
	int nVolSpcID = -1;
	int nVolIndex = -1;
	int nSpcID = -1;

	// Check that there's any species; 070418 p�d
	if (m_nNumOfDCLSSpecies == 0)
		return FALSE;

	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nVolSpcID = getVolumeSpcID();	
	nVolIndex = getVolumeIndex();
	// Loop through the tree list (m_vecSampleTrees) and
	// calculate volume for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getVolumeID() to get which species
	// method to use and getVolumeIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070417 p�d
	for (long i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		if (tree.getSpcID() == nSpcID)
		{
			// Adaption for "Intr�ngsv�rdering":
			// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
			// OBS! This only allies to "Intr�ng"; 080307 p�d
			nNumOfTrees = tree.getNumOf() + tree.getNumOfRandTrees();
			// Get "Gr�nkroneprocent f�r tr�dslag"; 080625 p�d
			fGreenCrown = getGreenCrownPercForSpc(tree.getSpcID());
			// Depending on specie, run function accoring to
			// nBarkIndex value; 070417 p�d
			if (nVolSpcID == 1)	// Tall
			{
				fM3Sk = getVolumeForPine(nVolIndex,getClassMid(tree)/10.0,	// dm
																					 tree.getHgt()/10.0,	// m
																					 getTrakt().getTraktHgtOverSea(),
																					 getTrakt().getTraktLatitude(),
																					 fGreenCrown, //tree.getGCrownPerc(),
																					 tree.getBarkThick());
			}	// if (nVolID == 1)	Tall
			if (nVolSpcID == 2) // Gran
			{
				fM3Sk = getVolumeForSpruce(nVolIndex,getClassMid(tree)/10.0,	// dm
																						 tree.getHgt()/10.0,	// m
																						 getTrakt().getTraktHgtOverSea(),
																						 getTrakt().getTraktLatitude(),
																						 fGreenCrown, //tree.getGCrownPerc(),
																						 tree.getBarkThick());
			}	// if (nVolID == 2) Gran
			if (nVolSpcID == 3)	// Bj�rk
			{
				fM3Sk = getVolumeForBirch(nVolIndex,getClassMid(tree)/10.0,	// dm
																					  tree.getHgt()/10.0,	// m
																					  getTrakt().getTraktHgtOverSea(),
																					  getTrakt().getTraktLatitude(),
																					  fGreenCrown, //tree.getGCrownPerc(),
																					  tree.getBarkThick());
			}	// if (nVolID == 3)	Bj�rk

			if (nNumOfTrees > 0)
			{
				fM3Sk	*= nNumOfTrees;
				fM3Fub	*= nNumOfTrees;
			}
			
			m_vecDCLSTrees[i].setM3sk(fM3Sk);

			// Also calculate m3fub using "�verf�ringstal" for m3sk to m3fub
			// in esti_trakt_set_spc_table; 070524 p�d
//			fM3Fub = fM3Sk * getM3SkToM3Fub();
			fM3Fub = 0.0;	// Not used 2009-06-04 p�d
/* FOR DEBUG
			CString S;
			S.Format(_T("CVolumeBrandel::calculate\n\nSpc %d\nfM3Sk %f\nfM3Fub %f\ngetM3SkToM3Fub %f"),
				nSpcID,fM3Sk,fM3Fub,getM3SkToM3Fub());
			AfxMessageBox(S);
/*/
			m_vecDCLSTrees[i].setM3fub(fM3Fub);
			m_vecDCLSTrees[i].setGCrownPerc(fGreenCrown);

			// Also set Calculated "Gr�nkrona", from SampleTrees/Specie; 080625 p�d
			setTraktData_gcrown(nSpcID,fGreenCrown);

		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}