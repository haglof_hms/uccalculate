#if !defined(AFX_HAGBERGMARTEN_H)
#define AFX_HAGBERGMARTEN_H

#include "StdAfx.h"

#include "UCCalculationBaseClass.h"

////////////////////////////////////////////////////////////////////////////////////////
// CVolumeHagbergMarten

class CVolumeHagbergMarten : public CCalculationBaseClass
{
	//private
protected:
	// Calculate Oak
	double setM3SkForOak_whole_trunk(double dbh,double hgt,bool add_form_volume);
	double setM3SkForOak_forks(double dbh,double hgt,bool add_form_volume);
	double setM3FubForOak_whole_trunk(double dbh,double hgt,bool add_form_volume);
	double setM3FubForOak_forks(double dbh,double hgt,bool add_form_volume);
	// Calculate Beech
	double setM3SkForBeech_whole_trunk(double dbh,double hgt,bool add_form_volume);
	double setM3SkForBeech_forks(double dbh,double hgt,bool add_form_volume);
	double setM3FubForBeech_whole_trunk(double dbh,double hgt,bool add_form_volume);
	double setM3FubForBeech_forks(double dbh,double hgt,bool add_form_volume);

	double calculateCorrFactor(double dbh,double hgt);

public:
	CVolumeHagbergMarten(void);
	CVolumeHagbergMarten(int spc_index,CTransaction_trakt rec1,
																		 CTransaction_trakt_misc_data rec2,
																		 vecTransactionTraktData &tdata_list,
																		 vecTransactionSampleTree &sample_tree_list,
																		 vecTransactionDCLSTree &dcls_tree_list,
																		 vecTransactionTraktSetSpc &spc_list);

	BOOL calculate(vecTransactionTraktData&,
								 vecTransactionSampleTree&,
								 vecTransactionDCLSTree&,
								 vecTransactionTraktSetSpc&);

	BOOL calculate_ub(vecTransactionTraktData&,
										vecTransactionSampleTree&,
										vecTransactionDCLSTree&,
										vecTransactionTraktSetSpc&);
};


#endif
