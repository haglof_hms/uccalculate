#if !defined(AFX_EXPORTEDFUNCTIONS_H)
#define AFX_EXPORTEDFUNCTIONS_H

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD extern __declspec(dllexport)
#else
#define DLL_BUILD extern __declspec(dllimport)
#endif

#include "StdAfx.h"

////////////////////////////////////////////////////////////////////////////
// exported functions
extern "C" 
{
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//=====================================================================================
// Returns a list of ALL bark-functions in this Module	
//=====================================================================================
	void DLL_BUILD getBarkFunctions(vecUCFunctions &list);
//=====================================================================================
//	Functions listsed; 070416 p�d
//=====================================================================================
	void DLL_BUILD getBarkFunctionList(vecUCFunctionList &list);

//=====================================================================================
// Returns a list of ALL heightfunctions in this Module	
//=====================================================================================
	void DLL_BUILD getHeightFunctions(vecUCFunctions &list);
//=====================================================================================
//	Functions listsed; 070416 p�d
//=====================================================================================
	void DLL_BUILD getHeightFunctionList(vecUCFunctionList &list);


//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//=====================================================================================
// Returns a list of ALL volumefunctions in this Module	
//=====================================================================================
	void DLL_BUILD getVolumeFunctions(vecUCFunctions &list);
//=====================================================================================
//	Functions listsed; 070416 p�d
//=====================================================================================
	void DLL_BUILD getVolumeFunctionList(vecUCFunctionList &list);

//=====================================================================================
// Returns a list of ALL volumefunctions for m3ub in this Module	
//=====================================================================================
	void DLL_BUILD getVolumeFunctions_ub(vecUCFunctions &list);
//=====================================================================================
//	Functions listsed; 071016 p�d
//=====================================================================================
	void DLL_BUILD getVolumeFunctionList_ub(vecUCFunctionList &list);

//=====================================================================================
//	GROT Functions listsed; 100311 p�d
//=====================================================================================
	void DLL_BUILD getGROTFunctionsCalc(vecUCFunctions &list);

//-------------------------------------------------------------------------------------
//	DoCalulate function; 060320 p�d
//-------------------------------------------------------------------------------------
	BOOL DLL_BUILD doCalculation(CTransaction_trakt rec1,
															 CTransaction_trakt_misc_data rec2,
															 vecTransactionTraktData &tdata_list,
															 vecTransactionSampleTree &sample_tree_list,
															 vecTransactionDCLSTree &dcls_tree_list,
															 vecTransactionTraktSetSpc &spc_list);

//-------------------------------------------------------------------------------------
//	DoGROTCalulate function; 100311 p�d
//	- St�ndortsindex f�r Tall eller Gran (m)
//	- Breddgrad
//	- H�jd �ver havet (km!)
//-------------------------------------------------------------------------------------
	BOOL DLL_BUILD doGROTCalc(CTransaction_trakt rec,vecTransactionTraktData &trakt_data,vecTransactionTraktSetSpc &spc_list,vecTransactionDCLSTree &dcls_tree_list);

}


#endif