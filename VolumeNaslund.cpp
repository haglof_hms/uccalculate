#include "StdAfx.h"

#include "VolumeNaslund.h"

//----------------------------------------------------------------
//  Constans for volume function "N�slunds"
//----------------------------------------------------------------

/*
*     TALL
*/
//                                 a        b        c         d
const double Pine_Large_N[4]  = { 0.10180, 0.03112, 0.007312, 0.002906 };
const double Pine_Small_N[3]  = { 0.09314, 0.03069, 0.002818 };
const double Pine_Large_S[5]  = { 0.11930, 0.02574, 0.007262, 0.004054, 0.003112 };
const double Pine_Small_S[3]  = { 0.10720, 0.02427, 0.007315 };


/*
*     GRAN
*/
//                                   a        b        c         d        f
const double Spruce_Large_N[5]  = { 0.11020, 0.01648, 0.005901, 0.01929, 0.05565 };
const double Spruce_Small_N[4]  = { 0.12020, 0.01504, 0.02341 , 0.06590 };
const double Spruce_Large_S[5]  = { 0.10590, 0.01968, 0.006168, 0.01468, 0.04585 };
const double Spruce_Small_S[4]  = { 0.11040, 0.01925, 0.01815 , 0.04936 };

/*
*     BJ�RK
*/
//                                   a        b        c         d        f
const double Birch_Large_N[5]  = { 0.04192, 0.029270, 0.003263, 0.003719, 0.001692 };
const double Birch_Small_N[3]  = { 0.03715, 0.028920, 0.004983 };
const double Birch_Large_S[5]  = { 0.09595, 0.023750, 0.012210, 0.036360, 0.004605 };
const double Birch_Small_S[4]  = { 0.14320, 0.008561, 0.021800, 0.06630};



/********************************************************************/
//  HANDLE CALCULATEIONS; 070413 p�d
/********************************************************************/
//---------------------------------------------
//  Fromula: d2 + d2h + d2k - dhb
//---------------------------------------------
double CVolumeNaslund::getCalc_1(double dbh,double hgt,double kg,double bark,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3 || kg == 0.0 || bark == 0.0) return 0.0;
	double fCalcKG = (hgt - (hgt*(kg/100.0)));

  double f_kg   = fCalcKG;
  double f_d2   = pow(dbh, 2) * const_val[0];
  double f_d2h  = pow(dbh, 2) * hgt * const_val[1];
  double f_d2k  = pow(dbh, 2) * f_kg * const_val[2];
  double f_dbh  = dbh * hgt * (bark*2.0) * const_val[3];

  return (f_d2 + f_d2h + f_d2k - f_dbh)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + d2k + dh2 - dhb
//---------------------------------------------
double CVolumeNaslund::getCalc_2(double dbh,double hgt,double kg,double bark,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3 || kg == 0.0 || bark == 0.0) return 0.0;
	double fCalcKG = (hgt - (hgt*(kg/100.0)));

  double f_kg   = fCalcKG;
  double f_d2   = pow(dbh, 2) * const_val[0];
  double f_d2h  = pow(dbh, 2) * hgt * const_val[1];
  double f_d2k  = pow(dbh, 2) * f_kg * const_val[2];
  double f_dh2  = dbh * pow(hgt,2) * const_val[3];
  double f_dbh  = dbh*hgt*(bark*2.0) * const_val[4];
	
  return (f_d2 + f_d2h + f_d2k + f_dh2 - f_dbh)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + dh2
//---------------------------------------------
double CVolumeNaslund::getCalc_3(double dbh,double hgt,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3) return 0.0;

  double f_d2   = pow(dbh, 2) * const_val[0];
  double f_d2h  = pow(dbh, 2) * hgt * const_val[1];
  double f_dh2  = dbh * pow(hgt,2) * const_val[2];

  return (f_d2 + f_d2h + f_dh2)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + d2k + dh2 - h2
//---------------------------------------------
double CVolumeNaslund::getCalc_4(double dbh,double hgt,double kg,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
	//  if (dbh == 0.0 || hgt <= 1.3 || kg == 0.0) return 0.0;
	double fCalcKG = (hgt - (hgt*(kg/100.0)));

	double f_kg   = fCalcKG;
  double f_d2   = pow(dbh, 2) * const_val[0];
  double f_d2h  = pow(dbh, 2) * hgt * const_val[1];
  double f_d2k  = pow(dbh, 2) * f_kg * const_val[2];
  double f_dh2  = dbh * pow(hgt,2) * const_val[3];
  double f_h2   = pow(hgt, 2) * const_val[4];

	return (f_d2 + f_d2h + f_d2k + f_dh2 - f_h2)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + dh2 - h2
//---------------------------------------------
double CVolumeNaslund::getCalc_5(double dbh,double hgt,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3) return 0.0;

  double f_d2   = pow(dbh, 2) * const_val[0];
  double f_d2h  = pow(dbh, 2) * hgt * const_val[1];
  double f_dh2  = dbh * pow(hgt,2) * const_val[2];
  double f_h2   = pow(hgt, 2) * const_val[3];

  return (f_d2 + f_d2h + f_dh2 - f_h2)/1000.0; // From dm3 to m3
}
//---------------------------------------------
//  Fromula: d2 + d2h + dh2 - h2 - dbh
//---------------------------------------------
double CVolumeNaslund::getCalc_6(double dbh,double hgt,double bark,const double *const_val)
{
  //------------------------------------------------------------
  //  Check if any of the valuse = 0, if so return 0.0; 021206 p�d
  //------------------------------------------------------------
  // if (dbh == 0.0 || hgt <= 1.3 || bark == 0.0) return 0.0;

  double f_d2   = pow(dbh, 2) * const_val[0];
  double f_d2h  = pow(dbh, 2) * hgt * const_val[1];
  double f_dh2  = dbh * pow(hgt,2) * const_val[2];
  double f_h2   = pow(hgt, 2) * const_val[3];
  double f_dbh  = dbh*hgt*(bark*2.0) * const_val[4];

  return (f_d2 + f_d2h + f_dh2 - f_h2 - f_dbh)/1000.0; // From dm3 to m3
}


//================================================================
//  N�slunds volume calculation; 070413 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  kg      = GreenCrown (Krongr�ns)
//  bark    = bark reduction (barkavdrag)
//================================================================
double CVolumeNaslund::getVolumeForPine(int n,double dbh,double hgt,double kg,double bark)
{
	switch (n)
	{
		//**************************************************
    //  "N�slunds mindre, Tall, Norra"; 070413 p�d
		//**************************************************
		case 0 :
    {
			return getCalc_3(dbh,hgt,Pine_Small_N);
    }
	
		//**************************************************
    //  "N�slunds mindre, Tall, S�dra"; 070413 p�d
		//**************************************************
		case 1 :
    {
			return getCalc_3(dbh,hgt,Pine_Small_S);
    }
	
		//**************************************************
    //  "N�slunds st�rre, Tall, Norra"; 070413 p�d
		//**************************************************
		case 2 :
    {
			return getCalc_1(dbh,hgt,kg,bark,Pine_Large_N);
    }
			
		//**************************************************
    //  "N�slunds st�rre, Tall, S�dra"; 070413 p�d
		//**************************************************
		case 3 :
    {
			return getCalc_2(dbh,hgt,kg,bark,Pine_Large_S);
    }
		
	}
	return 0.0;
}

//================================================================
//  N�slunds volume calculation; 070413 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  kg      = GreenCrown (Krongr�ns)
//  bark    = bark reduction (barkavdrag)
//================================================================
double CVolumeNaslund::getVolumeForSpruce(int n,double dbh,double hgt,double kg,double bark)
{
	switch (n)
	{

		//**************************************************
    //  "N�slunds mindre, Gran, Norra"; 070413 p�d
		//**************************************************
		case 0 :
    {
			return getCalc_5(dbh,hgt,Spruce_Small_N);
    }
	
		//**************************************************
    //  "N�slunds mindre, Gran, S�dra"; 070413 p�d
		//**************************************************
		case 1 :
    {
			return getCalc_5(dbh,hgt,Spruce_Small_S);
    }
	
		//**************************************************
    //  "N�slunds st�rre, Gran, Norra"; 070413 p�d
		//**************************************************
		case 2 :
    {
			return getCalc_4(dbh,hgt,kg,Spruce_Large_N);
    }
			
		//**************************************************
    //  "N�slunds st�rre, Gran, S�dra"; 070413 p�d
		//**************************************************
		case 3 :
    {
			return getCalc_4(dbh,hgt,kg,Spruce_Large_S);
    }
	}
	return 0.0;
}



//================================================================
//  N�slunds volume calculation; 070413 p�d
//  n       = sqeuence number for constants.
//  dbh     = diamter in breast height
//  hgt     = height of tree (testtree or calculated)
//  kg      = GreenCrown (Krongr�ns)
//  bark    = bark reduction (barkavdrag)
//================================================================
double CVolumeNaslund::getVolumeForBirch(int n,double dbh,double hgt,double kg,double bark)
{
	switch (n)
	{

		//**************************************************
    //  "N�slunds mindre, Bj�rk, Norra"; 070413 p�d
		//**************************************************
		case 0 :
    {
			return getCalc_3(dbh,hgt,Birch_Small_N);
    }
	
		//**************************************************
    //  "N�slunds mindre, Bj�rk, S�dra"; 070413 p�d
		//**************************************************
		case 1 :
    {
			return getCalc_5(dbh,hgt,Birch_Small_S);
    }
	
		//**************************************************
    //  "N�slunds st�rre, Bj�rk, Norra"; 070413 p�d
		//**************************************************
		case 2 :
    {
			return getCalc_2(dbh,hgt,kg,bark,Birch_Large_N);
    }
			
		//**************************************************
    //  "N�slunds st�rre, Bj�rk, S�drea"; 070413 p�d
		//**************************************************
		case 3 :
    {
			return getCalc_6(dbh,hgt,bark,Birch_Large_S);
    }
	}
	return 0.0;
}

// PUBLIC:
CVolumeNaslund::CVolumeNaslund(void)
	: CCalculationBaseClass()
{
}

CVolumeNaslund::CVolumeNaslund(int spc_index,CTransaction_trakt rec1,
																						 CTransaction_trakt_misc_data rec2,
																						 vecTransactionTraktData &tdata_list,
																						 vecTransactionSampleTree &sample_tree_list,
																						 vecTransactionDCLSTree &dcls_tree_list,
																						 vecTransactionTraktSetSpc &spc_list)
	: CCalculationBaseClass(spc_index,rec1,rec2,tdata_list,sample_tree_list,dcls_tree_list,spc_list)
{
}

BOOL CVolumeNaslund::calculate(vecTransactionTraktData& vec1,
															 vecTransactionSampleTree& vec2,
															 vecTransactionDCLSTree& vec4,	// Added 070820 p�d
															 vecTransactionTraktSetSpc& vec3)
{
	double fM3Sk = 0.0;
	double fM3Fub = 0.0;
	double fGreenCrown = 0.0;
	int nVolSpcID = -1;
	int nVolIndex = -1;
	int nSpcID = -1;
	int nNumOfTrees = 0;
	// Check that there's any species; 070418 p�d
	if (m_nNumOfDCLSSpecies == 0)
		return FALSE;

	// Check that there's any trees; 070418 p�d
	if (m_nNumOfDCLSTrees == 0)
		return FALSE;

	//==========================================================================
	// Start calculating volumes per tree in m_vecSampleTrees.
	// We'll do the calculation per specie, so we can get VolumeFuncionID and
	// VolumeFunctionIndex; 070418 p�d
	// These three values are linked to the Index 
	//	of the m_vecSetSpc; 070418 p�d
	nSpcID = getSpecieID();	
	nVolSpcID = getVolumeSpcID();	
	nVolIndex = getVolumeIndex();
	// Loop through the tree list (m_vecSampleTrees) and
	// calculate volume for each tree, based
	// on the method of calculation for specie.
	// To get information on which method to use
	// we call the getVolumeID() to get which species
	// method to use and getVolumeIndex() to get
	// the specific function withint the method,
	// declared in UCCalculationBaseClass; 070417 p�d
	for (long i = 0;i < m_nNumOfDCLSTrees;i++)
	{
		CTransaction_dcls_tree tree = m_vecDCLSTrees[i];
		if (tree.getSpcID() == nSpcID)
		{
			// Adaption for "Intr�ngsv�rdering":
			// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
			// OBS! This only allies to "Intr�ng"; 080307 p�d
			nNumOfTrees = tree.getNumOf() + tree.getNumOfRandTrees();
			// Get "Gr�nkroneprocent f�r tr�dslag"; 080626 p�d
			fGreenCrown = getGreenCrownPercForSpc(tree.getSpcID());
			// Depending on specie, run function accoring to
			// nBarkIndex value; 070417 p�d
			if (nVolSpcID == 1)	// Tall
			{
				fM3Sk = getVolumeForPine(nVolIndex,getClassMid(tree)/10.0,	// dm
																					 tree.getHgt()/10.0,	// m
																					 fGreenCrown, //tree.getGCrownPerc(),
																					 tree.getBarkThick());

			}	// if (nVolID == 1)	Tall
			if (nVolSpcID == 2) // Gran
			{
				fM3Sk = getVolumeForSpruce(nVolIndex,getClassMid(tree)/10.0,	// dm
																					 tree.getHgt()/10.0,		// m
																					 fGreenCrown, //tree.getGCrownPerc(),
																					 tree.getBarkThick());
			}	// if (nVolID == 2) Gran
			if (nVolSpcID == 3)	// Bj�rk
			{
				fM3Sk = getVolumeForBirch(nVolIndex,getClassMid(tree)/10.0,	// dm
																  				 tree.getHgt()/10.0,	// m
																					 fGreenCrown, //tree.getGCrownPerc(),
																					 tree.getBarkThick());
			}	// if (nVolID == 3)	Bj�rk
			if (nNumOfTrees > 0)
			{
				fM3Sk	*= nNumOfTrees;
				fM3Fub	*= nNumOfTrees;
			}

			m_vecDCLSTrees[i].setM3sk(fM3Sk);
			// Also calculate m3fub using "�verf�ringstal" for m3sk to m3fub
			// in esti_trakt_set_spc_table; 070626 p�d
//			fM3Fub = fM3Sk * getM3SkToM3Fub();
			fM3Fub = 0.0;	// Not used 2009-06-04 p�d
			m_vecDCLSTrees[i].setM3fub(fM3Fub);

			// Also set Calculated "Gr�nkrona", from SampleTrees/Specie; 080625 p�d
			setTraktData_gcrown(nSpcID,fGreenCrown);
		}	// if (tree.getSpcID() == nSpcID)
	}	// for (UINT i = 0;i < m_nNumOfSampleTrees;i++)

	vec1 = m_vecTraktData;
	vec2 = m_vecSampleTrees;
	vec4 = m_vecDCLSTrees;
	vec3 = m_vecSetSpc;

	return TRUE;
}
